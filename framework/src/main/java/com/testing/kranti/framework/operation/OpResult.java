package com.testing.kranti.framework.operation;

import java.util.List;

public interface OpResult {
    int getExitStatus();

    List<String> getStdOut();

    List<String> getStdErr();

    long getExecutionTime();

    String toStringAsOneLine();
}
