package com.testing.kranti.framework;

import java.util.List;

public interface TestCase {
    String getTestName();

    String getTestGroup();

    String getSandBoxDir();

    List<? extends TestComponent> getTestComponents();

    boolean isRunning();

    void tearDown();
}
