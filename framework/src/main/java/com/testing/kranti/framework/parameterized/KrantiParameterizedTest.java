package com.testing.kranti.framework.parameterized;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface KrantiParameterizedTest {
}
