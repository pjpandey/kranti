package com.testing.kranti.framework.parameterized;

public interface IParameterizedTest {
    IParametersGenerator getParametersGenerator();
}
