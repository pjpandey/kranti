package com.testing.kranti.framework.rules.config;

import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import com.testing.kranti.framework.utils.File.FileUtil;
import org.junit.runners.model.FrameworkMethod;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class KrantiFilesRule extends AbstractMethodRule {
    private List<String> filesToCopy = new ArrayList<String>();
    private List<String> lstCopyFromRelativePath = null;

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        extractTestProperties(oTest.getClass().getAnnotation(KrantiFiles.class), mTest.getAnnotation(KrantiFiles.class));

        lstCopyFromRelativePath = generateClassHierarchyCandidates(oTest);
        Collections.reverse(lstCopyFromRelativePath);
    }

    private void extractTestProperties(KrantiFiles... arrFiles) {
        for (KrantiFiles ePreCopy : arrFiles) {
            if (null != ePreCopy) {
                filesToCopy.addAll(Arrays.asList(ePreCopy.value()));
            }
        }
    }

    public void preCopy() {
        String sCopyToPath = TestEnvironment.getSandBoxDir();
        for (String sFile : filesToCopy) {
            for (String sCopyFromRelativePath : lstCopyFromRelativePath) {
                String sCopyFromFile = TestEnvironment.getTestFolderPath(sCopyFromRelativePath) + "/" + sFile;
                if (new File(sCopyFromFile).exists()) {
                    String destination = sCopyToPath + "/" + sFile;
                    try {
                        FileUtil.createDir(FileUtil.getParent(destination));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    FileUtil.copyFile(sCopyFromFile, destination);
                }
            }
        }
    }


    private List<String> generateClassHierarchyCandidates(Object oTest) {
        Set<String> candidates = new HashSet<>();
        Class<? extends Object> testClass = oTest.getClass();
        for (Class<?> c = testClass; c != Object.class; c = c.getSuperclass()) {
            String candidate = c.getPackage().getName().replaceAll("\\.", "/");
            candidates.add(candidate);
        }
        return new ArrayList<String>(candidates);
    }
}

