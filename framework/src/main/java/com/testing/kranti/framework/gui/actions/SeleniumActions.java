package com.testing.kranti.framework.gui.actions;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.check.Check;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import com.testing.kranti.framework.gui.timeout.TimeoutType;
import com.testing.kranti.framework.reporter.TestReporter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;

abstract class SeleniumActions<B extends Browser> implements ISeleniumActions {
    private static final long DEFAULT_POLL_MILLIS = 100;

    protected B browser;
    protected final CustomTimeouts timeoutsConfig;

    public SeleniumActions(B browser) {
        this.browser = Preconditions.checkNotNull(browser, "Error: you must supply a non-null Browser to BaseSeleniumActions!");
        this.timeoutsConfig = Preconditions.checkNotNull(browser.getTimeouts(), "Error: you must supply a non-null Timeouts to BaseSeleniumActions!");
    }

    protected WebDriver webDriver() {
        return browser.getWebDriver();
    }

    @SuppressWarnings("unchecked")
    @Override
    public B getBrowser() {
        return browser;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void setBrowser(Browser browser) {
        this.browser = (B) browser;
    }

    @Override
    public Actions getActionsBuilder() {
        return new Actions(webDriver());
    }

    @Override
    public void acceptAlert(TimeoutType timeout) {
        Alert alert = webDriver().switchTo().alert();
        alert.accept();
    }

    @Override
    public void dismissAlert(TimeoutType timeout) {
        Alert alert = webDriver().switchTo().alert();
        alert.dismiss();
    }

    @Override
    public String getAlertText() {
        Alert alert = webDriver().switchTo().alert();
        return alert.getText();
    }


    @Override
    public WebElement clearText(By locator, TimeoutType timeout) {
        final WebElement el = verifyElementPresent(locator, timeout);
        clearText(el);
        TestReporter.TRACE("Cleared text from element with Locator " + locator + "");
        return el;
    }

    @Override
    public WebElement clearText(By locator) {
        final WebElement el = verifyElementPresent(locator, TimeoutType.TWO_MINUTE);
        clearText(el);
        TestReporter.TRACE("Cleared text from element with Locator " + locator + "");
        return el;
    }

    @Override
    public WebElement clearText(@Nonnull WebElement el) {
        final WebElement elm = el;
        seleniumCallable<WebElement> callable = new seleniumCallable<WebElement>() {
            public WebElement call() {
                elm.clear();
                return elm;
            }

            @Override
            public WebElement getObject() {
                return elm;
            }
        };
        return waitOnCallableAction(callable, "Unable to clear text, trying on element  " + el);
    }

    @Override
    public void select(By locator, String sText) {
        select(locator, sText, TimeoutType.TWO_MINUTE);
    }

    @Override
    public void select(By locator, String sText, TimeoutType timeoutType) {
        WebElement el = verifyElementPresent(locator, timeoutType);
        if (!verifySelectOptionExist(locator, sText)) {
            TestReporter.FATAL("Given option did not present in select dropdown : " + sText);
        }
        Select dropdown = new Select(el);
        dropdown.selectByVisibleText(sText);
        TestReporter.TRACE("Selected item " + sText + " with locator " + locator);
    }

    @Override
    public boolean verifySelectOptionExist(By locator, String sText) {
        return verifySelectOptionExist(locator, sText, TimeoutType.TWO_MINUTE);
    }

    @Override
    public boolean verifySelectOptionExist(By locator, String sText, TimeoutType timeoutType) {
        WebElement el = verifyElementPresent(locator);
        Select dropdown = new Select(el);
        TestReporter.TRACE("clicking on dropdown options with locator " + locator);
        for (WebElement option : dropdown.getOptions()) {
            TestReporter.TRACE("got following options : " + option.getText());
        }
        for (WebElement option : dropdown.getOptions()) {
            if (option.getText().equals(sText))
                return true;
        }
        return false;
    }

    @Override
    public List<String> getAllDropDownList(By locator) {
        return getAllDropDownList(locator, TimeoutType.TWO_MINUTE);
    }

    @Override
    public List<String> getAllDropDownList(By locator, TimeoutType timeoutType) {
        ArrayList<String> lst = new ArrayList<String>();
        WebElement el = verifyElementPresent(locator);
        Select dropdown = new Select(el);
        TestReporter.TRACE("clicking on dropdown options with locator " + locator);
        for (WebElement option : dropdown.getOptions()) {
            lst.add(option.getText());
        }
        return lst;
    }

    @Override
    public void selectByClickAndInput(By clickLocator, By inputLocator, String sValue) {
        click(clickLocator, TimeoutType.TWO_MINUTE);
        TestEnvironment.sleep(1);
        WebElement findElement = verifyElementPresent(inputLocator);
        inputText(findElement, sValue);
        TestEnvironment.sleep(2);
        findElement.sendKeys(Keys.RETURN);
    }

    @Override
    public void selectKendoUi(String sText, int iTableNumber) {
        iTableNumber = iTableNumber - 1;
        List<WebElement> findElements = getBrowser().getWebDriver().findElements(By.cssSelector("span.k-widget"));
        WebElement firstDropDown = findElements.get(iTableNumber);
        if (isClickable(firstDropDown)) ;
        clickKendoOptions(sText, firstDropDown);
    }

    private void clickKendoOptions(String sText, WebElement firstDropDown) {
        firstDropDown.click();
        List<WebElement> options = getBrowser().getWebDriver().findElements(By.cssSelector("div.k-list-container ul.k-list li.k-item"));
        for (Iterator<WebElement> iterator = options.iterator(); iterator.hasNext(); ) {
            WebElement webElement = (WebElement) iterator.next();
            if (webElement.getText().equals(sText)) {
                TestReporter.TRACE(" Selected item " + sText);
                webElement.click();
                TestEnvironment.sleep(2);
                return;
            }
        }
    }


    @Override
    public void selectKendoUi(String sText, String sAttributeAriaOwns) {
        List<WebElement> findElements = getBrowser().getWebDriver().findElements(By.cssSelector("span.k-widget"));
        for (Iterator<WebElement> iterator = findElements.iterator(); iterator.hasNext(); ) {
            WebElement webElement = (WebElement) iterator.next();
            String attribute = webElement.getAttribute("aria-owns");
            if (webElement != null && attribute != null && attribute.equals(sAttributeAriaOwns)) {
                clickKendoOptions(sText, webElement);
            }
        }
    }

    @Override
    public void upload(By locator, TimeoutType timeout, String sPath) {
        WebElement el = waitUntilClickable(locator, timeout);
        el.sendKeys(sPath);
        TestReporter.TRACE("Uploaded file : " + sPath);
    }

    @Override
    public void upload(By locator, String sPath) {
        WebElement el = waitUntilClickable(locator, TimeoutType.TWO_MINUTE);
        el.sendKeys(sPath);
        TestReporter.TRACE("Uploaded file : " + sPath);
    }

    @Override
    public void rightClick(By locator, int sNumKeyDown, TimeoutType timeout) {
        WebElement el = verifyElementPresent(locator, timeout);

        Actions contextClick = new Actions(getBrowser().getWebDriver()).contextClick(el);
        for (int i = 0; i < sNumKeyDown; i++) {
            contextClick.sendKeys(Keys.ARROW_DOWN);
        }
        contextClick.sendKeys(Keys.RETURN).build().perform();
        TestReporter.TRACE("Right clicked on element with locator " + locator);
    }

    @Override
    public boolean isElementSelected(By locator, String sAttributeName) {
        ISeleniumActions actions = getBrowser().getActions();
        String attribute = actions.getElement(locator).getAttribute(sAttributeName);
        return BooleanUtils.isTrue(attribute.equals("true"));
    }

    @Override
    public void rightClick(By locator, int sNumKeyDown) {
        WebElement el = verifyElementPresent(locator, TimeoutType.TWO_MINUTE);
        TestReporter.TRACE("Right clicked on element with locator " + locator);
        Actions contextClick = new Actions(getBrowser().getWebDriver()).contextClick(el);
        //contextClick.build().perform();
        for (int i = 0; i < sNumKeyDown; i++) {
            contextClick.sendKeys(Keys.ARROW_DOWN);
        }
        contextClick.sendKeys(Keys.RETURN).build().perform();
    }


    @Override
    public WebElement rightClick(By rightClicklocator, By optionLocator, TimeoutType timeout) {
        WebElement el = verifyElementPresent(rightClicklocator, timeout);
        TestReporter.TRACE("Right clicked on element with locator " + rightClicklocator);
        getActionsBuilder().contextClick(el).build().perform();
        WebElement el1 = getBrowser().getWebDriver().findElement(optionLocator);
        click(el1, timeout);
        TestReporter.TRACE("Clicked on option with locator" + optionLocator);
        return el;
    }

    @Override
    public WebElement rightClickOnly(By rightClicklocator, TimeoutType timeout) {
        WebElement el = verifyElementPresent(rightClicklocator, timeout);
        TestReporter.TRACE("Right clicked on element with locator " + rightClicklocator);
        getActionsBuilder().contextClick(el).build().perform();
        return el;
    }

    @Override
    public WebElement rightClickOnly(By rightClicklocator) {
        return rightClickOnly(rightClicklocator, TimeoutType.TWO_MINUTE);
    }

    @Override
    public WebElement clickNoWait(By locator) {
        final WebElement el = getElement(locator);
        if (!isClickable(el)) {
            TestReporter.TRACE("Element is not clickable: " + locator.toString());
        }
        el.click();
        TestReporter.TRACE("Clicked element with locator " + locator);
        return el;
    }

    @Override
    public String getTextWithJSFunc(By by) {
        WebElement el = getElement(by);
        JavascriptExecutor e = (JavascriptExecutor) webDriver();
        return (String) e.executeScript(String.format("return $('#%s').val();", el.getAttribute("id")));
    }

    @Override
    public void mouseHover(By locator) {
        WebElement el = getElement(locator);
        getActionsBuilder().moveToElement(el).build().perform();
    }

    @Override
    public void doubleclick(By locator) {
        WebElement el = getElement(locator);
        getActionsBuilder().doubleClick(el).build().perform();
    }

    @Override
    public boolean isElementClickable(By locator) {
        WebElement el = getElement(locator);
        try {
            el.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void dragAndDrop(By fromLocator, By toLocator) {
        WebElement From = waitUntilClickable(fromLocator, TimeoutType.TWO_MINUTE);
        WebElement To = waitUntilClickable(toLocator, TimeoutType.TWO_MINUTE);
        Action build = getActionsBuilder().clickAndHold(From)
                .moveToElement(To)
                .release(To)
                .build();

        build.perform();
    }


    @Override
    public WebElement click(By locator) {
        WebElement el = waitUntilClickable(locator, TimeoutType.FIVE_SECONDS);
        click(el, TimeoutType.TWO_MINUTE);
        TestReporter.TRACE("Clicked element with locator " + locator);
        return el;
    }

    @Override
    public WebElement click(By locator, TimeoutType timeout) {
        WebElement el = waitUntilClickable(locator, timeout);
        click(el, timeout);
        TestReporter.TRACE("Clicked element with locator " + locator);
        return el;
    }

    @Override
    public WebElement click(WebElement el, TimeoutType timeout) {
        final WebElement elm = waitUntilClickable(el, timeout);
        String tag = el.getTagName();
        seleniumCallable<WebElement> seleniumCallable = new seleniumCallable<WebElement>() {
            public WebElement call() {
                elm.click();
                return elm;
            }

            @Override
            public WebElement getObject() {
                return elm;
            }
        };
        return waitOnCallableAction(seleniumCallable, "unable to click on element " + tag);
    }

    @Override
    public WebElement clickAndVerifyPresent(By locatorToClick, By locatorToVerifyPresent, TimeoutType timeout) {
        click(locatorToClick, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyPresent + " to be present.");
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        return wait.until(ExpectedConditions.presenceOfElementLocated(locatorToVerifyPresent));
    }

    @Override
    public WebElement clickAndVerifyPresent(WebElement el, By locatorToVerifyPresent, TimeoutType timeout) {
        click(el, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyPresent + " to be present.");
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        return wait.until(ExpectedConditions.presenceOfElementLocated(locatorToVerifyPresent));
    }

    @Override
    public WebElement clickAndVerifyVisible(By locatorToClick, By locatorToVerifyVisible, TimeoutType timeout) {
        click(locatorToClick, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyVisible + " to be visible.");
        return verifyElementVisible(locatorToVerifyVisible, timeout);
    }

    @Override
    public WebElement clickAndVerifyVisible(WebElement el, By locatorToVerifyVisible, TimeoutType timeout) {
        click(el, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyVisible + " to be visible.");
        return verifyElementVisible(locatorToVerifyVisible, timeout);
    }

    @Override
    public void clickAndVerifyNotPresent(By locatorToClick, By locatorToVerifyNotPresent, TimeoutType timeout) {
        click(locatorToClick, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyNotPresent + " to NOT be present.");
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        //api give wrong value require something to fail this
        try {
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locatorToVerifyNotPresent));
            throw new RuntimeException(format("Error in clickAndVerifyNotPresent: locator  %s is present", locatorToVerifyNotPresent));
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void clickAndVerifyNotPresent(WebElement el, By locatorToVerifyNotPresent, TimeoutType timeout) {
        click(el, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyNotPresent + " to NOT be present.");
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        //api give wrong value require something to fail this
        try {
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locatorToVerifyNotPresent));
            throw new RuntimeException(format("Error in clickAndVerifyNotPresent: locator  %s is present", locatorToVerifyNotPresent));
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void clickAndVerifyNotVisible(By locatorToClick, By locatorToVerifyNotVisible, TimeoutType timeout) {
        click(locatorToClick, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyNotVisible + " to NOT be visible.");
        verifyElementInvisible(locatorToVerifyNotVisible, timeout);
    }

    @Override
    public void clickAndVerifyNotVisible(WebElement el, By locatorToVerifyNotVisible, TimeoutType timeout) {
        click(el, timeout);
        TestReporter.TRACE("After click, waiting for " + locatorToVerifyNotVisible + " to NOT be visible.");
        verifyElementInvisible(locatorToVerifyNotVisible, timeout);
    }

    @Override
    public void clickAndSelectFromList(By locatorToClick, By popoverLocator) {
        invokeMenuItemAndSelect(getElement(locatorToClick), popoverLocator);
    }

    @Override
    public void clickAndSelectFromList(WebElement clickable, By popoverLocator) {
        invokeMenuItemAndSelect(clickable, popoverLocator);
    }

    @Override
    public Object executeJavascript(String script) {
        TestReporter.TRACE("Executing javascript: " + script);
        try {
            return ((JavascriptExecutor) webDriver()).executeScript(script);
        } catch (Exception e) {
            throw new RuntimeException(format("Exception executing Javascript '%s':", script), e);
        }
    }

    @Override
    public void waitForJavascriptSymbolToBeDefined(final String symbol, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getPageLoadTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds, DEFAULT_POLL_MILLIS); //Check every 100ms
        wait.ignoring(StaleElementReferenceException.class);
        try {
            wait.until(new ExpectedCondition<Object>() {
                @Nullable
                @Override
                public Object apply(@Nullable WebDriver input) {
                    Object jsResult = executeJavascript(format("return (typeof %s != 'undefined') && (%s != null)", symbol, symbol));
                    TestReporter.TRACE("javascript result: " + jsResult);
                    return jsResult;
                }
            });
        } catch (TimeoutException e) {
            throw new RuntimeException(
                    format("Timeout waiting for javascript symbol '%s' to be defined with %d seconds timeout used", symbol, waitSeconds), e);
        }
        TestReporter.TRACE("Success verifying javascript symbol " + symbol + " is defined!");
    }

    @Override
    public void waitForJavascriptSymbolToHaveValue(final String symbol, final String value, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getPageLoadTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds, DEFAULT_POLL_MILLIS); //Check every 100ms
        wait.ignoring(StaleElementReferenceException.class);
        try {
            wait.until(new ExpectedCondition<Object>() {
                @Nullable
                @Override
                public Object apply(@Nullable WebDriver input) {
                    Object jsResult = executeJavascript(format("return (%s) === (%s)", symbol, value));
                    TestReporter.TRACE("javascript result: " + jsResult);
                    return jsResult;
                }
            });
        } catch (TimeoutException e) {
            throw new RuntimeException(
                    format("Timeout waiting for javascript symbol '%s' to have value '%s' with %d seconds timeout used", symbol, value, waitSeconds), e);
        }
        TestReporter.TRACE("Success verifying javascript symbol " + symbol + " has value " + value + "!");
    }

    @Override
    public boolean exists(By locator) {
        return exists(locator, 15);
    }

    @Override
    public boolean exists(By locator, int iRetries) {
        List<WebElement> elements = findElements(locator, null, iRetries);
        return elements.size() > 0;
    }

    @Override
    public boolean exists(By locator, WebElement parentEl) {
        List<WebElement> elements = findElements(locator, parentEl);
        return elements.size() > 0;
    }

    @Override
    public WebElement findElementContainingText(By locator, String text) {
        List<WebElement> matches = findElements(locator, null);
        for (WebElement el : matches) {
            try {
                if (el.getText().contains(text)) {
                    TestReporter.TRACE("SUCCESS: Found web element containing text " + text + " with locator " + locator + "");
                    return el;
                }
            } catch (Exception e) {

            }
        }
        return null;
    }

    @Override
    public WebElement findVisibleElementContainingText(By locator, String text) {
        List<WebElement> matches = findElements(locator, null);
        for (WebElement el : matches) {
            try {
                if (el.getText().contains(text) && el.isDisplayed()) {
                    TestReporter.TRACE("SUCCESS: Found visible web element containing text " + text + " with locator " + locator + "");
                    return el;
                }
            } catch (Exception e) {

            }
        }
        return null;
    }

    @Override
    public WebElement findVisibleElementContainingTextWithWait(final By locator, final String text, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        return wait.until(new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(@Nullable WebDriver input) {
                WebElement el = findVisibleElementContainingText(locator, text);
                if (el == null) {
                    WaitUtils.waitOneSecond();
                }
                return el;
            }
        });
    }

    @Override
    public WebElement findElementWithRefresh(final By locator, TimeoutType timeout) {
        return findElementContainingTextWithRefresh(locator, "", timeout);
    }

    @Override
    public WebElement findElementContainingTextWithRefresh(final By locator, final String text, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getPollingWithRefreshTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);

        TestReporter.TRACE("Waiting for element containing text " + text + " defined by locator " + locator + ", timeout of " + waitSeconds + " seconds");
        try {
            WebElement found = wait.until(new ExpectedCondition<WebElement>() {
                @Override
                public WebElement apply(@Nullable WebDriver input) {
                    long start = new Date().getTime();
                    while ((new Date().getTime() - start) / 1000 < timeoutsConfig.getPauseBetweenRefreshSeconds()) {
                        WebElement el = findElementContainingText(locator, text);
                        if (el != null) {
                            return el;
                        }
                        WaitUtils.waitMillis(timeoutsConfig.getPauseBetweenTriesMillis());
                    }
                    getBrowser().refreshPage();
                    return null;
                }
            });
            TestReporter.TRACE("Success finding element containing text " + text + " defined by locator " + locator);
            return found;
        } catch (TimeoutException e) {
            TestReporter.TRACE("Timeout waiting to find text " + text + " in an element matching locator " + locator);
            throw new TimeoutException(
                    format("Timeout waiting to find text '%s' in an element matching locator '%s'", text, locator));
        }
    }

    @Override
    public WebElement findVisibleElementWithRefresh(final By locator, TimeoutType timeout) {
        return findVisibleElementContainingTextWithRefresh(locator, "", timeout);
    }

    @Override
    public WebElement findVisibleElementContainingTextWithRefresh(final By locator, final String text, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getPollingWithRefreshTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        TestReporter.TRACE("Waiting for element containing text " + text + " defined by locator " + locator + ", timeout of " + waitSeconds + " seconds");
        try {
            WebElement found = wait.until(new ExpectedCondition<WebElement>() {
                @Override
                public WebElement apply(@Nullable WebDriver input) {
                    long start = new Date().getTime();
                    while ((new Date().getTime() - start) / 1000 < timeoutsConfig.getPauseBetweenRefreshSeconds()) {
                        WebElement el = findVisibleElementContainingText(locator, text);
                        if (el != null) {
                            return el;
                        }
                        WaitUtils.waitMillis(timeoutsConfig.getPauseBetweenTriesMillis());
                    }
                    getBrowser().refreshPage();
                    return null;
                }
            });
            TestReporter.TRACE("Success finding element containing text " + text + " defined by locator " + locator);
            return found;
        } catch (TimeoutException e) {
            TestReporter.TRACE("Timeout waiting to find text " + text + " in an element matching locator " + locator);
            throw new TimeoutException(
                    format("Timeout waiting to find text '%s' in an element matching locator '%s'", text, locator));
        }
    }

    @Override
    public WebElement findElementContainingTextWithWait(final By locator, final String text, TimeoutType timeout) {
        return null;
    }

    @Override
    @Nullable
    public WebElement getChildElement(By locator, WebElement parentEl) {
        List<WebElement> elements = findElements(locator, parentEl);
        if (elements.size() > 0) {
            return elements.get(0);
        }
        return null;
    }

    @Override
    @Nullable
    public WebElement getElement(By locator) {
        List<WebElement> elements = findElements(locator, null);
        if (elements.size() > 0) {
            return elements.get(0);
        }
        return null;
    }

    @Override
    @Nonnull
    public List<WebElement> getChildElements(By locator, WebElement parentEl) {
        return findElements(locator, parentEl);
    }

    @Override
    @Nonnull
    public List<WebElement> getElements(By locator) {
        return findElements(locator, null);
    }

    @Override
    @Nonnull
    public WebElement getElementWithWait(By locator) {
        return getChildElementWithWait(locator, null);
    }


    @Override
    @Nonnull
    public WebElement getChildElementWithWait(By locator, WebElement parentEl) {
        try {
            WebElement el = findElement(locator, parentEl);
            TestReporter.TRACE("Successfully found web element by locator " + locator + "");
            return el;
        } catch (NoSuchElementException e) {
            long implicitWait = browser.getImplicitWaitTimeoutMillis();
            throw new RuntimeException(format("Timeout using implicit wait of %d ms waiting to find web element with locator '%s' ", implicitWait, locator));
        }
    }

    @Override
    @Nonnull
    public WebElement getParentElement(WebElement el) {
        return el.findElement(By.xpath(".."));
    }

    @Override
    @Nonnull
    public WebElement inputText(By locator, String text, TimeoutType timeout) {
        TestReporter.TRACE("Inputting text " + text + " into element with locator " + locator + "");
        WebElement el = verifyElementPresent(locator, timeout);
        return inputText(el, text);
    }

    @Override
    @Nonnull
    public WebElement inputText(By locator, String text) {
        TestReporter.TRACE("Inputting text " + text + " into element with locator " + locator + "");
        WebElement el = verifyElementPresent(locator);
        return inputText(el, text);
    }

    @Override
    @Nonnull
    public WebElement inputText(@Nonnull WebElement el, String text) {
        TestReporter.TRACE("Inputting text " + text + " into web element " + el.getTagName());
        final WebElement elm = el;
        final String sText = text;
        seleniumCallable<WebElement> seleniumCallable = new seleniumCallable<WebElement>() {
            public WebElement call() {
                elm.sendKeys(sText);
                return elm;
            }

            @Override
            public WebElement getObject() {
                return elm;
            }
        };
        return waitOnCallableAction(seleniumCallable, "unable to inputting text " + text + " into web element " + el.getTagName());
    }

    //TODO
    @Override
    public WebElement inputTextAndSelectFromList(WebElement inputField, String value, By popoverLocator) {
        return inputTextAndSelectFromList(inputField, value, popoverLocator, 0);         // default is no retries
    }

    //TODO
    @Override
    public WebElement inputTextAndSelectFromList(WebElement inputField, String value, By popoverLocator, int withRetryCount) {
        return enterTextAndSelectFromList(inputField, value, popoverLocator, withRetryCount, false);
    }

    //TODO
    @Override
    public WebElement inputTextSlowly(By locator, String text) {
        WebElement el = getElementWithWait(locator);
        TestReporter.TRACE("Inputting text " + text + " into web element with locator " + locator + "");
        return inputTextSlowly(el, text);
    }

    //TODO
    @Override
    public WebElement inputTextSlowly(@Nonnull WebElement el, String text) {
        TestReporter.TRACE("Inputting text " + text + " slowly into web element " + el.getTagName());
        for (Character c : text.toCharArray()) {
            el.sendKeys(String.valueOf(c));
            try {
                Thread.sleep(timeoutsConfig.getPauseBetweenKeysMillis());
            } catch (InterruptedException e) {
                // don't care
            }
        }
        return el;
    }

    //TODO
    @Override
    public WebElement inputTextSlowlyAndSelectFromList(WebElement inputField, String value, By popoverLocator) {
        return inputTextSlowlyAndSelectFromList(inputField, value, popoverLocator, 0);      // default is no retries
    }

    //TODO
    @Override
    public WebElement inputTextSlowlyAndSelectFromList(WebElement inputField, String value, By popoverLocator,
                                                       int withRetryCount) {
        return enterTextAndSelectFromList(inputField, value, popoverLocator, withRetryCount, true);
    }

    @Override
    public void enterTextForAutoCompleteAndSelectFirstMatch(By inputLocator, String text, By popoverLocator,
                                                            String requiredPopupText) {
        enterTextForAutoCompleteAndSelectFirstMatch(inputLocator, 0, text, popoverLocator, requiredPopupText);
    }

    //TODO
    @Override
    public void enterTextForAutoCompleteAndSelectFirstMatch(By inputLocator, int minChars, String text, By popoverLocator,
                                                            String requiredPopupText) {

    }

    //TODO
    @Override
    public void inputTinyMceText(String text) {
        waitForTinyMceToBeReady();
        ((JavascriptExecutor) webDriver()).executeScript(format("tinyMCE.activeEditor.setContent(\"%s\")", text));
    }

    //TODO
    @Override
    public void waitForTinyMceToBeReady() {
        waitForJavascriptSymbolToBeDefined("tinyMCE", TimeoutType.DEFAULT);
        waitForJavascriptSymbolToBeDefined("tinyMCE.activeEditor", TimeoutType.DEFAULT);
        waitForJavascriptSymbolToHaveValue("tinyMCE.activeEditor.initialized", "true", TimeoutType.DEFAULT);
    }

    @Override
    public boolean isClickable(By locator) {
        WebElement el = verifyElementPresent(locator);
        if (el == null) {
            return false;
        }
        return isClickable(el);
    }

    //TODO
    @Override
    public boolean isClickable(WebElement el) {
        if (el == null) {
            return false;
        }
        try {
            if (!el.isDisplayed()) {
                //If not visible, element isn't clickable
                return false;
            }
            if (el.getSize().getHeight() <= 0 || el.getSize().getWidth() <= 0) {
                // If width or height is 0, element is not clickable
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isVisible(By locator) {
        WebElement el = verifyElementPresent(locator);
        return isVisible(el);
    }

    //TODO
    @Override
    public boolean isVisible(WebElement el) {
        if (el == null) {
            return false;
        }
        return el.isDisplayed() && el.getSize().getHeight() > 0 && el.getSize().getWidth() > 0;
    }

    @Override
    public void scrollIntoView(By locator) {
        WebElement el = verifyElementPresent(locator, TimeoutType.DEFAULT);
        scrollIntoView(el);
    }

    @Override
    public void scrollIntoView(WebElement el) {
        int scrollHeight = webDriver().manage().window().getSize().getHeight();
        int y = Math.max(0, el.getLocation().getY() - scrollHeight / 2); //Subtract half the window height so its in the middle of the viewable area.
        executeJavascript(format("window.scrollTo(%d, %d)", 0, y));
    }

    @Override
    public void scrollIntoView(By scrollContainerLocator, By locator) {
        WebElement parent = verifyElementPresent(scrollContainerLocator, TimeoutType.DEFAULT);
        WebElement el = verifyElementPresent(locator, TimeoutType.DEFAULT);
        int currentScrollTop = ((Long) executeJavascript(format("return $('%s').scrollTop()", scrollContainerLocator))).intValue();
        int y = el.getLocation().getY();
        int parentY = parent.getLocation().getY();
        int scrollTo = Math.max(0, y - parentY + currentScrollTop);
        executeJavascript(format("$('%s').scrollTop(%d)", scrollContainerLocator, scrollTo));
    }

    @Override
    public void scrollIntoView(By scrollContainerLocator, WebElement el) {
        WebElement parent = verifyElementPresent(scrollContainerLocator, TimeoutType.DEFAULT);
        int currentScrollTop = ((Long) executeJavascript(format("return $('%s').scrollTop()", scrollContainerLocator))).intValue();
        int y = el.getLocation().getY();
        int parentY = parent.getLocation().getY();
        int scrollTo = Math.max(0, y - parentY + currentScrollTop);
        executeJavascript(format("$('%s').scrollTop(%d)", scrollContainerLocator, scrollTo));
    }

    @SuppressWarnings("deprecation")
    @Override
    public void verifyElementContainsText(final By locator, final String text, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        wait.until(ExpectedConditions.textToBePresentInElement(locator, text));
        TestReporter.TRACE("SUCCESS: Verified element with Locator " + locator + " contains text " + text);
    }

    @Override
    public void verifyElementContainsText(final By locator, final String text) {
        verifyElementContainsText(locator, text, TimeoutType.TWO_MINUTE);
    }

    @Override
    public WebElement verifyElementPresent(By locator, TimeoutType timeout) {
        return waitAndCheckOnExpectedCondition(ExpectedConditions.presenceOfElementLocated(locator), "Failed to get the Web Element", timeout);
    }

    @Override
    public WebElement verifyElementPresent(By locator) {
        return verifyElementPresent(locator, TimeoutType.TWO_MINUTE);
    }

    @Override
    public void verifyElementNotPresent(By locator, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        TestReporter.TRACE("SUCCESS: Verified element with locator " + locator + " is NOT present");
    }

    @Override
    public void verifyElementNotPresent(By locator) {
        verifyElementNotPresent(locator, TimeoutType.TWO_MINUTE);
    }


    @Override
    public void verifyElementWithTextNotPresent(By locator, String text, TimeoutType timeout) {
        try {
            findElementContainingTextWithWait(locator, text, timeout);
            throw new RuntimeException(
                    format("Error in verifyElementWithTextNotPresented: found element with locator '%s' containing text '%s'!", locator, text));
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void verifyElementSelected(By locator, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getClickTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        wait.until(ExpectedConditions.elementSelectionStateToBe(locator, true));
    }

    @Override
    public void verifyElementSelected(WebElement el, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getClickTimeoutSeconds(), timeout);
        String errorMessage = format("Failure in verifyElementSelected: Element '%s' never became selected after %d seconds!",
                el.getTagName(), waitSeconds);
        waitAndCheckOnExpectedCondition(ExpectedConditions.elementToBeSelected(el), errorMessage, timeout);
    }

    @Override
    public void verifyElementNotSelected(By locator, TimeoutType timeout) {
        //String errorMessage = "Element did not get selected";
        int waitSecond = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSecond);
        wait.until(ExpectedConditions.elementSelectionStateToBe(locator, false));
    }

    @Override
    public void verifyElementNotSelected(WebElement el, TimeoutType timeout) {
        String errorMessage = "Element did not get selected";
        waitAndCheckOnExpectedCondition(ExpectedConditions.elementSelectionStateToBe(el, false), errorMessage, timeout);
    }

    @Override
    public WebElement verifyElementVisible(final By locator, TimeoutType timeout) {
        int waitSecond = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSecond);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    @Override
    public void verifyElementInvisible(By locator, TimeoutType timeout) {
        int waitSecond = getTimeout(timeoutsConfig.getWebElementPresenceTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSecond);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		/*String sErrorMsg = format("Failure in verifyElementInvisible waiting for element with locator '%s' to be invisible", locator);
		waitOnExpectedCondition(ExpectedConditions.invisibilityOfElementLocated(locator), sErrorMsg, timeout);*/
    }

    @Override
    public void verifyElementWithTextIsInvisible(By locator, String text, TimeoutType timeout) {
        try {
            findVisibleElementContainingTextWithWait(locator, text, timeout);
            throw new RuntimeException(
                    format("Error in verifyElementWithTextIsInvisible: found element by locator '%s' containing text '%s'", locator, text));
        } catch (Exception e) {
            return; // OK - we didn't find a visible element containing the given text
        }
    }

    @Override
    public WebElement verifyPageRefreshed(WebElement elementFromBeforeRefresh, By locatorAfterRefresh, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getPageRefreshTimeoutSeconds(), timeout);
        TestReporter.TRACE("Waiting for locator " + locatorAfterRefresh + " to be present after page refreshes, using timeout of " + waitSeconds + " seconds");
        waitOnExpectedConditionForSeconds(ExpectedConditions.stalenessOf(elementFromBeforeRefresh),
                "Timeout waiting for web element to become stale (waiting for page to reload).",
                waitSeconds);
        TestReporter.TRACE("Verified web element became stale (page is reloading).");
        WebElement el = verifyElementPresent(locatorAfterRefresh, TimeoutType.DEFAULT);
        TestReporter.TRACE("Successfully verified page refreshed by finding web element with locator " + locatorAfterRefresh + ".");
        return el;
    }

    @Override
    public <T, V> V waitOnFunction(Function<T, V> function, T input, String message, TimeoutType timeout) {
        return null;

    }

    @Override
    public <T> void waitOnPredicate(Predicate<T> predicate, T input, String message, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getMediumTimeoutSeconds(), timeout);
        FluentWait<T> fluentWait = new FluentWait<T>(input)
                .withTimeout(waitSeconds, TimeUnit.SECONDS)
                .pollingEvery(DEFAULT_POLL_MILLIS, TimeUnit.MILLISECONDS)
                .ignoring(NotFoundException.class)
                .ignoring(StaleElementReferenceException.class);
        fluentWait.until(predicate);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void waitOnPredicate(@SuppressWarnings("rawtypes") Predicate predicate, String message, TimeoutType timeout) {
        waitOnPredicate(predicate, new Object(), message, timeout);
    }

    @Override
    public <T> void waitOnPredicateWithRefresh(final Predicate<T> predicate, final T input, String message, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getMediumTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds, DEFAULT_POLL_MILLIS);
        wait.ignoring(StaleElementReferenceException.class);
        TestReporter.TRACE("Waiting on expected condition, using timeout of " + waitSeconds + " seconds");
        wait.until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(@Nullable WebDriver webDriver) {
                if (predicate.apply(input)) {
                    return true;
                }
                return false;
            }
        });
    }

    //TODO
    @Override
    public void waitOnPredicateWithRefresh(@SuppressWarnings("rawtypes") final Predicate predicate, String message, TimeoutType timeout) {
    }

    @Override
    public <T> T waitOnExpectedCondition(ExpectedCondition<T> expectedCondition, String message, TimeoutType timeout) {
        int waitSeconds = getTimeout(timeoutsConfig.getClickTimeoutSeconds(), timeout);
        WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
        wait.ignoring(StaleElementReferenceException.class);
        TestReporter.TRACE("SUCCESS: Verified element with locator " + message + " is NOT selected");
        return wait.until(expectedCondition);
    }

    @Override
    public <T> T waitAndCheckOnExpectedCondition(ExpectedCondition<T> expectedCondition, String message, TimeoutType timeout) {
        final ExpectedCondition<T> cond = expectedCondition;
        final TimeoutType lTimeoutType = timeout;
        seleniumCallable<T> callable = new seleniumCallable<T>() {
            T result = null;

            public T call() {
                int waitSeconds = getTimeout(timeoutsConfig.getClickTimeoutSeconds(), lTimeoutType);
                WebDriverWait wait = new WebDriverWait(webDriver(), waitSeconds);
                wait.ignoring(StaleElementReferenceException.class);
                result = wait.until(cond);
                TestReporter.TRACE("waiting for condition to be satisfied with timeout :" + lTimeoutType);
                return result;
            }

            public T getObject() {
                return result;
            }
        };
        return waitOnCallableAction(callable, message);
    }


    private <T> T waitOnCallableAction(seleniumCallable<T> callable, String sMessage) {
        boolean isSuccess = Check.checkBusyWait(new SeleniumActionPredicate(), callable, 10, 10);
        if (!isSuccess) {
            TestReporter.FATAL(sMessage);
        }
        return callable.getObject();
    }

    //TODO
    private <T> T waitOnExpectedConditionForSeconds(ExpectedCondition<T> expectedCondition, String message, int timeout) {
        return null;
    }

    @Override
    public WebElement waitUntilClickable(By locator, TimeoutType timeout) {
        return waitAndCheckOnExpectedCondition(ExpectedConditions.elementToBeClickable(locator), "Failed to get the Web Element", timeout);
    }

    @Override
    public WebElement waitUntilClickable(final WebElement el, TimeoutType timeout) {
        return waitAndCheckOnExpectedCondition(ExpectedConditions.elementToBeClickable(el), "Failed to get the Web Element", timeout);
    }

    @Override
    public CustomTimeouts getTimeoutsConfig() {
        return timeoutsConfig;
    }

    protected int getTimeout(int defaultTimeout, TimeoutType timeout) {
        return timeout == TimeoutType.DEFAULT ? defaultTimeout : timeoutsConfig.getTimeoutInSeconds(timeout);
    }

    protected List<WebElement> findElements(By locator, WebElement parentEl) {
        return findElements(locator, parentEl, 15);
    }

    protected List<WebElement> findElements(By locator, WebElement parentEl, int iNumberOfRetries) {
        int withRetryCount = iNumberOfRetries;
        List<WebElement> findElements = Lists.newArrayList();
        do {
            if (parentEl == null) {
                findElements = webDriver().findElements(locator);
            } else {
                findElements = parentEl.findElements(locator);
            }
            withRetryCount--;
            TestEnvironment.sleep(1);
        } while (CollectionUtils.isEmpty(findElements) && withRetryCount > 0);
        return findElements;
    }

    protected WebElement findElement(By locator, WebElement parentEl) {
        int withRetryCount = 14;
        WebElement findElement = null;
        do {

            if (parentEl == null) {
                findElement = webDriver().findElement(locator);
            } else {
                findElement = parentEl.findElement(locator);
            }
            withRetryCount--;
            TestEnvironment.sleep(1);
        } while (findElement == null && withRetryCount > 0);
        return findElement;
    }

    protected WebElement enterTextAndSelectFromList(WebElement inputField, String value, By popoverLocator,
                                                    int withRetryCount, boolean slowly) {
        boolean done = false;
        int initialCount = withRetryCount;
        do {
            try {
                enterTextAndSelectFromList(inputField, value, popoverLocator, slowly);
                done = true;
            } catch (Exception ex) {
                TestReporter.TRACE("Caught an exception " + ex.getMessage());
            }
            withRetryCount--;
        } while (!done && withRetryCount > 0);

        // Need to subtract 1, so that we have 0 retries if we succeeded on the first try.
        int numberOfUsedRetries = initialCount - withRetryCount - 1;
        if (numberOfUsedRetries > 0) {

        }
        if (!done) {
            //to be handle
        }
        return inputField;
    }

    protected void enterTextAndSelectFromList(WebElement inputField, String value, By popoverLocator, boolean slowly) {
        clearText(inputField);
        if (slowly) {
            inputTextSlowly(inputField, value);
        } else {
            inputText(inputField, value);
        }
        verifyElementPresent(popoverLocator, TimeoutType.DEFAULT);
        click(popoverLocator, TimeoutType.DEFAULT);
    }

    protected void invokeMenuItemAndSelect(WebElement clickable, By popoverLocator) {
        Preconditions.checkNotNull(clickable, "Input WebElement cannot be null");
        waitUntilClickable(clickable, TimeoutType.DEFAULT);
        click(clickable, TimeoutType.DEFAULT);
        verifyElementPresent(popoverLocator, TimeoutType.DEFAULT);
        waitUntilClickable(popoverLocator, TimeoutType.DEFAULT);
        click(popoverLocator, TimeoutType.DEFAULT);
    }

    @Override
    public boolean doesElementHaveClass(By locator, String locatorClass) {
        WebElement el = verifyElementPresent(locator, TimeoutType.DEFAULT);
        return WebElementHelpers.webElementHasClass(el, locatorClass);
    }

    @Override
    public WebElement verifyElementHasClass(final By locator, final String locatorClass, TimeoutType timeout) {
        return waitOnFunction(new Function<ISeleniumActions, WebElement>() {
                                  @Nullable
                                  @Override
                                  public WebElement apply(ISeleniumActions input) {
                                      WebElement el = input.verifyElementPresent(locator, TimeoutType.DEFAULT);
                                      if (WebElementHelpers.webElementHasClass(el, locatorClass)) {
                                          return el;
                                      }
                                      return null;
                                  }
                              }, this,
                format("Waiting for element that matches locator '%s' to have class '%s'", locator, locatorClass),
                timeout);
    }

    @Override
    public WebElement verifyElementDoesNotHaveClass(final By locator, final String locatorClass, TimeoutType timeout) {
        return waitOnFunction(new Function<ISeleniumActions, WebElement>() {
                                  @Nullable
                                  @Override
                                  public WebElement apply(ISeleniumActions input) {
                                      WebElement el = input.verifyElementPresent(locator, TimeoutType.DEFAULT);
                                      if (!WebElementHelpers.webElementHasClass(el, locatorClass)) {
                                          return el;
                                      }
                                      return null;
                                  }
                              }, this,
                format("Waiting for element that matches locator '%s' to NOT have class '%s'", locator, locatorClass),
                timeout);
    }

    @Override
    public int getDivsCount(String parentDiv, String childDiv) {
        WebElement webElement = getBrowser().getActions().getElement(By.xpath(parentDiv));
        int taskCount = 0;
        int increment = 1;
        try {
            while (webElement.findElements(By.xpath(childDiv + "[" + increment + "]")).size() > 0) {
                taskCount++;
                increment++;
            }
            return taskCount;
        } catch (NoSuchElementException exception) {
            return 0;
        }
    }
}