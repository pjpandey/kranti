package com.testing.kranti.framework;

public abstract class AbstractNonStartableTestComponent extends AbstractTestComponent {


    protected AbstractNonStartableTestComponent(TestComponentData dData) {
        super(dData);
    }


    @Override
    public void start() {
    }


    @Override
    public void stop() {
    }

}
