package com.testing.kranti.framework.operation.http;

import com.testing.kranti.framework.operation.OpResult;

import java.util.List;
import java.util.Map;

public class HttpOpResponse implements OpResult {

    private int exitStatus;
    private List<String> stdOut;
    private List<String> stdErr;
    private long executionTime;
    private Map<String, String> headers;
    private String responseBody;


    @Override
    public int getExitStatus() {
        return exitStatus;
    }

    @Override
    public List<String> getStdOut() {
        return stdOut;
    }

    @Override
    public List<String> getStdErr() {
        return stdErr;
    }

    @Override
    public long getExecutionTime() {
        return executionTime;
    }

    @Override
    public String toStringAsOneLine() {
        return null;
    }
}
