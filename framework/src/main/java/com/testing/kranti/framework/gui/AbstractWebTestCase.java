package com.testing.kranti.framework.gui;

import com.testing.kranti.framework.AbstractTestCase;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.TestComponent;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

//@KrantiDontCleanLogOnPass
public abstract class AbstractWebTestCase extends AbstractTestCase {
    private Browser m_browser;
    private final TestComponentData.Builder m_builder;
    private List<TestComponent> m_lstUnstableComps = new ArrayList<>();

    protected abstract List<TestComponent> setUnstableCompsSpecific();

    protected abstract AbstractWebTestComponent getWebServer();

    protected abstract CustomTimeouts getCustomTimeout();

    protected abstract String getBaseurl();

    protected abstract void launchGui();

    protected AbstractWebTestCase(String aApplication) {
        super(aApplication);
        m_builder = new TestComponentData.Builder();
    }

    private void initBrowser() {
        TestCommandExecution.setenv("LOG_FILE", getSandBoxDir());
        TestComponentData data = m_builder.build(getWindowsHosts().get(0), getInstallationPath(getApplication(), "browser", 0), "");
        m_browser = BrowserFactory.getSelector().getBrowser(data, getBaseurl(), getSeleniumHubUrl(), getWebBrowserType(), getCustomTimeout());
        getWebServer().setBrowser(m_browser);
    }

    @Override
    public List<? extends TestComponent> getTestComponents() {
        List<TestComponent> lstComponents = new ArrayList<TestComponent>();
        if (getBrowser() != null)
            lstComponents.add(getBrowser());
        return lstComponents;
    }

    protected Browser getBrowser() {
        return m_browser;
    }


    private List<TestComponent> getUnstableComponents() {
        return m_lstUnstableComps;
    }

    @Override
    protected void initComponents() {
        initBrowser();
        m_lstUnstableComps = setUnstableCompsSpecific();
    }

    @Override
    protected void tearDownComponentsSpecific() {
        super.tearDownComponentsSpecific();
        m_browser.getActions().executeJavascript("window.localStorage.clear();");
        m_browser.stop();
        checkClientException();
    }

    private void checkClientException() {
    }

    @Before
    public void setSpecific() {
        launchGui();
    }
}