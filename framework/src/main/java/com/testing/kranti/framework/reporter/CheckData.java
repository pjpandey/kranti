package com.testing.kranti.framework.reporter;

public class CheckData {

    private boolean m_bFirstCheck;
    private boolean m_bInCheck;

    public void setFirstCheck(boolean firstCheck) {
        m_bFirstCheck = firstCheck;
    }

    public boolean isReportingCheck() {
        return m_bFirstCheck;
    }

    public boolean isInCheck() {
        return m_bInCheck;
    }

    public void setInCheck(boolean inCheck) {
        m_bInCheck = inCheck;
    }

}
