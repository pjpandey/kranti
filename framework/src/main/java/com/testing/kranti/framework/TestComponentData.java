package com.testing.kranti.framework;


public class TestComponentData
{
	private String m_sInstallationDir;
	private String m_sHost;
	private String m_sUpgradedInstallationDir;
	private String m_sUpgradedConfigurationDir;
	private String m_sDowngradedInstallationDir;
	private String m_sDowngradedConfigurationDir;
	private String m_sConfigurationDir;
	private String m_sPersistencyRoot;
	private String m_sLogDir;
	private int m_iNumOfComponentInList;

	private TestComponentData(Builder builder) 
	{
		m_sInstallationDir = builder.m_sInstallationDir;
		m_sConfigurationDir = builder.m_sConfigurationDir;
		m_sPersistencyRoot = builder.m_sPersistencyRoot;
		m_iNumOfComponentInList = builder.m_iNumOfComponentInList;
		m_sUpgradedInstallationDir = builder.m_sUpgradedInstallationDir;
		m_sUpgradedConfigurationDir = builder.m_sUpgradedConfigurationDir;
		m_sDowngradedInstallationDir = builder.m_sDowngradedInstallationDir;
		m_sDowngradedConfigurationDir = builder.m_sDowngradedConfigurationDir;
		m_sHost = builder.m_sHost;
		m_sLogDir = builder.m_sLogDir;
	}

	public static class Builder
	{
		private String m_sInstallationDir = null;
		private String m_sHost = null;
		private String m_sUpgradedInstallationDir = null;
		private String m_sUpgradedConfigurationDir = null;
		private String m_sDowngradedInstallationDir = null;
		private String m_sDowngradedConfigurationDir = null;
		private String m_sConfigurationDir = null;
		private String m_sPersistencyRoot = "/tmp/" + System.getenv("USER") + "/tester_persistency/persistency";
		private String m_sLogDir = null;
		private int m_iNumOfComponentInList = 0;
		
		public Builder()
		{
		}
		public Builder(String persistencyRoot)
		{
			m_sPersistencyRoot = persistencyRoot;
		}

		public TestComponentData build(String sHost, String sInstallationPath, String sConfigurationPath)
		{
			return build(sHost, sInstallationPath, sConfigurationPath, m_sPersistencyRoot);
		}

		public TestComponentData build(String sHost, String sInstallationPath, String sConfigurationPath, String sPersistencyRoot)
		{
			return build(sHost, sInstallationPath, sConfigurationPath, sPersistencyRoot, 0);
		}

		public TestComponentData build(String sHost, String sInstallationPath, String sConfigurationPath, String sPersistencyRoot, int numOfComponent)
		{
			return build(sHost, sInstallationPath, sConfigurationPath, sPersistencyRoot, null,null,null,null, numOfComponent);
		}

		public TestComponentData build(String sHost, String sInstallationPath, String sConfigurationPath,String sUpgradedInstallationPath,String sUpgradedConfigurationPath, String sDowngradedInstallationPath,String sDowngradedConfigurationPath)
		{
			return build(sHost, sInstallationPath, sConfigurationPath, m_sPersistencyRoot, sUpgradedInstallationPath, sUpgradedConfigurationPath, sDowngradedInstallationPath,sDowngradedConfigurationPath);
		}

		public TestComponentData build(String sHost, String sInstallationPath, String sConfigurationPath, String sPersistencyRoot,String sUpgradedInstallationPath,String sUpgradedConfigurationPath, String sDowngradedInstallationPath,String sDowngradedConfigurationPath)
		{
			return build(sHost, sInstallationPath, sConfigurationPath, sPersistencyRoot, sUpgradedInstallationPath, sUpgradedConfigurationPath,sDowngradedInstallationPath,sDowngradedConfigurationPath, 0);
		}

		public TestComponentData build(String sHost, String sInstallationPath, String sConfigurationPath, String sPersistencyRoot, String sUpgradedInstallationPath,String sUpgradedConfigurationPath,String sDowngradedInstallationPath,String sDowngradedConfigurationPath, int numOfComponent)
		{
			
			m_sHost = sHost;
			m_sInstallationDir = sInstallationPath;
			m_sConfigurationDir = sConfigurationPath;
			m_sPersistencyRoot = sPersistencyRoot;
			m_iNumOfComponentInList = numOfComponent;
			m_sUpgradedInstallationDir = sUpgradedInstallationPath;
			m_sUpgradedConfigurationDir = sUpgradedConfigurationPath;
			m_sDowngradedInstallationDir = sDowngradedInstallationPath;
			m_sDowngradedConfigurationDir = sDowngradedConfigurationPath;
			return build();
		}
		
		public TestComponentData build()
		{
			return new TestComponentData(this);
		}
		
		public Builder setUpgradedInstallationDir(String upgradedInstallationDir)
		{
			m_sUpgradedInstallationDir = upgradedInstallationDir;
			return this;
		}
		public Builder setDowngradedInstallationDir(String downgradedInstallationDir)
		{
			m_sDowngradedInstallationDir = downgradedInstallationDir;
			return this;
		}
		public Builder setInstallationDir(String installationDir)
		{
			m_sInstallationDir = installationDir;
			return this;
		}
		public Builder setHost(String host)
		{
			m_sHost = host;
			return this;
		}
		public Builder setConfigurationDir(String configurationDir)
		{
			m_sConfigurationDir = configurationDir;
			return this;
		}
		public Builder setDowngradedConfigurationDir(String downgradedConfigurationDir)
		{
			m_sDowngradedConfigurationDir = downgradedConfigurationDir;
			return this;
		}
		public Builder setUpgradedConfigurationDir(String upgradedConfigurationDir)
		{
			m_sUpgradedConfigurationDir = upgradedConfigurationDir;
			return this;
		}
		public Builder setNumOfComponentInList(int numOfComponentInList)
		{
			m_iNumOfComponentInList = numOfComponentInList;
			return this;
		}
		public Builder setPersistencyRoot(String persistencyRoot)
		{
			m_sPersistencyRoot = persistencyRoot;
			return this;
		}
	}

	public String getUpgradedInstallationDir()
	{
		return m_sUpgradedInstallationDir;
	}


	public String getDowngradedInstallationDir()
	{
		return m_sDowngradedInstallationDir;
	}
	public void setUpgradedInstallationDir(String sUpgardedInstallationPath)
	{
		m_sUpgradedInstallationDir=sUpgardedInstallationPath;
	}


	public String getInstallationDir()
	{
		return m_sInstallationDir;
	}

	public void setInstallationDir(String installationDir)
	{
		m_sInstallationDir = installationDir;
	}

	public String getHost()
	{
		return m_sHost;
	}

	public void setHost(String host)
	{
		m_sHost = host;
	}

	public String getConfigurationDir()
	{
		return m_sConfigurationDir;
	}

	public void setConfigurationDir(String configurationDir)
	{
		m_sConfigurationDir = configurationDir;
	}

	public String getPersistencyRoot()
	{
		return m_sPersistencyRoot;
	}

	public void setPersistencyRoot(String persistencyRoot)
	{
		m_sPersistencyRoot = persistencyRoot;
	}

	public String getLogDir()
	{
		return m_sLogDir;
	}

	public void setLogDir(String logDir)
	{
		m_sLogDir = logDir;
	}

	public void setNumOfComponentInList(int numOfComponentInList)
	{
		m_iNumOfComponentInList = numOfComponentInList;
	}

	public int getNumOfComponentInList()
	{
		return m_iNumOfComponentInList;
	}
	public String getUpgradedConfigurationDir()
	{
		return m_sUpgradedConfigurationDir;
	}

	public void setUpgradedConfigurationDir(String upgradedConfigurationDir)
	{
		m_sUpgradedConfigurationDir = upgradedConfigurationDir;
	}

	public String getDowngradedConfigurationDir()
	{
		return m_sDowngradedConfigurationDir;
	}

	public void setDowngradedConfigurationDir(String downgradedConfigurationDir)
	{
		m_sDowngradedConfigurationDir = downgradedConfigurationDir;
	}


	@Override
	public String toString()
	{
		return "TestComponentData [m_sHost=" + m_sHost + ", m_iNumOfComponentInList=" + m_iNumOfComponentInList + "]";
	}

	
}
