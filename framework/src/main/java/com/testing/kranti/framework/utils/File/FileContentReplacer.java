package com.testing.kranti.framework.utils.File;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FileContentReplacer {
    private static final Logger log = Logger.getLogger(FileContentReplacer.class);
    private static boolean RENAME_LINK_TARGETS = true;
    private final Map<String, String> m_mpReplace = new HashMap<>();// new OrderMap();
    private final Map<String, String> m_mpReplaceEntiresLines = new HashMap<>();// new OrderMap();
    private boolean m_verifyNoLeftovers;

    public FileContentReplacer(boolean verifyNoLeftovers) {
        m_verifyNoLeftovers = verifyNoLeftovers;
    }

    public FileContentReplacer() {
        this(true);
    }

    private boolean m_bFollowLinks = true;
    private boolean m_bAddSpaceToEveryLine = true;

    private Map<String, String> getReplace() {
        return m_mpReplace;
    }

    private Map<String, String> getReplaceEntiresLines() {
        return m_mpReplaceEntiresLines;
    }

    public void addReplacement(String replace, String with) {
        m_mpReplace.put(replace, with);
    }

    public void addEntireLineReplacement(String startsWith, String replaceLineWith) {
        m_mpReplaceEntiresLines.put(startsWith, replaceLineWith);
    }

    public void execute(File file) throws FileNotFoundException, IOException {
        /*Map<String, String> mp = new OrderedMap<String, String>();
        mp.putAll(getReplace());
        renameAndReplaceContent(file, mp);*/
    }

    private void renameAndReplaceContent(File file, Map<String, String> mp) throws FileNotFoundException, IOException {
        /*Set<String> trimmedParams;
        trimmedParams = StringUtils.trimAll(mp.keySet());
        if (file.isFile()) {
            String fromContent = TextFileUtil.getContents(file);
            if (isAddSpaceToEveryLine()) {
                fromContent = fromContent.replace("\n", " \n") + " ";
            }
            for (Entry<String, String> e : getReplaceEntiresLines().entrySet()) {
                fromContent = fromContent.replaceAll(".*" + e.getKey() + ".*", e.getValue());
            }
            String replaceParameters = StringParser.replaceParameters(fromContent, mp, "", "");
            TextFileUtil.setContents(file, replaceParameters);
            if (m_verifyNoLeftovers) {
                for (String string : trimmedParams) {
                    if (replaceParameters.contains(string)) {
                        for (Entry<String, String> e : mp.entrySet()) {
                            if (e.getKey().trim().equals(string)) {
                                throw new RuntimeException(
                                        string
                                                + "["
                                                + e.getKey()
                                                + "] should have been replaced with "
                                                + e.getValue()
                                                + " since it is in map but is not trimmed in file "
                                                + file.getPath());
                            }
                        }
                        throw new RuntimeException(
                                string
                                        + " should have been replaced since it is in map but is not trimmed in file "
                                        + file.getPath());
                    }
                }
            }
        }*/
        /*if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                File renamedFile = new File(f.getParent(), StringParser.replaceParameters(f.getName(), mp, "", ""));
                if (!renamedFile.equals(f)) {
                    FileUtil.delete(renamedFile);
                    f.renameTo(renamedFile);
                }
                if (FileUtil.isFollowedLink(renamedFile, isFollowLinks())) {
                    if (RENAME_LINK_TARGETS && Files.isSymbolicLink(renamedFile.toPath())) {
                        File target = new File(Files.readSymbolicLink(renamedFile.toPath()).toString());
                        File renamedTarget = new File(target.getParent(), StringParser.replaceParameters(target.getName(), mp, "", ""));
                        if (!renamedTarget.equals(target)) {
                            FileUtil.delete(renamedFile);
                            FileUtil.createLink(renamedFile.toString(), renamedTarget.toString());
                        }
                    }
                    renameAndReplaceContent(renamedFile, mp);
                }
                if (m_verifyNoLeftovers) {
                    if (trimmedParams.contains(renamedFile.getName())) {
                        throw new RuntimeException(
                                renamedFile.getName()
                                        + " should have been replaced since it is in map but is not trimmed in directory "
                                        + file.getPath());
                    }
                }
            }
        }*/
    }

    public void setFollowLinks(boolean followLinks) {
        m_bFollowLinks = followLinks;
    }

    public void setAddSpaceToEveryLine(boolean val) {
        m_bAddSpaceToEveryLine = val;
    }

    public boolean isAddSpaceToEveryLine() {
        return m_bAddSpaceToEveryLine;
    }

    public boolean isFollowLinks() {
        return m_bFollowLinks;
    }
}
