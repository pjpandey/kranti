package com.testing.kranti.framework.reporter;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.testing.kranti.framework.TestCase;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.operation.commmand.CommandRequest;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.utils.File.FileUtil;
import junit.framework.Assert;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.*;

import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

public class TestReporter {

    private static final String STDOUT_APPENDER = "STDOUT_APPENDER";
    private static final String FILE_APPENDER = "FILE_APPENDER";
    private static List<String> lstIgnoreErrMsg = Lists.newArrayList();
    private static TestCase testCase = null;
    final static String COMMANDS_DIR = "commands";
    private final static String TEST_LOG = "test.log";
    private static ErrorReporter reporter = null;
    private static boolean bFatalOnFail = false;
    private static boolean bReconfigLog = true;
    private static Map<String, TestReporterAppender> appenders = newHashMap();

    static {
        if (RegressionEnvironment.isRegression()) {
            IOutputFileStrategy strategy = new SingleFileStrategy();
            appenders.put(FILE_APPENDER, new TestFileAppender(strategy));
            appenders.put(STDOUT_APPENDER, new TestOutputStreamAppender(strategy));
        } else {
            IOutputFileStrategy strategy = new SeparateFilePerCommandStrategy();
            appenders.put(FILE_APPENDER, new TestFileAppender(strategy));
            appenders.put(STDOUT_APPENDER, new TestConsoleAppender(strategy));
        }
    }

    public static void redirectOutput(OutputStream oOut) {
        ((TestOutputStreamAppender) (appenders.get(STDOUT_APPENDER))).redirectOutput(oOut);
        bReconfigLog = true;
    }

    public static void init(TestCase tTest) {
        testCase = tTest;
        initOutputDir();
        initAppenders();
        TraceLink("INIT: running with base dir ", tTest.getSandBoxDir());
        TRACE("Java home: " + System.getProperty("java.home"));
        initErrorReporter();
    }

    private static void initErrorReporter() {
        reporter = new ErrorReporter(getTestCase(), getOutputDir());
    }

    public static void start() {
        logMessage("START: " + getTestCase().getTestName());
    }

    public static void finish() {
        if (!testFailed() && !TestEnvironment.shoudKeepLog()) {
            //TestEnvironment.deleteOnAllHosts(getOutputDir());
            FileUtil.delete(getOutputDir());
        } else {
            reporter.dump();
            TraceLink("LOCAL BUGS DIR: ", getOutputDir());
        }

        logMessage("END: " + getTestCase().getTestName());
        logMessage("MULTYTEST:: " + reporter.getPass() + " PASSED " + reporter.getFail() + " FAILED " + reporter.getFatal() + " FATAL");

        if (testFailed()) {
            if (testFailedNoFatal()) {
                Assert.fail(reporter.getFirstFailure());
            } else {
                if (reporter.getFatalThrowable() instanceof RuntimeException) {
                    throw (RuntimeException) reporter.getFatalThrowable();
                }
                if (reporter.getFatalThrowable() instanceof Error) {
                    throw (Error) reporter.getFatalThrowable();
                }
                throw new RuntimeException(reporter.getFatalThrowable());
            }
        }
    }

    public static void TRACE(String sMessage) {
        logMessage(sMessage);
    }

    public static void TRACE(Throwable e) {
        String message = e.getMessage() == null ? e.toString() : e.getMessage();
        logMessage("exception caught: " + message, e);
    }

    public static void TraceLink(String sMessage, String link) {
        if (!RegressionEnvironment.isRegression()) {
            link = "file://" + link;
        }
        logMessage(sMessage + " " + link);
    }

    public static void PASS(String sMessage) {
        logMessage("PASS : " + sMessage);
        reporter.pass();
    }

    public static void FAIL(String sMessage) {
        if (bFatalOnFail) {
            FATAL(sMessage);
            return;
        } else {
            logMessage("FAIL : " + sMessage, true);
            reporter.fail(sMessage);
        }
    }

    public static void FAIL(String... sMessage) {
        FAIL(String.join("\n", sMessage));
    }

    public static void FATAL(Throwable e) {
        if (ignoredErrorMessageExist(e.getMessage())) {
            return;
        }
        String message = e.getMessage() == null ? e.toString() : e.getMessage();
        logMessage("FATAL ERROR: " + message, e);
        fatal(e);
    }


    public static TestFailException FATAL(String sMessage) {
        if (ignoredErrorMessageExist(sMessage)) {
            return null;
        }

        TestFailException t = new TestFailException(sMessage);
        logMessage("FATAL ERROR: " + sMessage, t);
        return fatal(t);
    }

    private static TestFailException fatal(Throwable t) {
        reporter.fatal(t);
        if (getTestCase().isRunning()) {
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            }
            if (t instanceof Error) {
                throw (Error) t;
            }
            throw new TestFailException(t);
        }
        return null;
    }

    private static void logMessage(String sMessage) {
        logMessage(sMessage, false);
    }

    private static boolean ignoredErrorMessageExist(String sMessage) {
        for (String errMsg : lstIgnoreErrMsg) {
            if (StringUtils.contains(sMessage, errMsg)) {
                return true;
            }
        }
        return false;
    }


    private static void initAppenders() {
        if (!bReconfigLog) {
            return;
        }
        Logger.getRootLogger().removeAllAppenders();
        for (TestReporterAppender appender : appenders.values()) {
            appender.init();
        }

        try {
            BasicConfigurator.configure(new FileAppender(new TTCCLayout("ISO8601"), getOutputDir() + File.separator + TEST_LOG));
        } catch (Exception e) {
        }
        Logger.getRootLogger().setLevel(Level.INFO);
        bReconfigLog = false;
    }

    private static TestCase getTestCase() {
        return testCase;
    }

    private static void initOutputDir() {
        new File(getOutputDir()).mkdirs();
        new File(getOutputDir() + File.separator + COMMANDS_DIR).mkdir();
    }

    static String getOutputDir() {
        return getTestCase().getSandBoxDir();
    }

    public static boolean testFailed() {
        return ((reporter.getFail() + reporter.getFatal() > 0) || (reporter.getPass() <= 0));
    }

    public static boolean testFailedNoFatal() {
        return ((reporter.getFail() > 0 && reporter.getFatal() == 0) &&
                testWithValidResults()
        );
    }

    private static boolean testWithValidResults() {
        return reporter.getFail() + reporter.getFatal() + reporter.getPass() > 0;
    }

    public static void setFatalOnFail() {
        bFatalOnFail = true;
    }

    public static void addIgnoredErrorMessge(String errMsg) {
        Preconditions.checkNotNull(errMsg);
        lstIgnoreErrMsg.add(errMsg);
    }

    private static void logMessage(String sMessage, boolean bError) {
        for (TestReporterAppender appender : appenders.values()) {
            appender.logMessage(sMessage, bError);
        }
    }

    private static void logMessage(String sMessage, Throwable t) {
        for (TestReporterAppender appender : appenders.values()) {
            appender.logMessage(sMessage, t);
        }
    }

    public static void traceCommandExecution(CommandRequest cRequest, OpResult rResult) {
        for (TestReporterAppender appender : appenders.values()) {
            appender.traceCommandExecution(cRequest, rResult);
        }
    }

    public static void traceWebRequestExcution(String url, String cRequest, int iStatusCode, StringBuffer bOutput) {
        for (TestReporterAppender appender : appenders.values()) {
            appender.traceWebRequestExecution(url, cRequest, iStatusCode, bOutput);
        }
    }

    public static ErrorReporter getErrorReporter() {
        return reporter;
    }

    public static void disable() {
        appenders.clear();
    }


}
