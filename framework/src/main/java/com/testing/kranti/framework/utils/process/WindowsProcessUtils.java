package com.testing.kranti.framework.utils.process;

import com.google.common.collect.Lists;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.ArrayList;
import java.util.List;

public class WindowsProcessUtils extends AbstractProcessUtils implements IProcessUtils {

    public final static String PS_CMD = "c:\\Windows\\System32\\tasklist";
    public final static String CMD_CMD = "C:\\Windows\\System32\\cmd.exe";
    public final static String KILL_CMD = "taskkill /f /pid ";

    private List<Integer> findMatchingProcessIDs(String sHost, String sKey, String... matches) {
        TestReporter.TRACE("looking for matches: " + String.join(",", matches));
        String[] arrCmd = {CMD_CMD, "/C", PS_CMD, "/FO", "CSV", "/NH", "/FI", "\"" + sKey + " eq " + String.join(",", matches) + "\""};
        List<String> cmdOut = runCommandNoException(sHost, arrCmd);
        List<Integer> pid = new ArrayList<Integer>();
        for (String sLine : cmdOut) {
            try {
                String sPID = sLine.split(",")[1];
                pid.add(Integer.parseInt(sPID.substring(1, sPID.length() - 1)));
            } catch (Exception e) {
            }
        }
        return pid;
    }

    public Integer getRunningServiceProcessPID(String sHost, String serviceName) {
        TestReporter.TRACE("Looking for Windows WSM PID on " + sHost);
        String line = "(get-wmiobject win32_service | where { $_.name -eq '<SERVICE>'}).processID";
        line = line.replace("<SERVICE>", serviceName);
        List<String> cmdOut = runPowerShell(sHost, line);
        if (cmdOut.isEmpty()) {
            return 0;
        }
        int parseInt = Integer.parseInt(cmdOut.get(0));
        TestReporter.TRACE("Found PID: " + parseInt);
        return parseInt;
    }
	
	/*@Override
	public List<Integer> getProcessID(String sHost, String... matches)
	{
		return findMatchingProcessIDs (sHost, "IMAGENAME", matches);
	}*/

    private List<String> runPowerShell(String sHost, String cmd) {
        ArrayList<String> lstCmd = Lists.newArrayList("c:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe", "-command");
        lstCmd.add("\"" + cmd + "\"");
        return runCommandNoException(sHost, lstCmd.toArray(new String[lstCmd.size()]));
    }

    public int getProcessPIDByNameAndPathFilter(String sHost, String sProcessName, String sExecutablePathFilter) {
        String command = "(get-wmiobject win32_process | where { $_.ProcessName -eq '<PROCNAME>' } | where { $_.ExecutablePath -like '*<EXECPATH>*' }).ProcessID";
        command = command.replace("<PROCNAME>", sProcessName);
        command = command.replace("<EXECPATH>", sExecutablePathFilter);
        List<String> cmdOut = runPowerShell(sHost, command);
        if (cmdOut.size() != 1) {
            return 0;
        }
        return Integer.parseInt(cmdOut.get(0));
    }

    @Override
    public List<String> getChildProcessIDs(String sHost, Integer sPPID) {
        String line = "get-wmiobject win32_process | where { $_.ParentProcessId -eq <PPID> } | foreach { $_.ProcessID }";
        line = line.replace("<PPID>", sPPID.toString());
        List<String> pids = Lists.newArrayList();
        for (String $ : runPowerShell(sHost, line)) {
            int pid = Integer.parseInt($);
            pids.add($);
            pids.addAll(getChildProcessIDs(sHost, pid));
        }
        return pids;
    }

    @Override
    public List<String> getChildProcessIDUsingPPiD(String sHost, Integer sPPID) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer getppid(String sHost, Integer pid) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getpgid(String sHost, Integer pid) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean processRunning(String sHost, int iPid) {
        List<Integer> lst = findMatchingProcessIDs(sHost, "PID", Integer.toString(iPid));
        return !lst.isEmpty();
    }

    @Override
    public String getProcessStatus(String sHost, int iPid) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getProcessField(String sHost, int iPid, String sField) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void killProcess(String sHost, int... pids) {
        for (int $ : pids) {
            if ($ <= 0) {
                continue;
            }
            String line = "Stop-Process -Force " + $;
            ;
            runPowerShell(sHost, line);
        }
    }

    @Override
    public void sendSignal(String sHost, int iPid, String iSignal) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void suspendProcess(String sHost, int... pids) {

    }

    @Override
    public void resumeProcess(String sHost, int... pids) {

    }

    @Override
    protected String normalizeHost(String sHost) {
        return TestEnvironment.getFullHost(sHost);
    }

    @Override
    public List<String> getProcessTable(String sHost) {
        ArrayList<String> cmd = Lists.newArrayList("c:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe", "-command");
        String line = "\"get-wmiobject win32_process |  Select-Object ProcessName,ProcessID,CommandLine | ft -hidetableheaders -Property * -AutoSize | Out-String -Width 250\"";
        cmd.add(line);
        String[] arrCmd = cmd.toArray(new String[cmd.size()]);
        return runCommandNoException(sHost, arrCmd);
    }

    @Override
    public List<String> getSleepCommand(String sHost, String numOfSec) {
        List<String> lst = Lists.newArrayList();
        lst.add(CMD_CMD);
        lst.add("/c");
        lst.add("C:\\Windows\\system32\\PING.EXE -n " + numOfSec + " 127.0.0.1");
        return lst;
    }

    @Override
    public Long getCpuUtilizationOfProcess(String sHost, String sMatches) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ArrayList<Integer> getThreadIDs(String sHost, String sPPID) {
        throw new UnsupportedOperationException();
    }


    @Override
    public int getCpuListOfProcess(String sHost, String sProcessId) {
        throw new UnsupportedOperationException();
    }

}
