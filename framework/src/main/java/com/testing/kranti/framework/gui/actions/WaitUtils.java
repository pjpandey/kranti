package com.testing.kranti.framework.gui.actions;

//this class must be removed using sleep is prohibited
public class WaitUtils {

    public static void waitMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // Don't care.
        }
    }

    public static void waitSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            // Don't care.
        }
    }

    public static void waitOneSecond() {
        waitSeconds(1);
    }

    public static void waitFiveSeconds() {
        waitSeconds(5);
    }
}
