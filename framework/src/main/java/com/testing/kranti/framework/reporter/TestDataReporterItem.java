
package com.testing.kranti.framework.reporter;

import com.google.common.collect.Lists;

import java.util.List;

public class TestDataReporterItem {

    private boolean stoBeReported;
    private boolean isGeneric;
    private boolean increaseType;
    private Double sValue;
    private String sKey;
    private int sCount;
    private Double sAvg;
    private Double sMin;
    private Double sMax;

    public TestDataReporterItem(String sKey, Double sValue, boolean toBeReported, boolean isGeneric, boolean increaseType) {
        this.sKey = sKey.toLowerCase();
        this.sValue = sValue;
        stoBeReported = toBeReported;
        sCount = 1;
        sAvg = sValue;
        sMin = sValue;
        sMax = sValue;
        this.isGeneric = isGeneric;
        this.increaseType = increaseType;
    }

    public String getKey() {
        return sKey;
    }

    public Double getValue() {
        return sValue;
    }

    public boolean shouldBeReported() {
        return stoBeReported;
    }

    public void settoBeReported(boolean m_val) {
        stoBeReported = m_val;
    }

    public boolean getIsGeneric() {
        return isGeneric;
    }

    public void setIsGeneric(boolean m_val) {
        isGeneric = m_val;
    }

    public boolean getIncreaseType() {
        return increaseType;
    }

    public void setIncreaseType(boolean m_val) {
        increaseType = m_val;
    }


    public void updateItem(double newValue) {
        sValue = newValue;
        sAvg = ((sAvg * sCount) + newValue) / (sCount + 1);
        sCount++;
        sMax = (sMax < sValue) ? sValue : sMax;
        sMin = (sMin > sValue) ? sValue : sMin;

    }

    public List<TestDataReporterItem> getAllSubDataReporterItems() {
        List<TestDataReporterItem> lstSubData = Lists.newArrayList();
        lstSubData.add(new TestDataReporterItem(sKey + ".avg", sAvg, false, isGeneric, increaseType));
        lstSubData.add(new TestDataReporterItem(sKey + ".max", sMax, false, isGeneric, increaseType));
        lstSubData.add(new TestDataReporterItem(sKey + ".min", sMin, false, isGeneric, increaseType));
        lstSubData.add(this);
        return lstSubData;
    }

}
