package com.testing.kranti.framework.rules.env;

import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KrantiMandatoryEnvRule extends AbstractMethodRule
{
	private ArrayList<String> m_lstEnv=new ArrayList<>();
	@Override
	protected void extractTestProperties(FrameworkMethod mTest, Object oTest) 
	{
		extractSpecificConfiguration(oTest.getClass().getAnnotation(KrantiMandatoryEnv.class), mTest.getAnnotation(KrantiMandatoryEnv.class));
	}
	private void extractSpecificConfiguration(KrantiMandatoryEnv... arrEnv)
	{
		for (KrantiMandatoryEnv eEnv : arrEnv)
		{
			if (null != eEnv)
			{
				m_lstEnv.addAll(Arrays.asList(eEnv.value()));
			}
		}
	}
	public List<String> getMandatoryEnv()
	{
		return m_lstEnv;
	}
}