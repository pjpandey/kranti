package com.testing.kranti.framework.rules;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public abstract class AbstractMethodRule implements MethodRule {

	@Override
	public final Statement apply(final Statement arg0, FrameworkMethod arg1, Object arg2) 
	{
		extractTestProperties(arg1, arg2);
		return arg0;
	}

	protected abstract void extractTestProperties(FrameworkMethod mTest, Object oTest);
}
