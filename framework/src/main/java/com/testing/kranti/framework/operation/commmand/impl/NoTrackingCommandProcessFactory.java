package com.testing.kranti.framework.operation.commmand.impl;

import java.io.File;
import java.io.IOException;

class NoTrackingCommandProcessFactory implements CommandProcessFactory {


    @Override
    public NoTrackingCommandProcess createAndExec(String[] cmd, File cwd, String[] env, String user) throws IOException {
        NoTrackingCommandProcess ret = new NoTrackingCommandProcess();
        ret.exec(cmd, cwd, env, user);
        return ret;
    }
}