package com.testing.kranti.framework.rules.exceptions;

import com.google.common.collect.Sets;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

import java.util.Set;

public class KrantiIgnoredLeakRule extends AbstractMethodRule {
    private Set<String> m_ignoredLeaks = Sets.newHashSet();
    public boolean isLeakCheckRequired = false;

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        isLeakCheckRequired(mTest.getMethod().isAnnotationPresent(KrantiIgnoredAllLeaks.class) || oTest.getClass().isAnnotationPresent(KrantiIgnoredAllLeaks.class));
        extract(oTest.getClass().getAnnotation(KrantiIgnoredLeaks.class), mTest.getAnnotation(KrantiIgnoredLeaks.class));
    }

    private void isLeakCheckRequired(boolean annotationPresent) {
        isLeakCheckRequired = annotationPresent;
    }

    private void extract(KrantiIgnoredLeaks... arrExceptions) {
        for (KrantiIgnoredLeaks exceptions : arrExceptions) {
            if (null != exceptions) {
                for (String s : exceptions.value()) {
                    m_ignoredLeaks.add(s);
                }
            }
        }
    }

    public Set<String> getIgnoredLeaks() {
        return m_ignoredLeaks;
    }

}
