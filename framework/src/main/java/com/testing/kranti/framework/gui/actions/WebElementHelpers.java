package com.testing.kranti.framework.gui.actions;

import com.google.common.collect.Lists;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WebElementHelpers {

    public static boolean webElementHasClass(WebElement webElement, String className) {
        return getClasses(webElement).contains(className);
    }

    public static List<String> getClasses(WebElement webElement) {
        if (webElement.getAttribute("class") == null) {
            return Lists.newArrayList();
        }
        return Lists.newArrayList(webElement.getAttribute("class").split("\\s+"));
    }
}