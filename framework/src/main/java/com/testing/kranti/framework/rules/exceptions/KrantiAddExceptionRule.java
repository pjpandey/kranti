package com.testing.kranti.framework.rules.exceptions;

import com.google.common.collect.Sets;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

import java.util.Set;

public class KrantiAddExceptionRule extends AbstractMethodRule {
    private Set<Class> additionalException = Sets.newHashSet();

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        extractExceptions(oTest.getClass().getAnnotation(KrantiAddExceptionClass.class), mTest.getAnnotation(KrantiAddExceptionClass.class));
    }

    private void extractExceptions(KrantiAddExceptionClass... arrExceptions) {
        for (KrantiAddExceptionClass exceptions : arrExceptions) {
            if (null != exceptions) {
                for (Class cls : exceptions.value()) {
                    additionalException.add(cls);
                }
            }
        }
    }

    public Set<Class> getKrantiAdditionalException() {
        return additionalException;
    }

}

