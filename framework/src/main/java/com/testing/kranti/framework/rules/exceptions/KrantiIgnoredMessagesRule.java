package com.testing.kranti.framework.rules.exceptions;

import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiIgnoredMessagesRule extends AbstractMethodRule {

    private String ignoredMessages = "";

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        extractExceptions(mTest.getAnnotation(KrantiIgnoredMessages.class));
    }

    private void extractExceptions(KrantiIgnoredMessages messages) {
        if (messages != null) {
            ignoredMessages = messages.value();
        }
    }

    public String getIngnoredMessages() {
        return ignoredMessages;
    }

}
