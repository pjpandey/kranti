package com.testing.kranti.framework.configuration.kranti;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ComponentInstallation {
    @JsonProperty("installationpath")
    private List<String> paths = new ArrayList<String>();
    @JsonProperty("upgradedinstallationpath")
    private List<String> upgradedPaths = new ArrayList<String>();
    @JsonProperty("downgradedinstallationpath")
    private List<String> downgradedPaths = new ArrayList<String>();
    @JsonProperty("name")
    private String name;

    @JsonProperty("installationpath")
    public void setInstallationPath(List<String> paths) {
        this.paths = paths;
    }

    @JsonProperty("installationpath")
    public List<String> getInstallationPath() {
        return paths;
    }

    @JsonProperty("upgradedinstallationpath")
    public void setUpgradedInstallationPath(List<String> paths) {
        upgradedPaths = paths;
    }

    @JsonProperty("upgradedinstallationpath")
    public List<String> getUpgradedInstallationPath() {
        return upgradedPaths;
    }

    @JsonProperty("downgradedinstallationpath")
    public void setDowngradedInstallationPath(List<String> paths) {
        downgradedPaths = paths;
    }

    @JsonProperty("downgradedinstallationpath")
    public List<String> getDowngradedInstallationPath() {
        return downgradedPaths;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setPaths(String name) {
        this.name = name;
    }
}
