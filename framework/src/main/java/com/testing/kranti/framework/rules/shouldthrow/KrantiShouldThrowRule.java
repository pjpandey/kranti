package com.testing.kranti.framework.rules.shouldthrow;

import com.testing.kranti.framework.reporter.TestReporter;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class KrantiShouldThrowRule implements MethodRule {

    @Override
    public Statement apply(final Statement arg0, FrameworkMethod arg1, Object arg2) {
        KrantiShouldThrow shouldThrow = arg1.getAnnotation(KrantiShouldThrow.class);
        if (shouldThrow == null) {
            return arg0;
        }
        final String shouldContain = shouldThrow.contains();
        TestReporter.addIgnoredErrorMessge(shouldContain);
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    arg0.evaluate();
                } catch (Throwable exception) {

                    String message = exception.getMessage();
                    if (null != message && message.toLowerCase().contains(shouldContain.toLowerCase())) {
                        return;
                    }
                }
                throw new ThisTestShouldHaveThrownException();
            }
        };
    }

}
