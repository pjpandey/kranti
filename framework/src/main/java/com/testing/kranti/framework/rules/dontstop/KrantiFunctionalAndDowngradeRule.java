package com.testing.kranti.framework.rules.dontstop;

import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiFunctionalAndDowngradeRule extends AbstractMethodRule {

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        if (RegressionEnvironment.isDowngradeRegression()) {
            boolean bFunctionalAndDowngrade = (null != mTest.getAnnotation(KrantiFunctionalAndDowngradeTest.class)) || (null != oTest.getClass().getAnnotation(KrantiFunctionalAndDowngradeTest.class)) || (null != oTest.getClass().getPackage().getAnnotation(KrantiFunctionalAndDowngradeTest.class));
            TestEnvironment.setComponentsDowngradeRequired(bFunctionalAndDowngrade);
        }
    }

}
