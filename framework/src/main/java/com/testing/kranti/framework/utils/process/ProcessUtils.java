package com.testing.kranti.framework.utils.process;

import com.google.common.primitives.Ints;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.check.Check;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProcessUtils {
    private static WindowsProcessUtils windowsHost = new WindowsProcessUtils();
    private static UnixProcessUtils unixHost = new UnixProcessUtils();

    public static List<Integer> getProcessID(String sHost, String... matches) {
        TestReporter.TRACE("looking for matches: " + String.join(",", matches));

        List<String> colProcesses = getSpecificUtils(sHost).getProcessTable(sHost);
        for (String sMatch : matches) {
            if (null == sMatch) {
                continue;
            }
            final String sMatchLC = sMatch.toLowerCase();
            colProcesses = colProcesses.stream().filter(sProcess -> sProcess.toLowerCase().contains(sMatchLC)).collect(Collectors.toList());
            TestReporter.TRACE("remaining processes: " + colProcesses + " after matching " + sMatch);
        }

        return colProcesses.stream().map(sProcess -> {
            String[] arrParts = sProcess.split(" ");
            String sProcID = arrParts[1];
            return Integer.parseInt(sProcID);
        }).collect(Collectors.toList());
    }

    private static IProcessUtils getSpecificUtils(String sHost) {
        return TestEnvironment.isWindowsHost(sHost) ? windowsHost : unixHost;
    }

    public static List<String> getChildProcessIDs(String sHost, Integer sPPID) {
        return getSpecificUtils(sHost).getChildProcessIDs(sHost, sPPID);
    }

    public static List<String> getChildProcessIDUsingPPiD(String sHost, Integer sPPID) {
        return getSpecificUtils(sHost).getChildProcessIDUsingPPiD(sHost, sPPID);
    }

    public static Integer getppid(String sHost, Integer pid) {
        return getSpecificUtils(sHost).getppid(sHost, pid);
    }


    public static String getpgid(String sHost, Integer pid) {
        return getSpecificUtils(sHost).getpgid(sHost, pid);
    }


    public static boolean processExistence(String sHost, int iPid) {
        return processRunning(sHost, iPid);
    }

    public static boolean processRunning(String sHost, int iPid) {
        return getSpecificUtils(sHost).processRunning(sHost, iPid);
    }

    public static String getProcessStatus(String sHost, int iPid) {
        return getSpecificUtils(sHost).getProcessStatus(sHost, iPid);
    }

    public static String getProcessField(String sHost, int iPid, String sField) {
        return getSpecificUtils(sHost).getProcessField(sHost, iPid, sField);

    }

    public static void killProcess(String sHost, String sKeyword) {
        List<Integer> pids = getProcessID(sHost, sKeyword);
        killProcess(sHost, Ints.toArray(pids));
    }

    public static void killProcess(String sHost, int... pids) {
        getSpecificUtils(sHost).killProcess(sHost, pids);
    }

    public static void sendSignal(String sHost, int iPid, int iSignal) {
        sendSignal(sHost, iPid, Integer.toString(iSignal));
    }

    public static void sendSignal(String sHost, int iPid, String iSignal) {
        getSpecificUtils(sHost).sendSignal(sHost, iPid, iSignal);
    }

    public static void sendSignal(String sHost, List<Integer> pidList, String iSignal) {
        for (Integer iPid : pidList) {
            sendSignal(sHost, iPid, iSignal);
        }
    }

    public static void suspendProcess(String sHost, int... pids) {
        getSpecificUtils(sHost).suspendProcess(sHost, pids);
    }

    public static void resumeProcess(String sHost, int... pids) {
        getSpecificUtils(sHost).resumeProcess(sHost, pids);
    }


    public static void verifyProcessIsRunning(final Boolean sValue, final String sHost, final String sPath) {
        verifyProcessIsRunning(sValue, sHost, sPath, 60);
    }

    public static void verifyProcessIsRunning(final Boolean sValue, final String sHost, final String sPath, int iTimeout) {
        Check.assertBusyWait(val -> {
            List<Integer> sProcessPID = getProcessID(sHost, sPath);
            Boolean sStatus = sProcessPID.size() > 0;
            return sStatus == sValue;
        }, "Process is not running", iTimeout, 3);
    }

    public static void verifyProcessIsRunning(final Boolean sValue, final String sHost, final int sProcessID) {
        verifyProcessIsRunning(sValue, sHost, sProcessID, 30);
    }

    public static void verifyProcessIsRunning(final Boolean sValue, final String sHost, final int sProcessID, final int iTimeout) {
        Check.assertBusyWait(val -> {
            Boolean sStatus = processRunning(sHost, sProcessID);
            return sStatus == sValue;
        }, "Process is not running", iTimeout, 3);
    }

    public static List<String> getSleepCommand(String sHost, String numOfSec) {
        return getSpecificUtils(sHost).getSleepCommand(sHost, numOfSec);
    }

    public static int getCpuListOfProcess(String sHost, String sPID) {
        return getSpecificUtils(sHost).getCpuListOfProcess(sHost, sPID);
    }

    public static Long getCpuUtilizationOfProcess(String sHost, String sMatches) {
        return getSpecificUtils(sHost).getCpuUtilizationOfProcess(sHost, sMatches);
    }

    public static ArrayList<Integer> getThreadIDs(String sHost, String sPPID) {
        return getSpecificUtils(sHost).getThreadIDs(sHost, sPPID);
    }
}

