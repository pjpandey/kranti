package com.testing.kranti.framework.utils.process;

import java.util.ArrayList;
import java.util.List;

public interface IProcessUtils {

    List<String> getChildProcessIDs(String sHost, Integer sPPID);

    List<String> getChildProcessIDUsingPPiD(String sHost, Integer sPPID);

    Integer getppid(String sHost, Integer pid);

    String getpgid(String sHost, Integer pid);

    boolean processRunning(String sHost, int iPid);

    String getProcessStatus(String sHost, int iPid);

    String getProcessField(String sHost, int iPid, String sField);

    void killProcess(String sHost, int... pids);

    void sendSignal(String sHost, int iPid, String iSignal);

    void suspendProcess(String sHost, int... pids);

    void resumeProcess(String sHost, int... pids);

    List<String> getProcessTable(String sHost);

    List<String> getSleepCommand(String sHost, String numOfSec);

    Long getCpuUtilizationOfProcess(String sHost, String Matches);

    ArrayList<Integer> getThreadIDs(String sHost, String sPPID);

    int getCpuListOfProcess(String sHost, String sProcessId);
}