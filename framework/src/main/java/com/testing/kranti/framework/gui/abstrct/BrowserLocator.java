package com.testing.kranti.framework.gui.abstrct;


public class BrowserLocator {
    private static Browser s_browser;

    public static void setBrowser(Browser browser) {
        s_browser = browser;
    }

    public static Browser getBrowser() {
        return s_browser;
    }
}
