package com.testing.kranti.framework.reporter;

import com.testing.kranti.framework.operation.commmand.CommandRequest;
import com.testing.kranti.framework.operation.OpResult;

public interface TestReporterAppender {

    void logMessage(String sMessage, boolean bError);

    void logMessage(String sMessage, Throwable t);

    void traceCommandExecution(CommandRequest cRequest, OpResult rResult);

    void traceWebRequestExecution(String sUrl, String sRequest, int iStatusCode, StringBuffer bOutput);

    void init();
}
