package com.testing.kranti.framework.operation.http;

import com.testing.kranti.framework.operation.OpRequest;

import java.net.URLConnection;
import java.util.Map;
import java.util.Optional;

public class HttpOpRequest implements OpRequest {

    private String url;
    private HttpOppRequestType requestType;
    private Map<String,String>  headers;
    private Optional<String> requestBody;


    public HttpOpRequest(String url, HttpOppRequestType requestType, Map<String, String> headers, Optional<String> requestBody) {
        this.url = url;
        this.requestType = requestType;
        this.headers = headers;
        this.requestBody = requestBody;
    }

    public String getUrl() {
        return url;
    }

    public HttpOppRequestType getRequestType() {
        return requestType;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Optional<String> getRequestBody() {
        return requestBody;
    }
}

enum HttpOppRequestType {
    GET,POST,PUT,DELETE
}
