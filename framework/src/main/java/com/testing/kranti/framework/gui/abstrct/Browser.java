package com.testing.kranti.framework.gui.abstrct;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.testing.kranti.framework.*;
import com.testing.kranti.framework.gui.actions.ISeleniumActions;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import com.testing.kranti.framework.gui.utils.UriUtils;
import com.testing.kranti.framework.operation.commmand.AbstractCommandOperation;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.utils.File.KrantiFileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public abstract class Browser extends AbstractTestComponent {
    private WebDriver m_webDriver;
    private String m_sBaseTestUrl;
    private CustomTimeouts m_eTimeouts;
    private String m_sSeleniumHubUrl;

    private final Optional<String> m_osWebDriverPath;
    private final Optional<String> m_osBrowserBinaryPath;
    private final Optional<String> m_osBrowserVersion;
    private boolean m_bRunning = false;

    protected Browser(TestComponentData data, String baseTestUrl, CustomTimeouts timeouts, Optional<String> webDriverPath, Optional<String> browserBinaryPath, Optional<String> browserVersion, String seleniumHubURL) {
        super(data);
        this.m_sBaseTestUrl = Preconditions.checkNotNull(baseTestUrl);
        this.m_eTimeouts = timeouts;
        this.m_osWebDriverPath = webDriverPath;
        this.m_osBrowserBinaryPath = browserBinaryPath;
        this.m_osBrowserVersion = browserVersion;
        this.m_sSeleniumHubUrl = seleniumHubURL;
    }

    public abstract WebBrowserType getBrowserType();

    public abstract DesiredCapabilities getDesiredCapabilities();

    public void initializeBrowser() {
        this.m_webDriver = startWebDriver();
        this.m_webDriver.manage().timeouts().implicitlyWait(getImplicitWaitTimeoutMillis(), TimeUnit.MILLISECONDS);
    }

    public String getBaseTestUrl() {
        return m_sBaseTestUrl;
    }

    public String getSeleniumHubUrl() {
        return m_sSeleniumHubUrl;
    }


    public CustomTimeouts getTimeouts() {
        return m_eTimeouts;
    }

    protected abstract WebDriver startWebDriver();

    public WebDriver getWebDriver() {
        return m_webDriver;
    }

    public long getPageTimeoutSeconds() {
        return m_eTimeouts.getPageLoadTimeoutSeconds();
    }

    public long getImplicitWaitTimeoutMillis() {
        return m_eTimeouts.getImplicitWaitTimeoutMillis();
    }


    /**
     * Save a screenshot in PNG format to given file name.
     */
    public File saveScreenshotToFile(String filename) throws IOException {
        TakesScreenshot screenshotDriver;
        screenshotDriver = ((TakesScreenshot) getWebDriver());
        File scrFile = screenshotDriver.getScreenshotAs(OutputType.FILE);
        File outFile = new File(filename);
        KrantiFileUtils.copyFile(scrFile, outFile);
        return outFile;
    }

    public void quit() {
        if (null == m_webDriver) {
            return;
        }
        //m_webDriver.close();
        try {
            Thread.sleep(5000);
            m_webDriver.quit();
        } catch (Exception e) {
        }
    }

    public void open(String href) {
        String fullURIStr = UriUtils.uri(m_sBaseTestUrl, "/", href);
        TestReporter.TRACE("Opening web page by URL " + fullURIStr);
        getWebDriver().get(fullURIStr);
    }

    public void refreshPage() {
        m_webDriver.navigate().refresh();
    }

    public void cleanSession() {
        m_webDriver.manage().deleteAllCookies();
    }

    public Optional<String> getWebDriverPath() {
        return m_osWebDriverPath;
    }

    public Optional<String> getBrowserBinaryPath() {
        return m_osBrowserBinaryPath;
    }

    public Optional<String> getBrowserVersion() {
        return m_osBrowserVersion;
    }


    @Override
    protected String getPersistencyType() {
        return null;
    }


    @Override
    protected void clean() {
    }


    @Override
    protected AbstractCommandOperation getStartOperation() {
        return null;
    }


    @Override
    protected AbstractCommandOperation getStopOperation() {
        return null;
    }


    @Override
    protected AbstractCommandOperation getHupOperation() {
        return null;
    }


    @Override
    public void start() {
        stop();
        new Thread() {
            @Override
            public void run() {
                try {
                    WebBrowserType browserType = getBrowserType();
                    if (browserType == WebBrowserType.CHROME) {
                        String[] arr = {"c:\\\\KrantiGUITesting\\\\start_selenium_chrome.bat"};
                        TestCommandExecution.runCommand(arr, getHost()/*, null, TimeUnitsTranslator.fromMinutes(10)*/);
                    } else if (browserType == WebBrowserType.FIREFOX) {
                        String[] arr = {"c:\\\\KrantiGUITesting\\\\start_selenium_firefox.bat"};
                        TestCommandExecution.runCommand(arr, getHost()/*, null, TimeUnitsTranslator.fromMinutes(10)*/);
                    }
                } catch (Throwable t) {
                }
            }
        }.start();
        TestEnvironment.sleep(5);
        m_bRunning = true;

        initializeBrowser();
    }

    @Override
    public void stop() {
        quit();
        TestCommandExecution.runCommand("c:\\\\KrantiGUITesting\\\\kill_selenium.bat", getHost());
        TestCommandExecution.runCommand("c:\\\\KrantiGUITesting\\\\kill_all_browser.bat", getHost());
        m_bRunning = false;
    }

    @Override
    public boolean isRunning() {
        return m_bRunning;
    }

    @Override
    public void initRunningComponent() {
        start();
    }

    public abstract ISeleniumActions getActions();
}
