package com.testing.kranti.framework.gui;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.abstrct.WebBrowserType;
import com.testing.kranti.framework.gui.browserItem.ChromeBrowser;
import com.testing.kranti.framework.gui.browserItem.FirefoxBrowser;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import com.testing.kranti.framework.reporter.TestReporter;

class BrowserBuilder {

    private final WebBrowserType m_BrowserType;
    private final String m_sBaseTestUrl;
    private final String m_sSeleniumHubURL;

    private CustomTimeouts m_eTimeoutsConfig;

    private Optional<String> m_osBrowserVersion = Optional.absent();

    private BrowserBuilder(WebBrowserType browserType, String baseTestUrl, String seleniumHubURL) {
        this.m_BrowserType = Preconditions.checkNotNull(browserType, "You must provide a non-null BrowserType!");
        this.m_sBaseTestUrl = Preconditions.checkNotNull(baseTestUrl, "You must provide a non-null baseTestUrl!");
        this.m_sSeleniumHubURL = Preconditions.checkNotNull(seleniumHubURL, "You must provide a non-null seleniumHubURL");
        this.m_eTimeoutsConfig = CustomTimeouts.defaultTimeoutsConfig();
    }

    public WebBrowserType getBrowserType() {
        return m_BrowserType;
    }

    public String getBaseTestUrl() {
        return m_sBaseTestUrl;
    }

    public String getSeleniumHubURL() {
        return m_sSeleniumHubURL;
    }

    public CustomTimeouts getTimeoutsConfig() {
        return m_eTimeoutsConfig;
    }

    public Optional<String> getBrowserVersion() {
        return m_osBrowserVersion;
    }

    public static BrowserBuilder getBuilder(WebBrowserType browserType, String baseTestUrl, String seleniumHubURL) {
        return new BrowserBuilder(browserType, baseTestUrl, seleniumHubURL);
    }

    public static BrowserBuilder getChromeBuilder(String baseTestUrl, String seleniumHubURL) {
        return new BrowserBuilder(WebBrowserType.CHROME, baseTestUrl, seleniumHubURL);
    }

    public static BrowserBuilder getFirefoxBuilder(String baseTestUrl, String seleniumHubURL) {
        return new BrowserBuilder(WebBrowserType.FIREFOX, baseTestUrl, seleniumHubURL);
    }

    public static BrowserBuilder getInternetExplorerBuilder(String baseTestUrl, String seleniumHubURL) {
        return new BrowserBuilder(WebBrowserType.IE, baseTestUrl, seleniumHubURL);
    }

    public Browser build(TestComponentData data) {
        TestReporter.TRACE("Building Remote Browser with the following config: " + toString());
        Browser browser;
        switch (m_BrowserType) {
            case FIREFOX:
                browser = new FirefoxBrowser(data, m_sBaseTestUrl, m_eTimeoutsConfig, Optional.<String>absent(), Optional.<String>absent(), m_osBrowserVersion, m_sSeleniumHubURL);
                break;
            case CHROME:
                browser = new ChromeBrowser(data, m_sBaseTestUrl, m_eTimeoutsConfig, Optional.<String>absent(), Optional.<String>absent(), m_osBrowserVersion, m_sSeleniumHubURL);
                break;
            /*case IE:
                browser = new InternetExplorerBrowser(baseTestUrl, timeoutsConfig, Optional.<String>absent(), Optional.<String>absent(), browserVersion);
                break;*/
            default:
                throw new IllegalArgumentException("Only Firefox, Chrome, and IE are currently supported!");
        }
        return browser;
    }

    public BrowserBuilder setTimeoutsConfig(CustomTimeouts timeoutsConfig) {
        this.m_eTimeoutsConfig = timeoutsConfig == null ? CustomTimeouts.defaultTimeoutsConfig() : timeoutsConfig;
        return this;
    }

    public BrowserBuilder setBrowserVersion(String browserVersion) {
        this.m_osBrowserVersion = Optional.fromNullable(browserVersion);
        return this;
    }

    @Override
    public String toString() {
        return "BrowserBuilder{" +
                "m_BrowserType=" + m_BrowserType +
                ", m_sBaseTestUrl='" + m_sBaseTestUrl + '\'' +
                ", m_sSeleniumHubURL='" + m_sSeleniumHubURL + '\'' +
                ", m_eTimeoutsConfig=" + m_eTimeoutsConfig +
                ", m_osBrowserVersion=" + m_osBrowserVersion +
                '}';
    }

}

