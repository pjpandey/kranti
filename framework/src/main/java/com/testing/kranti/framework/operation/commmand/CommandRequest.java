package com.testing.kranti.framework.operation.commmand;

import com.testing.kranti.framework.operation.OpRequest;

public class CommandRequest implements OpRequest {
    private String host;
    private String[] arrCommand = null;
    private String sUser = null;

    public CommandRequest(String[] arrCommand) {
        this(arrCommand, null);
    }

    public CommandRequest(String[] arrCommand, String host) {
        super();
        this.arrCommand = arrCommand;
        this.host = host;
    }

    public String[] getCommand() {
        return arrCommand;
    }

    public String getUser() {
        return sUser;
    }

    public String getHost() {
        return host;
    }

    public void setUser(String user) {
        sUser = user;
    }
}
