package com.testing.kranti.framework;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.testing.kranti.framework.configuration.kranti.KrantiConfiguration;
import com.testing.kranti.framework.env.WindowsEnvironment;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.reporter.TestDataReporter;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.utils.File.KrantiFileUtils;
import com.testing.kranti.framework.utils.File.WindowsFileUtils;
import com.testing.kranti.framework.utils.LocalHost;
import com.testing.kranti.framework.utils.ThreadUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class TestEnvironment {
    public static final String BASE_DIR_ROOT = "/tmp" + File.separator + "test_kranti" + File.separator;
    private static TemporaryFolder m_tmpFolder = null;
    private static String baseDir = null;
    private static TestCase testCase = null;
    private static Collection<String> m_colHosts = new ArrayList<String>();
    private static List<String> m_windowsHosts = new ArrayList<String>();
    private static boolean bComponentsUpgradeRequired = false;
    private static boolean bComponentsDowngradeRequired = false;
    private static boolean bUseExistingConfiguration = false;
    private static Method mTestMethod = null;
    private static boolean bWindowsTest = false;
    private static boolean bShouldKeepLog = false;
    private static boolean bShadowPoolTest = false;
    private static long initTime;
    private static long tReadyTime;
    private static long tDoneTime;
    private static long tEndTime;
    private static boolean bUseOldCommands = false;
    private static boolean bEnableJobSignalHandler = false;
    private static boolean bIsPerformance = false;
    private static boolean isWebserverTest = false;
    private static boolean bIsIntegration = false;
    private static boolean isHaltOnError = false;
    private static KrantiConfiguration eKrantiConf;
    private static String currentApplication;

    public static boolean isWebserverTest() {
        return isWebserverTest;
    }

    public static void setWebserverTest(boolean m_bIsWebserverTest) {
        TestEnvironment.isWebserverTest = m_bIsWebserverTest;
    }

    private static String windowsSandboxDir;

    public static void init(TestCase tTestCase, String sBaseDir, TemporaryFolder tmpFolder) {
        init(tTestCase, sBaseDir, tmpFolder, null);
    }

    public static void init(TestCase tTestCase, String sBaseDir, TemporaryFolder tmpFolder, String sWindowsSandboxDir) {
        baseDir = sBaseDir;
        m_tmpFolder = tmpFolder;
        testCase = tTestCase;
        windowsSandboxDir = sWindowsSandboxDir;
        initTime = System.currentTimeMillis();
        TestReporter.TRACE("Test started at : " + new Date(initTime));
    }

    public static TemporaryFolder getTemporaryFolder() {
        return m_tmpFolder;
    }

    public static void sleep(long lSeconds) {
        if (lSeconds <= 0) {
            return;
        }
        TestReporter.TRACE("Sleeping for " + lSeconds + " seconds");
        ThreadUtils.sleep(lSeconds * 1000L);
    }

    public static void sleepInMilli(Integer iMilliSec) {
        if (iMilliSec <= 0) {
            return;
        }
        TestReporter.TRACE("Sleeping for " + iMilliSec + " seconds");
        ThreadUtils.sleep(iMilliSec);
    }

    public static String getTestFolderPath(Object oTest) {
        return getTestFolderPath(getTestFolderRelativePath(oTest));
    }

    public static String getTestFolderPath(String sRelativePath) {
        String testProjectPath = getKrantiConf().getProjectHead() + "/" + getCurrentApplication() + ".testing";
        if (isGradleProject(testProjectPath)) {
            return testProjectPath + "src/main/java/" + sRelativePath;
        }
        return testProjectPath + sRelativePath;
    }

    public static boolean isGradleProject(String prefix) {
        return new File(prefix + "src/main/java").exists();
    }

    public static String getTestFolderRelativePath(Object oTest) {
        return oTest.getClass().getPackage().getName().replace(".", "/");
    }

    public static String getSandBoxDir() {
        return baseDir;
    }

    public static String getWindowsSandboxDir() {
        return windowsSandboxDir;
    }

    public static void prepare(Collection<String> colHosts) {
        Thread[] arrThreads = new Thread[colHosts.size()];
        int i = 0;
        for (final String sHost : colHosts) {
            arrThreads[i++] = new Thread() {
                @Override
                public void run() {
                    if (!LocalHost.isLocalHost(sHost)) {
                        if (TestEnvironment.isWindowsHost(sHost) && TestEnvironment.isWindowsTest()) {
                            String sCurrentLinkPath = WindowsEnvironment.SANDBOX_DIR_ROOT + "\\current";
                            WindowsFileUtils.removeFile(windowsSandboxDir, sHost, false);
                            KrantiFileUtils.createDirectory(windowsSandboxDir, sHost);
                            WindowsFileUtils.removeFile(sCurrentLinkPath, sHost, true);
                            WindowsFileUtils.createLink(sCurrentLinkPath, windowsSandboxDir, sHost);
                        } else {
                            if (!TestEnvironment.isWindowsHost(sHost)) {
                                TestCommandExecution.runCommand("rm -fr " + getSandBoxDir(), sHost);
                                TestCommandExecution.runCommand("rm -f " + TestEnvironment.BASE_DIR_ROOT + "/current", sHost);
                                TestCommandExecution.runCommand("mkdir -p " + getSandBoxDir(), sHost);
                            }
                        }
                    }
                }
            }
            ;
        }

        for (Thread t : arrThreads) {
            t.start();
        }

        for (Thread t : arrThreads) {
            try {
                t.join(TestCommandExecution.DEFAULT_COMMAND_TIMEOUT);
            } catch (InterruptedException e) {
            }
        }
        m_colHosts = colHosts.stream().filter((Predicate<String>) input -> !isWindowsHost(input)).collect(Collectors.toList());
    }

    public static Collection<String> getHosts() {
        return m_colHosts;
    }


    public static TestCase getTestCase() {
        return testCase;
    }


    public static void setComponentsUpgradeRequired(boolean bComponentsUpgradeRequired) {
        TestEnvironment.bComponentsUpgradeRequired = bComponentsUpgradeRequired;
    }

    public static boolean isComponentsUpgradeRequired() {
        return bComponentsUpgradeRequired;
    }

    public static void setTestMethod(Method mTestMethod) {
        TestEnvironment.mTestMethod = mTestMethod;
    }

    public static Method getTestMethod() {
        return mTestMethod;
    }

    public static void setToWindows(List<String> windowsHosts) {
        m_windowsHosts = Lists.transform(windowsHosts, new Function<String, String>() {

            @Override
            public String apply(String input) {
                return input.toLowerCase();
            }
        });
    }

    public static boolean isWindowsHost(String sHost) {
        if (StringUtils.isEmpty(sHost)) {
            return false;
        }
        if (m_windowsHosts.contains(sHost.toLowerCase())) {
            return true;
        }
        for (String sWindowsHost : m_windowsHosts) {
            if (sWindowsHost.startsWith(sHost + ".")) {
                return true;
            }
        }
        return false;
    }

    public static String getFullHost(String sHost) {
        for (String sWindowsHost : m_windowsHosts) {
            if (sWindowsHost.startsWith(sHost + ".")) {
                return sWindowsHost;
            }
        }
        return sHost;
    }

    public static boolean isWindowsTest() {
        return bWindowsTest;
    }

    public static void setWindowsTest(boolean bWindowsTest) {
        TestEnvironment.bWindowsTest = bWindowsTest;
    }

    public static void setUseExistingConfiguration(boolean bUseExistingConfiguration) {
        TestReporter.TRACE(" Setting useExisting configuration to " + bUseExistingConfiguration);
        TestEnvironment.bUseExistingConfiguration = bUseExistingConfiguration;
    }

    public static boolean isUseExistingConfiguration() {
        return bUseExistingConfiguration;
    }

    public static boolean isShadowPoolTest() {
        return bShadowPoolTest;
    }

    public static void setShadowPoolTest(boolean bShadowPoolTest) {
        TestEnvironment.bShadowPoolTest = bShadowPoolTest;
    }

    public static boolean isComponentsDowngradeRequired() {
        return bComponentsDowngradeRequired;
    }

    public static void setComponentsDowngradeRequired(boolean bComponentsDowngradeRequired) {
        TestEnvironment.bComponentsDowngradeRequired = bComponentsDowngradeRequired;
    }

    public static long getInitTime() {
        return initTime;
    }


    public static void setReadyTime() {
        tReadyTime = System.currentTimeMillis();
        TestReporter.TRACE("Test Sequence Ready to execute at : " + new Date(tReadyTime));
    }

    public static long getReadyTime() {
        return tReadyTime;
    }

    public static void setDoneTime() {
        tDoneTime = System.currentTimeMillis();
        TestReporter.TRACE("Test Sequence Finished at : " + new Date(tDoneTime));
        TestDataReporter.addData("duration", (double) getTestTime());
    }

    public static long getTestTime() {
        return (tDoneTime - tReadyTime) / 1000;
    }

    public static long getDoneTime() {
        return tDoneTime;
    }

    public static void setEndTime() {
        tEndTime = System.currentTimeMillis();
        TestReporter.TRACE("Test Completed at : " + new Date(tEndTime));
    }

    public static long getEndTime() {
        return tEndTime;
    }

    public static void useOldCommand(boolean oldCommands) {
        bUseOldCommands = oldCommands;
    }

    public static boolean isTestRequiresOldCommands() {
        return bUseOldCommands;
    }

    public static boolean isJobSignalHandlerEnabled() {
        return bEnableJobSignalHandler;
    }

    public static void setJobSignalHandlerEnabler(boolean value) {
        bEnableJobSignalHandler = value;
    }

    public static void setPerformance(boolean isPerformance) {
        bIsPerformance = isPerformance;
    }

    public static void setIntegration(boolean isIntegration) {
        bIsIntegration = isIntegration;
    }

    public static boolean isPerformance() {
        return bIsPerformance;
    }

    public static void setShoudKeepLog(boolean bShouldKeepLog) {
        TestEnvironment.bShouldKeepLog = bShouldKeepLog;
    }

    public static boolean shoudKeepLog() {
        return bShouldKeepLog;
    }

    public static boolean isHaltOnError() {
        return isHaltOnError;
    }

    public static void setHaltOnError(boolean isHaltOnError) {
        TestEnvironment.isHaltOnError = isHaltOnError;
    }

    public static void setKrantiConf(KrantiConfiguration krantiConf) {
        TestEnvironment.eKrantiConf = krantiConf;
    }

    public static KrantiConfiguration getKrantiConf() {
        return eKrantiConf;
    }

    public static String getCurrentApplication() {
        return currentApplication;
    }

    public static void setCurrentApplication(String currentApplication) {
        TestEnvironment.currentApplication = currentApplication;
    }
}
