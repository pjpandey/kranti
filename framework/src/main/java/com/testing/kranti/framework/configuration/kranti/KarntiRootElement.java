package com.testing.kranti.framework.configuration.kranti;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KarntiRootElement {
    @JsonProperty("kranti")
    public KrantiConfiguration kranti;

    @JsonProperty("kranti")
    public KrantiConfiguration getKranti() {
        return kranti;
    }

    @JsonProperty("kranti")
    public void setKranti(KrantiConfiguration kranti) {
        this.kranti = kranti;
    }
}
