package com.testing.kranti.framework.gui.abstrct;

public enum WebBrowserType {
    IE, CHROME, FIREFOX, MOBILE;

    public static WebBrowserType forName(String name) {
        for (WebBrowserType type : WebBrowserType.values()) {
            if (type.toString().equalsIgnoreCase(name)) {
                return type;
            }
        }
        throw new IllegalArgumentException("WebBrowserType must be 'IE', 'CHROME', or 'FIREFOX'");
    }
}
