package com.testing.kranti.framework.rules.exceptions;

import com.google.common.collect.Sets;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

import java.util.Set;

public class krantiIgnoredExceptionRule extends AbstractMethodRule {
    private Set<Class> ignoredException = Sets.newHashSet();

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        extractExceptions(oTest.getClass().getAnnotation(KrantiIgnoredExceptions.class), mTest.getAnnotation(KrantiIgnoredExceptions.class));
    }

    private void extractExceptions(KrantiIgnoredExceptions... arrExceptions) {
        for (KrantiIgnoredExceptions exceptions : arrExceptions) {
            if (null != exceptions) {
                for (Class cls : exceptions.value()) {
                    ignoredException.add(cls);
                }
            }
        }
    }

    public Set<Class> getIgnoredException() {
        return ignoredException;
    }
}
