package com.testing.kranti.framework.utils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class LocalHost {
    public static final String VIRTUAL_HOST_NAME = "__VIRTUAL_HOST_NAME";
    private static Logger log = Logger.getLogger(LocalHost.class);

    static boolean IS_LOCAL_HOST_SHOULD_CHECK_DSN_ALIAS = true;
    public final static String FULL_HOST_NAME = "FullHostName";
    public final static String TYPE_VIRTUAL = "virtual";
    public final static String PROPERTY_TYPE = "type";
    public final static String PROPERTY_PHISICAL = "physicalhost";
    private static String EXTERNAL_DOMAINS = "me-corp";
    private static String s_sVirtualLong = null;
    private static String s_sPhysicalLong = null;
    private static String s_sPhysicalShort = null;
    private static String s_sVirtualShort = null;
    private static String s_sAddress = null;
    private static String s_sCanonical = null;
    private static InetAddress s_inetAddress = null;
    private static String s_sDomain = null;
    private static String s_sNISDomain = null;
    static InetStrategy s_startegy = new InetStrategy();
    static Cache<String, Boolean> s_DNSAliasCaching = CacheBuilder.newBuilder().maximumSize(100).expireAfterWrite(1, TimeUnit.HOURS).build();

    static class InetStrategy {

        public InetAddress[] getAllByName(String target) throws UnknownHostException {
            return InetAddress.getAllByName(target);
        }

    }


    public static String getVirtualShort() {
        String virtualHost = s_sVirtualShort;
        if (virtualHost != null) {
            return virtualHost;
        } else {
            refresh();
            if (null == s_sVirtualShort) {
                s_sVirtualShort = s_sPhysicalShort;
            }
            return s_sVirtualShort;
        }
    }

    public static String getVirtualLong() {
        String virtualLong = s_sVirtualLong;
        if (virtualLong != null) {
            return virtualLong;
        }
        if (s_sPhysicalLong != null) {
            return s_sPhysicalLong;
        } else {
            refresh();
            if (null == s_sVirtualLong) {
                s_sVirtualLong = s_sPhysicalLong;
            }
            return s_sVirtualLong;
        }
    }

    public static String getPhysicalShort() {
        if (s_sPhysicalShort != null) {
            return s_sPhysicalShort;
        } else {
            refresh();
            return s_sPhysicalShort;
        }
    }

    public static String getPhysicalLong() {
        if (s_sPhysicalLong != null) {
            return s_sPhysicalLong;
        } else {
            refresh();
            return s_sPhysicalLong;
        }
    }

    public static void refresh() {
        try {
            s_inetAddress = InetAddress.getLocalHost();
            s_sAddress = s_inetAddress.getHostAddress();
            try {
                s_sCanonical = s_inetAddress.getCanonicalHostName();
            } catch (Throwable t) {
                // getCanonicalHostName is a new method in java 1.4
                s_sCanonical = s_inetAddress.getHostAddress();
            }

            // s_sPrimaryLong - if define in property take it from there.
            String fullHostName = System.getProperty(FULL_HOST_NAME);
            if (StringUtils.isEmpty(fullHostName) || !fullHostName.contains(".")) {
                fullHostName = s_sCanonical.toLowerCase();
            }

            if (!isVirtual()) {
                setMachineHost(fullHostName);
            }
            // on virtual OS
            else {
                setVirtualLong(fullHostName);
                String sPhysicalLong = System.getProperty(PROPERTY_PHISICAL);
                s_sDomain = getDomainFromLong(fullHostName);
                s_sPhysicalLong = appendFullDomainName(sPhysicalLong);
            }
            s_sPhysicalShort = getShortFromLong(s_sPhysicalLong);
            s_sNISDomain = System.getProperty("NISDomain");
        } catch (UnknownHostException e) {
            System.out.println("refresh() Failed to get host name" + e);
            s_inetAddress = null;
            s_sPhysicalLong = null;
            s_sPhysicalShort = null;
            s_sAddress = null;
            s_sCanonical = null;
        }

    }

    private static void setMachineHost(String fullHostName) {
        String physicalLong = System.getenv(VIRTUAL_HOST_NAME);
        if (!StringUtils.isEmpty(physicalLong)) {
            s_sPhysicalLong = physicalLong;
        } else {
            s_sPhysicalLong = fullHostName;
        }
        s_sDomain = getDomainFromLong(fullHostName);
    }

    public static boolean isVirtual() {
        String sType = System.getProperty(PROPERTY_TYPE);
        // type is null - when nothing is set (not vwsm and not hwsm)
        if (sType != null && sType.equalsIgnoreCase(TYPE_VIRTUAL)) {
            return true;
        }
        return false;
    }


    public static void setVirtualLong(String virtualHostname) {
        if (null != virtualHostname) {
            if (-1 != virtualHostname.indexOf(".")) {
                s_sVirtualLong = virtualHostname;
                s_sVirtualShort = getShortFromLong(s_sVirtualLong);
            } else {
                s_sVirtualShort = virtualHostname;
                if (null == s_sDomain) {
                    refresh();
                    s_sVirtualLong = getLongFromShort(s_sVirtualShort, null);
                }
            }
        }
    }

    public static String getShortFromLong(String longName) {
        if (longName == null) {
            return longName;
        }
        int index = longName.indexOf(".");
        if (index != -1) {
            return longName.substring(0, index);
        } else {
            return longName;
        }
    }

    public static String getDomainFromLong(String longName) {
        if (longName == null) {
            return longName;
        }
        int index = longName.indexOf(".");
        if (index != -1) {
            return longName.substring(index + 1);
        } else {
            // return longName;
            return null;
        }
    }

    public static String getDomain() {
        if (s_sDomain != null) {
            return s_sDomain;
        }
        refresh();
        return s_sDomain;
    }


    public static String appendFullDomainName(String hostName) {
        return appendFullDomainName(hostName, null);
    }

    public static String appendFullDomainName(String hostName, String sDomain) {
        if (null == hostName) {
            return "";
        }
        int index = hostName.indexOf('.');
        if (index == -1) {
            return getLongFromShort(hostName, sDomain);
        }
        if (!needToAppendDomainName(hostName)) {
            return hostName;
        }

        if (hostName.indexOf(".cltp") > 0) {
            return hostName + ".com";
        }
        return hostName + ".cltp.com";
    }

    private static boolean needToAppendDomainName(String hostName) {
        String realDomain = getDomain();
        if (!StringUtils.isEmpty(realDomain) && !realDomain.contains("cleartrip")) {
            return false;
        }

        if (hostName.indexOf(".cltp.com") > 0) {
            return false;
        }

        for (String sExternalDomain : EXTERNAL_DOMAINS.split(",")) {
            if (hostName.indexOf("." + sExternalDomain) > 0) {
                return false;
            }
        }
        return true;
    }

    private static String getLongFromShort(String shortName, String domain) {
        String sDomain = domain;
        if (shortName == null) {
            return shortName;
        }

        if ((null == sDomain) || (0 == sDomain.length())) {
            sDomain = getDomain();
        }

        if (sDomain == null) {
            return shortName;
        }

        return shortName + "." + sDomain;
    }

    public static boolean isLocalHost(String sHost) {
        String host = sHost;
        if (null == host) {
            return false;
        }

        int iIndex = host.indexOf(':');
        if (iIndex >= 0) {
            host = getToken(host, ":", 1);
        }

        if (host.compareToIgnoreCase(getPhysicalLong()) == 0 || host.compareToIgnoreCase(getPhysicalShort()) == 0
                || host.compareToIgnoreCase(getVirtualLong()) == 0 || host.compareToIgnoreCase(getVirtualShort()) == 0) {
            return true;
        }

        if (host.equalsIgnoreCase("localhost") || host.equalsIgnoreCase("me") || host.equalsIgnoreCase("127.0.0.1")) {
            return true;
        }

        if (!host.endsWith(".")) {
            host += ".";
        }
        if (getPhysicalLong().startsWith(host) || getVirtualLong().startsWith(host)) {
            return true;
        }
        return IS_LOCAL_HOST_SHOULD_CHECK_DSN_ALIAS && isDNSAlias(sHost);
    }

    private static boolean IsLocalIPTheSame(InetAddress address) {
        if (null == address || null == s_inetAddress) {
            return false;
        }
        return Arrays.equals(s_inetAddress.getAddress(), address.getAddress());
    }


    public static boolean isDNSAlias(String target) {
        Boolean $ = getTargetAliasInCache(target);
        if ($ != null) {
            return $;
        }
        InetAddress[] allByName = null;
        try {
            allByName = s_startegy.getAllByName(target);
        } catch (UnknownHostException ex) {
            log.info("isDNSAlias() - failed to resolve hostname: " + target + " to inet address");
        }
        $ = false;
        if (allByName != null) {
            for (InetAddress address : allByName) {
                if (IsLocalIPTheSame(address)) {
                    $ = true;
                    break;
                }
            }
        }
        log.debug("Adding hostname: " + target + " to cache with result: " + $);
        setTargetAliasInCache(target, $);
        return $;
    }

    private static void setTargetAliasInCache(String target, Boolean res) {
        s_DNSAliasCaching.put(target, res);
    }

    private static Boolean getTargetAliasInCache(String target) {
        return s_DNSAliasCaching.getIfPresent(target);
    }


    public static String getToken(String sLine, String sDelimiter, int iNum) {
        try {
            String[] arrTokens = sLine.split(sDelimiter);
            if ((iNum > arrTokens.length) || (iNum < 1)) {
                return null;
            }
            return arrTokens[iNum - 1];
        } catch (Exception e) {
            int iDelimiter = sDelimiter.length();

            String sValue = sLine;
            int iPrev = 0;

            for (int i = 0; i < iNum; i++) {
                if (iPrev >= sLine.length()) {
                    return null;
                }
                int iIndex = sLine.indexOf(sDelimiter, iPrev);
                if (iIndex >= 0) {
                    sValue = safeSubString(sLine, iPrev, iIndex);
                    iPrev = iIndex + iDelimiter;
                } else {
                    sValue = sLine.substring(iPrev);
                    iPrev = sLine.length();
                }
            }
            return sValue;
        }
    }

    private static String safeSubString(String s, int startIndex, int endIndex) {
        if (endIndex < 0 || endIndex > s.length()) {
            return s.substring(startIndex);
        }
        return s.substring(startIndex, endIndex);
    }
}
