package com.testing.kranti.framework;


public abstract class TestFlow implements Runnable {
    private Throwable m_tFatal = null;

    @Override
    public void run() {
        try {
            runSequence();
        } catch (Throwable t) {
            m_tFatal = t;
        }
    }

    public Throwable getFatal() {
        return m_tFatal;
    }

    public abstract void runSequence();

    protected void sleep(long lSeconds) {
        try {
            Thread.sleep(lSeconds * 1000L);
        } catch (InterruptedException e) {
        }
    }
}
