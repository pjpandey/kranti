package com.testing.kranti.framework.operation.commmand.impl;


public abstract class CommandProcess extends Process {

    public abstract void timeout();

    public abstract boolean isTimedOut();

    public abstract int getPID() throws Exception;

}
