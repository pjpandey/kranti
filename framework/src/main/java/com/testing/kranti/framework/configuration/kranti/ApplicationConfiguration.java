package com.testing.kranti.framework.configuration.kranti;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;


public class ApplicationConfiguration {
    @JsonProperty("installationpath")
    private String defaultInstallationPath;
    private String commandBinaries;
    @JsonProperty("upgradedinstallationpath")
    private String defaultUpgradedInstallationPath;
    @JsonProperty("downgradedinstallationpath")
    private String defaultDowngradedInstallationPath;
    private List<ComponentInstallation> lstComponents = new ArrayList<ComponentInstallation>();
    private String tarPath;
    @JsonProperty("name")
    private String name;

    @JsonProperty("installationpath")
    public void setDefaultInstallationPath(String installationPath) {
        defaultInstallationPath = installationPath;
    }

    @JsonProperty("installationpath")
    public String getDefaultInstallationPath() {
        return defaultInstallationPath;
    }

    @JsonProperty("upgradedinstallationpath")
    public void setDefaultUpgradedInstallationPath(String defaultUpgradedInstallationPath) {
        this.defaultUpgradedInstallationPath = defaultUpgradedInstallationPath;
    }

    @JsonProperty("upgradedinstallationpath")
    public String getDefaultUpgradedInstallationPath() {
        return defaultUpgradedInstallationPath;
    }

    @JsonProperty("downgradedinstallationpath")
    public void setDefaultDowngradedInstallationPath(String defaultDowngradedInstallationPath) {
        this.defaultDowngradedInstallationPath = defaultDowngradedInstallationPath;
    }

    @JsonProperty("downgradedinstallationpath")
    public String getDefaultDowngradedInstallationPath() {
        return defaultDowngradedInstallationPath;
    }

    @JsonProperty("tarpath")
    public void setTarPath(String tarPath) {
        this.tarPath = tarPath;
    }

    @JsonProperty("tarpath")
    public String getTarPath() {
        return tarPath;
    }

    @JsonProperty("component")
    public List<ComponentInstallation> getComponents() {
        return lstComponents;
    }

    @JsonProperty("component")
    public void setLstComponents(List<ComponentInstallation> lstComponents) {
        this.lstComponents = lstComponents;
    }

    @JsonProperty("commandBinaries")
    public String getCommandBinaries() {
        return commandBinaries;
    }

    @JsonProperty("commandBinaries")
    public void setCommandBinaries(String commandBinaries) {
        this.commandBinaries = commandBinaries;
    }


    @JsonIgnore
    public String getDefaultConfigurationPath() {
        return getDefaultInstallationPath() + "/conf";
    }

    @JsonIgnore
    public String getWebConfigurationPath() {
        return getDefaultConfigurationPath();
    }

    @JsonIgnore
    public String getDefaultUpgradedConfigurationPath() {
        if (null == getDefaultUpgradedInstallationPath()) {
            return null;
        }
        return getDefaultUpgradedInstallationPath() + "/conf";
    }

    @JsonIgnore
    public String getDefaultDowngradedConfigurationPath() {
        if (null == getDefaultDowngradedInstallationPath()) {
            return null;
        }
        return getDefaultDowngradedInstallationPath() + "/conf";
    }

    @JsonIgnore
    public String getComponentInstallationPath(String type, int index) {
        String path = getDefaultInstallationPath();
        for (ComponentInstallation comp : getComponents()) {
            if (comp.getName().equalsIgnoreCase(type)) {
                List<String> paths = comp.getInstallationPath();
                if ((null != paths) && (paths.size() > index))
                    path = paths.get(index);
                break;
            }
        }
        return path;
    }

    @JsonIgnore
    public String getComponentUpgradedInstallationPath(String type, int index) {
        String path = getDefaultUpgradedInstallationPath();
        for (ComponentInstallation comp : getComponents()) {
            if (comp.getName().equalsIgnoreCase(type)) {
                List<String> paths = comp.getUpgradedInstallationPath();
                if ((null != paths) && (paths.size() > index))
                    path = paths.get(index);
                break;
            }
        }
        return path;
    }

    @JsonIgnore
    public String getComponentDowngradedInstallationPath(String type, int index) {
        String path = getDefaultDowngradedInstallationPath();
        for (ComponentInstallation comp : getComponents()) {
            if (comp.getName().equalsIgnoreCase(type)) {
                List<String> paths = comp.getDowngradedInstallationPath();
                if ((null != paths) && (paths.size() > index))
                    path = paths.get(index);
                break;
            }
        }
        return path;
    }

    @JsonIgnore
    public String getUpgradedConfigurationPath(String type, int index) {
        return getComponentUpgradedInstallationPath(type, index) + "/conf";
    }

    @JsonIgnore
    public String getDowngradedConfigurationPath(String type, int index) {
        return getComponentDowngradedInstallationPath(type, index) + "/conf";
    }

    @JsonIgnore
    public String getConfigurationPath(String type, int index) {
        return getComponentInstallationPath(type, index) + "/conf";
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }
}
