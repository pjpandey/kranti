package com.testing.kranti.framework.gui.actions;

import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.gui.browserItem.ChromeBrowser;
import com.testing.kranti.framework.gui.timeout.TimeoutType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ChromeSeleniumActions extends SeleniumActions<ChromeBrowser>
{
	public ChromeSeleniumActions(ChromeBrowser browser)
	{
		super(browser);
	}

	@Override
	public void upload(By locator, TimeoutType timeout, String sPath)
	{
		WebElement el = waitUntilClickable(locator, timeout);
		el.click();
		TestEnvironment.sleep(5);
		TestCommandExecution.runCommand("C:\\\\selenium-client\\\\upload_chrome.exe " + sPath, getBrowser().getHost());
	}
}
