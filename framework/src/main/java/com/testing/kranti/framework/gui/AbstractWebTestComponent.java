package com.testing.kranti.framework.gui;

import com.testing.kranti.framework.AbstractNonStartableTestComponent;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.operation.commmand.AbstractCommandOperation;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.ArrayList;


public abstract class AbstractWebTestComponent extends AbstractNonStartableTestComponent {
    private static int NUMBER_OF_TIMES_TRY_TO_START = 5;
    private Browser m_browser;

    public AbstractWebTestComponent(TestComponentData dData) {
        super(dData);
    }


    @Override
    protected void clean() {

    }

    @Override
    protected AbstractCommandOperation getStartOperation() {
        //TODO??
        final String sInstallationPath = getInstallationDir();
        final String configurationDir = getConfigurationDir();
        TestEnvironment.sleep(5);
        setEnvironment();
        AbstractCommandOperation abstractOperation = new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "webcomponentstart";
            }
        };
        return abstractOperation;
    }

    protected abstract void setEnvironment();


    @Override
    protected AbstractCommandOperation getStopOperation() {
        //TODO??
        final String sInstallationPath = getInstallationDir();
        final String configurationDir = getConfigurationDir();
        TestReporter.TRACE("running command ==>" + sInstallationPath + "/stop  on host" + getData().getHost());
        return new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "webcomponentstop";
            }

        };
    }

    @Override
    protected AbstractCommandOperation getHupOperation() {
        return new AbstractCommandOperation() {

            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "Hupped";
            }

        };
    }

    @Override
    public boolean isRunning() {
        if (getCurrentlyRunningPID(true) > 0) {
            return true;
        }
        return false;
    }

    public void restart() {
        stop();
        start();
    }


    @Override
    public OpResult hup() {
        restart();
        return new CommandResult(0, new ArrayList<String>(), new ArrayList<String>(), 0L);
    }

    @Override
    protected String[] getComponentRepresentationInPS() {
        return new String[]{"tomcat"};
    }

    protected Browser getBrowser() {
        return m_browser;
    }

    public void setBrowser(Browser browser) {
        m_browser = browser;
    }
}