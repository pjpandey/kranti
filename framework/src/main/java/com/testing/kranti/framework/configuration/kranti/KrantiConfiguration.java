package com.testing.kranti.framework.configuration.kranti;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class KrantiConfiguration {
    @JsonProperty("projectHead")
    private String projectHead;
    @JsonProperty("application")
    private List<ApplicationConfiguration> lstApplicationConfigurations = new ArrayList<ApplicationConfiguration>();
    @JsonProperty("hosts")
    private List<String> hosts = new ArrayList<String>();
    @JsonProperty("windowsHosts")
    private List<String> windowsHosts = new ArrayList<String>();
    @JsonProperty("seleniumHubUrl")
    private String seleniumHubUrl;
    @JsonProperty("browserType")
    private String webBrowserType;
    @JsonProperty("dumpPath")
    private String reportDumpPath;
    @JsonProperty("useExistingConfig")
    private Boolean useExistingConfig = null;
    @JsonProperty("useLocalHost")
    private Boolean useLocalHost = false;
    @JsonProperty("haltOnError")
    private Boolean haltOnError = false;
    @JsonProperty("localMachine")
    private String localWorkspaceMachine;

    @JsonProperty("projectHead")
    public String getProjectHead() {
        return projectHead;
    }

    @JsonProperty("projectHead")
    public void setProjectHead(String projectHead) {
        this.projectHead = projectHead;
    }

    @JsonProperty("application")
    public List<ApplicationConfiguration> getApplicationConfigurations() {
        return lstApplicationConfigurations;
    }

    @JsonProperty("application")
    public ApplicationConfiguration getApplicationConfiguration(String appName) {
        return lstApplicationConfigurations.stream().filter(a -> a.getName().equals(appName)).findFirst().get();
    }

    @JsonProperty("application")
    public void setApplicationConfigurations(List<ApplicationConfiguration> lstApplicationConfigurations) {
        this.lstApplicationConfigurations = lstApplicationConfigurations;
    }

    @JsonProperty("hosts")
    public List<String> getHosts() {
        return hosts;
    }

    @JsonProperty("hosts")
    public void setHosts(List<String> hosts) {
        this.hosts = hosts;
    }

    @JsonProperty("windowsHosts")
    public List<String> getWindowsHosts() {
        return windowsHosts;
    }

    @JsonProperty("windowsHosts")
    public void setWindowsHosts(List<String> windowsHosts) {
        this.windowsHosts = windowsHosts;
    }

    @JsonProperty("seleniumHubUrl")
    public String getSeleniumHubUrl() {
        return seleniumHubUrl;
    }

    @JsonProperty("seleniumHubUrl")
    public void setSeleniumHubUrl(String seleniumHubUrl) {
        this.seleniumHubUrl = seleniumHubUrl;
    }

    @JsonProperty("browserType")
    public String getWebBrowserType() {
        return webBrowserType;
    }

    @JsonProperty("browserType")
    public void setWebBrowserType(String webBrowserType) {
        this.webBrowserType = webBrowserType;
    }

    @JsonProperty("dumpPath")
    public String getReportDumpPath() {
        return reportDumpPath;
    }

    @JsonProperty("dumpPath")
    public void setReportDumpPath(String reportDumpPath) {
        this.reportDumpPath = reportDumpPath;
    }

    @JsonProperty("useExistingConfig")
    public Boolean getUseExistingConfig() {
        return useExistingConfig;
    }

    @JsonProperty("useExistingConfig")
    public void setUseExistingConfig(Boolean useExistingConfig) {
        this.useExistingConfig = useExistingConfig;
    }

    @JsonProperty("useLocalHost")
    public Boolean getUseLocalHost() {
        return useLocalHost;
    }

    @JsonProperty("useLocalHost")
    public void setUseLocalHost(Boolean useLocalHost) {
        this.useLocalHost = useLocalHost;
    }

    @JsonProperty("haltOnError")
    public Boolean getHaltOnError() {
        return haltOnError;
    }

    @JsonProperty("haltOnError")
    public void setHaltOnError(Boolean haltOnError) {
        this.haltOnError = haltOnError;
    }

    @JsonProperty("localMachine")
    public String getLocalWorkspaceMachine() {
        return localWorkspaceMachine;
    }

    @JsonProperty("localMachine")
    public void setLocalWorkspaceMachine(String localWorkspaceMachine) {
        this.localWorkspaceMachine = localWorkspaceMachine;
    }

    @JsonIgnore
    public Collection<String> getConfiguredApplications() {
        Collection<String> colApplications = new ArrayList<String>();
        for (ApplicationConfiguration cConf : lstApplicationConfigurations) {
            colApplications.add(cConf.getName());
        }
        return colApplications;
    }
}

