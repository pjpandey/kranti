package com.testing.kranti.framework.rules.dontstop;

import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiManageComponentOnTestFinishRule extends AbstractMethodRule {
    private boolean m_bDontStopOnFailure = false;
    private boolean m_bForceStop = false;
    private boolean m_bDontCleanOnFailure = false;
    private boolean m_bDontCleanOnPass = false;
    private boolean m_bDontCleanLogOnSuccess = false;

    private void setDontStopOnFailure(boolean dontStop) {
        m_bDontStopOnFailure = dontStop;
    }

    private void setDontCleanOnFailure(boolean dontClean) {
        m_bDontCleanOnFailure = dontClean;
    }

    private void setDontCleanOnPass(boolean dontClean) {
        TestReporter.TRACE("Setting Don't clean on Pass to " + dontClean);
        m_bDontCleanOnPass = dontClean;
    }

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        setDontStopOnFailure(mTest.getMethod().isAnnotationPresent(KrantiDontStopComponentsOnFail.class) || oTest.getClass().isAnnotationPresent(KrantiDontStopComponentsOnFail.class));
        setDontCleanOnFailure(mTest.getMethod().isAnnotationPresent(KrantiDontCleanComponentsOnFail.class) || mTest.getClass().isAnnotationPresent(KrantiDontCleanComponentsOnFail.class) || oTest.getClass().isAnnotationPresent(KrantiDontStopComponentsOnFail.class));
        setForceStop(oTest.getClass().isAnnotationPresent(KrantiForceStopComponents.class) || mTest.getMethod().isAnnotationPresent(KrantiForceStopComponents.class) || mTest.getMethod().isAnnotationPresent(KrantiUpgradeTest.class) || mTest.getMethod().getDeclaringClass().isAnnotationPresent(KrantiUpgradeTest.class) || mTest.getMethod().getDeclaringClass().getPackage().isAnnotationPresent(KrantiUpgradeTest.class));
        setDontCleanOnPass(mTest.getMethod().isAnnotationPresent(KrantiDontCleanComponetOnPass.class) || mTest.getClass().isAnnotationPresent(KrantiDontCleanComponetOnPass.class) || oTest.getClass().isAnnotationPresent(KrantiDontCleanComponetOnPass.class));
    }

    private void setForceStop(boolean forceStop) {
        m_bForceStop = forceStop;
    }

    public boolean shouldStop() {
        if (m_bForceStop) {
            return true;
        }
        if (RegressionEnvironment.isSmartCoverage()) {
            return true;
        }
        if (TestReporter.testFailed()) {
            if ((m_bDontStopOnFailure || m_bDontCleanOnFailure) && !RegressionEnvironment.isRegression()) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean shouldClean() {
        if (TestReporter.testFailed()) {
            if (m_bDontCleanOnFailure && !RegressionEnvironment.isRegression()) {
                return false;
            }
            return true;
        } else {
            if (m_bDontCleanOnPass) {
                return false;
            }
            return true;
        }
    }


}
