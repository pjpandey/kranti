package com.testing.kranti.framework;

import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.operation.Operation;

import java.util.Collection;
import java.util.List;


public interface TestComponent {
    void start();

    void stop();

    void suspendComponent();

    void resumeComponent();

    OpResult hup();

    void restart(TestFlow... fFlowBeforeStart);

    void upgrade(TestFlow... arrFlowsBeforeStart);

    void downgrade(TestFlow... arrFlowsBeforeStart);

    void clean(boolean bForce);

    boolean isRunning();

    boolean isSuspended();

    void setShouldCleanPersistencyLeftoversOnTeardown(boolean shouldCleanPersistencyLeftoversOnTeardown);

    int getProcessID();

    String getSandBoxDir();

    String getHost();

    String getInstallationDir();

    String getConfigurationDir();

    String getPersistencyDir();

    String getLogDir();

    String getComponentType();

    List<ISnapshot> getSnapShots();

    OpResult performOperation(Operation operation);

    OpResult performFailedOperation(Operation oOperation);

    void initRunningComponent();

    void prepareForExecution();

    void prepareForStart();

    String getLogConfigurationFile();

    int getCleanOrder();

    void kill();

    boolean shouldCleanPersistencyLeftoversOnTeardown();

    void registerMemoryLeakCheckers();

    void captureSnapshotOnError(String sTargetDir);

    Collection<String> getIngoredReferencesForLeakCheck();

    void setSandBoxDir(String sandBoxDir);

    void setConfigurationDir(String sDir);

    void setShouldVerifyNextStop(boolean shouldVerify);

    String getUpgradedInstallationDir();

    OpResult startWithoutExceptionReturnsResult();

    void prepareSandBoxDir();

    String getDowngradedInstallationDir();

    String getDowngradedConfigurationDir();

    boolean disableLeakCheck();
}
