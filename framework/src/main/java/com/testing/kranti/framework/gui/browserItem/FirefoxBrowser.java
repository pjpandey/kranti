package com.testing.kranti.framework.gui.browserItem;

import com.google.common.base.Optional;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.abstrct.WebBrowserType;
import com.testing.kranti.framework.gui.actions.FirefoxSeleniumActions;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import com.testing.kranti.framework.operation.commmand.AbstractCommandOperation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

public class FirefoxBrowser extends Browser {

    public FirefoxBrowser(TestComponentData data, String baseTestUrl, CustomTimeouts timeoutsConfig, Optional<String> webDriverPath, Optional<String> browserBinaryPath, Optional<String> browserVersion, String seleniumHubURL) {
        super(data, baseTestUrl, timeoutsConfig, webDriverPath, browserBinaryPath, browserVersion, seleniumHubURL);
    }

    @Override
    public WebBrowserType getBrowserType() {
        return WebBrowserType.FIREFOX;
    }

    @Override
    public DesiredCapabilities getDesiredCapabilities() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
        desiredCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

        FirefoxProfile profile = new FirefoxProfile();
        //profile.setEnableNativeEvents(true);
        desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);

        Optional<String> browserBinaryPath = getBrowserBinaryPath();
        if (browserBinaryPath.isPresent() && !browserBinaryPath.get().isEmpty()) {
            final String browserBinaryPathStr = browserBinaryPath.get();
            File file = new File(browserBinaryPathStr);
            if (file.exists()) {
                desiredCapabilities.setCapability(FirefoxDriver.BINARY, new FirefoxBinary(file));
            }
        }
        Optional<String> browserVersion = getBrowserVersion();
        if (browserVersion.isPresent() && !browserVersion.get().isEmpty()) {
            desiredCapabilities.setCapability(CapabilityType.VERSION, browserVersion.get());
        }
        return desiredCapabilities;
    }

    @Override
    protected WebDriver startWebDriver() {
        DesiredCapabilities desiredCapabilities = getDesiredCapabilities();
        return new FirefoxDriver(desiredCapabilities);
    }

    @Override
    public FirefoxSeleniumActions getActions() {
        return new FirefoxSeleniumActions(this);
    }

    @Override
    protected AbstractCommandOperation getHealthOperation() {
        return new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "fireFixHealthOp";
            }
        };
    }

    @Override
    protected String getTestedApplication() {
        return "fireforx";
    }
}

