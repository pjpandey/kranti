package com.testing.kranti.framework.rules.persistency;

import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiForceCleanPersistencyLeftoversOnTeardownRule extends AbstractMethodRule {
    private boolean m_bForceCleanPersistencyLeftovers;

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        boolean annotationPresentOnClass = oTest.getClass().isAnnotationPresent(KrantiForceCleanPersistencyLeftoversOnTeardown.class);
        boolean annotationPresentOnMethod = mTest.getMethod().isAnnotationPresent(KrantiForceCleanPersistencyLeftoversOnTeardown.class);
        setForceCleanPersistencyLeftovers(annotationPresentOnMethod || annotationPresentOnClass);
    }

    public void setForceCleanPersistencyLeftovers(boolean forceCleanPersistency) {
        this.m_bForceCleanPersistencyLeftovers = forceCleanPersistency;
    }

    public boolean isForceCleanPersistencyLeftovers() {
        return m_bForceCleanPersistencyLeftovers;
    }
}

