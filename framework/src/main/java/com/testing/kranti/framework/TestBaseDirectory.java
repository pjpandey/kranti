package com.testing.kranti.framework;

import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.env.WindowsEnvironment;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.Calendar;

public class TestBaseDirectory {

    public static String generate(TestCase tTestCase) {
        if (null != RegressionEnvironment.BaseDir.getValue()) {
            return TestEnvironment.BASE_DIR_ROOT + RegressionEnvironment.BaseDir.getValue();
        }
        String sRandomDirName = getRandomTestDirName(tTestCase);
        return TestEnvironment.BASE_DIR_ROOT + sRandomDirName;
    }

    private static String getRandomTestDirName(TestCase tTestCase) {
        String sRandomDirName = tTestCase.getClass().getSimpleName();
        if (tTestCase.getTestName() == null || !tTestCase.getTestName().startsWith("_")) {
            if (tTestCase.getTestName() == null) {
                TestReporter.TRACE("method name is null, should only happen when running repeat" + tTestCase);
            }
            sRandomDirName = sRandomDirName + "_" + tTestCase.getTestName();
        }
        return sRandomDirName;
    }

    public static String generateWindows(TestCase tTestCase) {
        String sRandomDirName = getRandomTestDirName(tTestCase);
        Calendar now = Calendar.getInstance();
        sRandomDirName += "_" + formatNumber(now.get(Calendar.DAY_OF_MONTH));
        sRandomDirName += "_" + formatNumber(now.get(Calendar.MONTH) + 1);
        sRandomDirName += "_" + formatNumber(now.get(Calendar.HOUR_OF_DAY));
        sRandomDirName += "_" + formatNumber(now.get(Calendar.MINUTE));
        return WindowsEnvironment.SANDBOX_DIR_ROOT + sRandomDirName;
    }

    private static String formatNumber(int i) {
        if (i == 0) {
            return "00";
        } else if (i < 10) {
            return "0" + i;
        } else {
            return Integer.toString(i);
        }
    }
}
