package com.testing.kranti.framework.gui.actions;

import java.util.function.Predicate;

public class SeleniumActionPredicate<T> implements Predicate<seleniumCallable<T>> {

    @Override
    public boolean test(seleniumCallable<T> subjectCallable) {
        T el = null;
        try {
            el = subjectCallable.call();
        } catch (Exception e) {
            return false;
        }
        if (el == null) {
            return false;
        }
        return true;
    }

    @Override
    public Predicate<seleniumCallable<T>> and(Predicate<? super seleniumCallable<T>> other) {
        return null;
    }

    @Override
    public Predicate<seleniumCallable<T>> negate() {
        return null;
    }

    @Override
    public Predicate<seleniumCallable<T>> or(Predicate<? super seleniumCallable<T>> other) {
        return null;
    }
}
