package com.testing.kranti.framework.rules.env;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Inherited
public @interface KrantiMandatoryEnv {
    String[] value();
}
