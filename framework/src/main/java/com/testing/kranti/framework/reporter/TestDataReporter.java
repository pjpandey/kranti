package com.testing.kranti.framework.reporter;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class TestDataReporter {

    private static Map<String, TestDataReporterItem> testData;

    static {
        init();
    }

    public static void init() {
        testData = Maps.newHashMap();
    }

    public static void addData(String sKey, Double sValue) {
        boolean toBeReported = false;
        boolean isGeneric = false;
        boolean increaseType = false;
        addData(sKey, sValue, toBeReported, isGeneric, increaseType);
    }

    public static void addData(String sKey, Double sValue, boolean toBeReported, boolean isGeneric, boolean sIncreaseType) {
        TestReporter.TRACE("Adding data : sKey:" + sKey + " sValue:" + sValue + " toBeReported: " + toBeReported + " isGeneric:" + isGeneric + " increaseType:" + sIncreaseType);
        testData.put(sKey, new TestDataReporterItem(sKey, sValue, toBeReported, isGeneric, sIncreaseType));
    }

    public static void addDataMap(String sKey, Map<String, String> itemMap) {
        try {
            for (String itemkey : itemMap.keySet()) {
                double value = Double.parseDouble(itemMap.get(itemkey));
                if (testData.containsKey(getFullKey(sKey, itemkey))) {
                    testData.get(getFullKey(sKey, itemkey)).updateItem(value);
                } else {
                    addData(getFullKey(sKey, itemkey), value);
                }
            }
        } catch (Exception e) {
            TestReporter.TRACE("Error while reporting " + itemMap + " for " + sKey + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getFullKey(String sKey, String item) {
        return sKey.toLowerCase() + "." + item.toLowerCase();
    }

    public static List<TestDataReporterItem> getTestData() {
        return Lists.newArrayList(testData.values());
    }

}
