package com.testing.kranti.framework.rules.exceptions;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface KrantiIgnoredLeaks {
    String[] value();
}
