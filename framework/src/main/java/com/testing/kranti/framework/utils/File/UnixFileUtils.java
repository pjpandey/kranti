package com.testing.kranti.framework.utils.File;

import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.check.Check;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.List;

public class UnixFileUtils implements IFileUtils
{
	@Override
	public void createDirectory(String sFullpath, String sHost) {
		Check.assertTrue(TestCommandExecution.runCommand("mkdir -p " + sFullpath,sHost).getExitStatus() == 0, "Creating directory " + sFullpath);
	}

	@Override
	public void deleteDirectory(String sFullpath, String sHost) {
		OpResult cResult = deleteDirectoryWithoutCheck(sFullpath, sHost);
		Check.assertTrue(cResult.getExitStatus() == 0, "Delete directory " + sFullpath);
	}

	public static OpResult deleteDirectoryWithoutCheck(String sFullpath, String sHost) {
		return TestCommandExecution.runCommand("rm -rf " + sFullpath,sHost);
	}

	@Override
	public Boolean fileExists(String sHost, String sPath) {
		Boolean retValue = true;
		TestReporter.TRACE("Finding File existance of : " + sPath);
		String[] arrCmd = { "/usr/bin/stat", "--format", "%i", sPath };
		CommandResult result = TestCommandExecution.runCommand(arrCmd, sHost);
		if (result.getExitStatus() != 0) {
			retValue = false;
		}
		return retValue;
	}
	@Override
	public OpResult getFileContent(String sPath, String sHost)
	{
		TestReporter.TRACE("Checking File Content for path : " + sPath);
		String[] arrCmd = {"/bin/cat", sPath};
		return KrantiFileUtils.runCommandOnRemoteHost(arrCmd,sHost);
	}
	@Override
	public List<String> getStringMatches(String path, String pattern, String sHost)
	{
		OpResult result = TestCommandExecution.runCommand("/bin/cat " + path, sHost);
		return result.getStdOut();
	}

	@Override
	public void deleteFile(String sFile, String sHost)
	{
		TestCommandExecution.runCommand("rm -f " + sFile,sHost);
	}

	@Override
	public String getJobRecoveryFilePath(String fullID)
	{
		return "/tmp/NetStar." + fullID + ".recovery.v2";
	}
}