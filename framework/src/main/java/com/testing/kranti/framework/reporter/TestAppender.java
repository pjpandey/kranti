package com.testing.kranti.framework.reporter;

import com.testing.kranti.framework.KrantiTestRunner;
import com.testing.kranti.framework.operation.commmand.CommandRequest;
import com.testing.kranti.framework.operation.OpResult;
import org.apache.log4j.Logger;

import java.io.File;


public class TestAppender implements TestReporterAppender {
    protected static final Logger log = Logger.getLogger(TestAppender.class);

    private int iCommandNum = 0;
    private IOutputFileStrategy fileStrategy;

    public void incrementCommandNum() {
        iCommandNum++;
    }

    public TestAppender(IOutputFileStrategy strategy) {
        fileStrategy = strategy;
    }

    @Override
    public void logMessage(String sMessage, boolean bError) {
        if (!bError) {
            log.info(generateCaller() + " - " + sMessage);
        } else {
            log.error(generateCaller() + " - " + sMessage + generateException());
        }
    }

    @Override
    public void logMessage(String sMessage, Throwable t) {
        log.error(generateCaller() + " - " + sMessage, t);
    }

    @Override
    public void traceCommandExecution(CommandRequest cRequest, OpResult rResult) {
        try {
            String sCommandResultPath = dumpCommandResults(cRequest, rResult);
            logMessage("executed command " + iCommandNum + ": " + String.join(" ", cRequest.getCommand()), false);
            logMessage("result in: file://" + sCommandResultPath, false);
        } catch (Exception e) {

        }
    }

    @Override
    public void traceWebRequestExecution(String url, String sRequest, int iStatusCode, StringBuffer bOutput) {
        String sResultPath = dumpWebResults(sRequest, iStatusCode, bOutput, url);
        logMessage("executed request " + sRequest, false);
        logMessage("result in file :" + url + ":" + " file://" + sResultPath, false);
    }

    protected String generateCaller() {
        try {
            StackTraceElement ste[] = new Exception().getStackTrace();
            for (int i = 0; i < ste.length; i++) {
                String fileName = ste[i].getFileName().replace(".java", "");
                if (fileName.equals(TestReporter.class.getSimpleName())
                        || fileName.equals(TestAppender.class.getSimpleName())
                        || fileName.equals(TestOutputStreamAppender.class.getSimpleName())
                        || fileName.equals(TestConsoleAppender.class.getSimpleName())
                        || fileName.equals(KrantiTestRunner.class.getSimpleName())
                ) {
                    continue;
                }
                return fileName + "." + ste[i].getMethodName() + "()";
            }
        } catch (final Throwable t) {
        }
        return "";
    }

    protected String generateException() {
        try {
            StackTraceElement ste[] = new Exception().getStackTrace();
            String sException = "";
            for (int i = 0; i < ste.length; i++) {
                if (ste[i].getClassName().equals(TestReporter.class.getName())) {
                    continue;
                }
                if (!ste[i].getClassName().startsWith("ct.air.")) {
                    break;
                }
                sException += "\n\tat " + ste[i].getClassName() + "." + ste[i].getMethodName() + "(" + ste[i].getFileName() + ":" + ste[i].getLineNumber() + ")";
            }
            return sException;
        } catch (final Throwable t) {
        }
        return "";
    }

    @Override
    public void init() {
        resetCounters();
    }

    private void resetCounters() {
        iCommandNum = 0;
    }

    protected int getCommandNum() {
        return iCommandNum;
    }

    protected synchronized void setCommandNum(int iCommandNum) {
        this.iCommandNum = iCommandNum;
    }

    private synchronized String dumpCommandResults(CommandRequest cRequest, OpResult rResult) {
        iCommandNum++;
        String sCommandFile = fileStrategy.getCommandFile(iCommandNum);
        String sFileName = TestReporter.getOutputDir() + File.separator + TestReporter.COMMANDS_DIR + File.separator + sCommandFile;
        writeToFile(cRequest, rResult, sFileName);
        return sFileName;
    }

    private synchronized String dumpWebResults(String sRequest, int iStatusCode, StringBuffer bOutput, String sUrl) {
        iCommandNum++;
        String sCommandFile = fileStrategy.getCommandFile(iCommandNum);
        String sFileName = TestReporter.getOutputDir() + File.separator + TestReporter.COMMANDS_DIR + File.separator + sCommandFile;
        writeToFile(sRequest, iStatusCode, bOutput, sUrl, sFileName);
        return sFileName;
    }


    protected void writeToFile(String sRequest, int iStatusCode, StringBuffer bOutput, String sUrl, String sFileName) {


    }

    protected void writeToFile(CommandRequest cRequest, OpResult rResult, String sFileName) {

    }


}
