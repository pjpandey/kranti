package com.testing.kranti.framework.utils.time;


public class TimeTranslationException extends Exception {
    TimeTranslationException(String message) {
        super(message);
    }
}
