package com.testing.kranti.framework.store;

import com.testing.kranti.framework.TestComponent;
import com.testing.kranti.framework.TestComponentData;

public abstract class AbstractComponentFactory<T extends TestComponent> {
    private TestComponentData data = null;
    private String application = null;
    private T component = null;

    protected AbstractComponentFactory(String aApplication) {
        super();
        setApplication(aApplication);
    }

    public final void init(TestComponentData dData) {
        data = dData;
        component = createComponent();
    }

    protected TestComponentData getComponentData() {
        return data;
    }

    protected abstract T createComponent();

    public String getApplication() {
        return application;
    }

    private void setApplication(String application) {
        this.application = application;
    }

    public abstract String getName();
}
