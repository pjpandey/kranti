package com.testing.kranti.framework.gui;

import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.abstrct.WebBrowserType;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;

public class BrowserFactory {
    public Browser getBrowser(TestComponentData data, String baseTestUrl, String seleniumHubURL, WebBrowserType WebBrowserType, CustomTimeouts customTimeout) {
        BrowserBuilder browserBuilder = BrowserBuilder.getBuilder(WebBrowserType, baseTestUrl, seleniumHubURL);
        browserBuilder.setTimeoutsConfig(customTimeout);
        Browser browser = browserBuilder.build(data);
        //BrowserLocator.setBrowser(browser);
        return browser;
    }

    private BrowserFactory() {

    }

    public static BrowserFactory getSelector() {
        return new BrowserFactory();
    }
}
