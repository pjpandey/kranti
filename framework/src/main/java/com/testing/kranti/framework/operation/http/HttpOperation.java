package com.testing.kranti.framework.operation.http;

import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.operation.Operation;

public class HttpOperation implements Operation {


    private HttpOpRequest request;
    private HttpOpResponse response;

    @Override
    public void execute() {
        switch (request.getRequestType()){
            case GET:
                processGet();
                break;
            case POST:
                processPost();
                break;
            case PUT:
                processPut();
                break;
            case DELETE:
                processDelete();
                break;
            default:
                defaultImpl();
                break;
        }
    }

    private void defaultImpl() {

    }
    private void processDelete() {

    }

    private void processPut() {

    }

    private void processPost() {

    }

    private void processGet() {

    }

    @Override
    public OpResult getResult() {
        return null;
    }

    @Override
    public boolean shouldRunInBackground() {
        return false;
    }

    @Override
    public String getName() {
        return null;
    }


}
