package com.testing.kranti.framework.rules.forcerestart;

import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiForceRestartComponentsOnTestStartRule extends AbstractMethodRule {
    private boolean m_bForceRestart = false;

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        setForceRestart(mTest.getMethod().isAnnotationPresent(KrantiForceRestartComponents.class) || oTest.getClass().isAnnotationPresent(KrantiForceRestartComponents.class) || oTest.getClass().getPackage().isAnnotationPresent(KrantiForceRestartComponents.class));
    }

    private void setForceRestart(boolean forceRestart) {
        this.m_bForceRestart = forceRestart;
    }

    public boolean isForceRestart() {
        return m_bForceRestart;
    }
}
