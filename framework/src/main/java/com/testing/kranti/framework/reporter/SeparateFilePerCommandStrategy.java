package com.testing.kranti.framework.reporter;

public class SeparateFilePerCommandStrategy implements IOutputFileStrategy {

    @Override
    public String getCommandFile(int commandNum) {
        return "command_file" + commandNum;
    }

}
