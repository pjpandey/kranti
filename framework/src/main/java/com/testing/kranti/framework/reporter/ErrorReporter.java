package com.testing.kranti.framework.reporter;

import com.testing.kranti.framework.TestCase;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.TestComponent;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.store.ApplicationStore;
import com.testing.kranti.framework.utils.LocalHost;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.function.Function;

public class ErrorReporter {
    private TestCase testCase = null;
    private int iErrorNum = 0;
    private String sOutputDir = null;
    private int iPass = 0;
    private int iFail = 0;
    private int iFatal = 0;
    private Throwable fatalThrowable;
    private boolean errorLogging = false;
    private String sFirstFailure = null;
    private static boolean saveConfigurationOnError = true;
    private static boolean bSaveInformation = true;
    private static boolean captureSnapshotOnError = true;

    public ErrorReporter(TestCase tTest, String sOutputDir) {
        testCase = tTest;
        this.sOutputDir = sOutputDir;
    }

    private String getCurrentErrorDir() {
        return getOutputDir() + File.separator + "error" + iErrorNum;
    }

    private String getOutputDir() {
        return sOutputDir;
    }

    public void pass() {
        iPass++;
    }

    public void fail(String sMessage) {
        if (errorLogging) {
            return;
        }
        saveFailure(sMessage);
        iFail++;
        reportError();
    }

    public void fatal(Throwable t) {
        if (errorLogging) {
            return;
        }
        iFatal++;
        fatalThrowable = t;
        reportError();
    }

	/*private void captureSnapshot() {
		for(ITestComponent testComponent : getTestCase().getTestComponents())
		{
			testComponent.captureSnapshotOnFailure();
		}
		
	}*/

    private TestCase getTestCase() {
        return testCase;
    }

    private void reportError() {
        if (!bSaveInformation) {
            return;
        }
        try {
            errorLogging = true;
            iErrorNum++;
            String sErrorDir = getCurrentErrorDir();
            new File(sErrorDir).mkdir();

            Function<TestComponent, Void> fCopyOnErrorFunction = cComponent -> {
                if (null == cComponent) {
                    return null;
                }
                TestReporter.TRACE("saving information on error for " + cComponent);
                String sTargetDir = getCurrentErrorDir() + File.separator + cComponent.getComponentType() + "_" + cComponent.getHost();
                new File(sTargetDir).mkdirs();

                if (saveConfigurationOnError) {
                    saveFileOnError(cComponent.getHost(), cComponent.getConfigurationDir(), sTargetDir);
//						saveFileOnError(cComponent.getHost(), "/tmp/" + TestEnvironment.getUser() +"/" + cComponent.getComponentType().charAt(0)+ "*" +  cComponent.getHost() + ".startup",sTargetDir);

                }

                if (captureSnapshotOnError) {
                    String sSnapshotDir = sTargetDir + "/snapshotOnError/";
                    new File(sSnapshotDir).mkdir();
                    captureSnapshotOnError(cComponent, sSnapshotDir);
                }

                return null;
            };

            getTestCase().getTestComponents().stream().map(fCopyOnErrorFunction);
            ApplicationStore.getInstance().getStartedComponents().stream().map(fCopyOnErrorFunction);
        } finally {
            errorLogging = false;
        }
    }

    private void saveFileOnError(String sSourceHost, String sSourcePath, String sDestination) {
        if (TestEnvironment.isWindowsHost(sSourceHost)) {
            return;
        }
        TestReporter.TRACE("saving file:" + TestCommandExecution.runCommand("/usr/bin/realpath " + sSourcePath, sSourceHost).getStdOut().toString() + " on error");
        if (StringUtils.isEmpty(sSourcePath)) {
            return;
        }
        String sCommand = "";
        if (LocalHost.isLocalHost(sSourceHost)) {
            sCommand = "sh -c 'cp -frL --preserve=timestamps " + sSourcePath + " " + sDestination + "/'";
        } else {
            sCommand = "/usr/bin/rsync -rtl --copy-unsafe-links " + sSourceHost + ":" + sSourcePath + " " + sDestination + "/";
        }
        TestCommandExecution.runCommand(sCommand);
    }

    private void captureSnapshotOnError(TestComponent cComponent, String sTargetDir) {
        cComponent.captureSnapshotOnError(sTargetDir);
    }

    public int getPass() {
        if (iFail + iFatal + iPass <= 0) {
            return 1;
        }
        return iPass;
    }

    public int getFail() {
        return iFail;
    }

    public int getFatal() {
        return iFatal;
    }

    public void dump() {
        for (String sHost : TestEnvironment.getHosts()) {
            if (!LocalHost.isLocalHost(sHost)) {
                saveFileOnError(sHost, getOutputDir(), getOutputDir());
            }
        }
    }

    public String getFirstFailure() {
        return sFirstFailure;
    }

    private void saveFailure(String sFailure) {
        if (StringUtils.isEmpty(sFirstFailure)) {
            sFirstFailure = sFailure;
        }
    }

    public Throwable getFatalThrowable() {
        return fatalThrowable;
    }

    public static void setSaveInformation(boolean saveInformation) {
        bSaveInformation = saveInformation;
    }

    public static void setSaveConfigurationOnError(boolean m_bSaveConfigurationOnError) {
        ErrorReporter.saveConfigurationOnError = m_bSaveConfigurationOnError;
    }

    public static void setCaptureSnapshotOnError(boolean m_bCaptureSnapshotOnError) {
        ErrorReporter.captureSnapshotOnError = m_bCaptureSnapshotOnError;
    }

}
