package com.testing.kranti.framework.reporter;

import com.testing.kranti.framework.operation.commmand.CommandRequest;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.utils.LocalHost;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;

public class TestFileAppender extends TestAppender {

    public TestFileAppender(IOutputFileStrategy strategy) {
        super(strategy);
    }

    @Override
    protected void writeToFile(CommandRequest cRequest, OpResult rResult, String sFileName) {
        try {
            FileWriter fFileWriter = new FileWriter(new File(sFileName), true);
            fFileWriter.append("Command " + getCommandNum() + ": " + String.join(" ", cRequest.getCommand()) + "\n");
            fFileWriter.append("CurrentTime: " + new Date() + "\n");
            String sHost = getTargetHost(cRequest);
            fFileWriter.append("Hostname: " + sHost + "\n");
            fFileWriter.append("ExitStatus: " + rResult.getExitStatus() + "\n");
            fFileWriter.append("Stdout: " + String.join("\n", rResult.getStdOut()) + "\n");
            fFileWriter.append("Stderr: " + String.join("\n", rResult.getStdErr()) + "\n");
            fFileWriter.close();
        } catch (Exception e) {
            logMessage("failed write to file", e);
        }
    }

    private String getTargetHost(CommandRequest cRequest) {
        String sHost = cRequest.getHost();
        if (sHost == null) {
            sHost = "LocalHost";
        } else if (LocalHost.isLocalHost(sHost)) {
            sHost = sHost + " (LocalHost)";
        }
        return sHost;
    }

    @Override
    protected void writeToFile(String sRequest, int iStatusCode, StringBuffer bOutput, String sUrl, String sFileName) {
        try {
            FileWriter fFileWriter = new FileWriter(new File(sFileName), true);
            fFileWriter.append("Url: " + sUrl + "\n");
            fFileWriter.append("Request " + getCommandNum() + ": " + sRequest + " " + "\n");
            fFileWriter.append("Response: " + bOutput + "\n");
            fFileWriter.append("Response Code:" + iStatusCode + "\n");
            fFileWriter.close();
        } catch (Exception e) {
            logMessage("failed write to file", e);
        }

    }

}
