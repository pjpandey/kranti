package com.testing.kranti.framework.gui.actions;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import com.testing.kranti.framework.gui.timeout.TimeoutType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface ISeleniumActions {

    public Actions getActionsBuilder();

    public <B extends Browser> B getBrowser();

    public void setBrowser(Browser browser);

    public void acceptAlert(TimeoutType timeout);

    public void dismissAlert(TimeoutType timeout);

    public WebElement clickNoWait(By locator);

    @Deprecated
    public WebElement click(By locator, TimeoutType timeout);

    public WebElement click(WebElement el, TimeoutType timeout);

    public WebElement clickAndVerifyPresent(By locatorToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public WebElement clickAndVerifyPresent(WebElement elToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public WebElement clickAndVerifyVisible(By locatorToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public WebElement clickAndVerifyVisible(WebElement elToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public void clickAndVerifyNotPresent(By locatorToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public void clickAndVerifyNotPresent(WebElement elToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public void clickAndVerifyNotVisible(By locatorToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public void clickAndVerifyNotVisible(WebElement elToClick, By locatorToVerifyPresent, TimeoutType timeout);

    public void clickAndSelectFromList(By locatorToClick, By popoverLocator);

    public void clickAndSelectFromList(WebElement clickable, By popoverLocator);

    public WebElement clearText(By locator);

    public WebElement clearText(WebElement el);

    public Object executeJavascript(String script);

    public void waitForJavascriptSymbolToBeDefined(String symbol, TimeoutType timeout);

    public void waitForJavascriptSymbolToHaveValue(String symbol, String value, TimeoutType timeout);

    public boolean exists(By locator);

    public boolean exists(By locator, int iRetries);

    public boolean exists(By locator, WebElement parentEl);

    public WebElement findElementWithRefresh(By locator, TimeoutType timeout);

    public WebElement findVisibleElementWithRefresh(By locator, TimeoutType timeout);

    public WebElement findElementContainingText(By locator, String text);

    public WebElement findVisibleElementContainingText(By locator, String text);

    public WebElement findVisibleElementContainingTextWithWait(final By locator, final String text, TimeoutType timeout);

    public WebElement findElementContainingTextWithRefresh(final By locator, final String text, TimeoutType timeout);

    public WebElement findVisibleElementContainingTextWithRefresh(final By locator, final String text, TimeoutType timeout);

    public WebElement findElementContainingTextWithWait(By locator, String text, TimeoutType timeout);

    public @Nullable
    WebElement getElement(By locator);

    public @Nonnull
    WebElement getElementWithWait(By locator);

    public @Nullable
    WebElement getChildElement(By locator, WebElement parentEl);

    public @Nonnull
    WebElement getChildElementWithWait(By locator, WebElement parentEl);

    public List<WebElement> getElements(By locator);

    public List<WebElement> getChildElements(By locator, WebElement parentEl);

    public WebElement getParentElement(WebElement el);

    public WebElement inputText(By locator, String text);

    public WebElement inputText(@Nonnull WebElement el, String text);

    public WebElement inputTextAndSelectFromList(WebElement inputField, String value, By popoverLocator);

    public WebElement inputTextAndSelectFromList(WebElement inputField, String value, By popoverLocator, int withRetryCount);

    public WebElement inputTextSlowly(By locator, String text);

    public WebElement inputTextSlowly(WebElement el, String text);

    public WebElement inputTextSlowlyAndSelectFromList(WebElement inputField, String value, By popoverLocator);

    public WebElement inputTextSlowlyAndSelectFromList(WebElement inputField, String value, By popoverLocator, int withRetryCount);

    public void enterTextForAutoCompleteAndSelectFirstMatch(By inputLocator, String text, By popoverLocator,
                                                            String requiredPopupText);

    public void enterTextForAutoCompleteAndSelectFirstMatch(By inputLocator, int minChars, String text, By popoverLocator,
                                                            String requiredPopupText);

    public void inputTinyMceText(String text);

    public void waitForTinyMceToBeReady();

    public boolean isClickable(By locator);

    public boolean isClickable(WebElement el);

    public boolean isVisible(By locator);

    public boolean isVisible(WebElement css);

    public void scrollIntoView(By locator);

    public void scrollIntoView(WebElement el);

    public void scrollIntoView(By scrollContainerLocator, By locator);

    public void scrollIntoView(By scrollContainerLocator, WebElement el);

    public void verifyElementContainsText(By locator, String text, TimeoutType timeout);

    public void verifyElementSelected(By locator, TimeoutType timeout);

    public void verifyElementSelected(WebElement el, TimeoutType timeout);

    public void verifyElementNotSelected(By locator, TimeoutType timeout);

    public void verifyElementNotSelected(WebElement el, TimeoutType timeout);

    public WebElement verifyElementPresent(By locator, TimeoutType timeout);

    public void verifyElementNotPresent(By locator, TimeoutType timeout);

    public void verifyElementWithTextNotPresent(By locator, String text, TimeoutType timeout);

    public WebElement verifyElementVisible(By locator, TimeoutType timeout);

    public void verifyElementInvisible(By locator, TimeoutType timeout);

    public void verifyElementWithTextIsInvisible(By locator, String text, TimeoutType timeout);

    public <T, V> V waitOnFunction(Function<T, V> function, T input, String message, TimeoutType timeout);

    public <T> void waitOnPredicate(Predicate<T> predicate, T input, String message, TimeoutType timeout);

    public void waitOnPredicate(Predicate<Object> predicate, String message, TimeoutType timeout);

    public <T> void waitOnPredicateWithRefresh(Predicate<T> predicate, T input, String message, TimeoutType timeout);

    public void waitOnPredicateWithRefresh(Predicate<Object> predicate, String message, TimeoutType timeout);

    public <T> T waitOnExpectedCondition(ExpectedCondition<T> expectedCondition, String message, TimeoutType timeout);

    public WebElement verifyPageRefreshed(WebElement elementFromBeforeRefresh, By locatorAfterRefresh, TimeoutType timeout);

    public WebElement waitUntilClickable(By locator, TimeoutType timeout);

    public WebElement waitUntilClickable(WebElement el, TimeoutType timeout);

    public boolean doesElementHaveClass(By locator, String locatorClass);

    public WebElement verifyElementHasClass(By locator, String locatorClass, TimeoutType timeout);

    public WebElement verifyElementDoesNotHaveClass(final By locator, final String locatorClass, TimeoutType timeout);

    public CustomTimeouts getTimeoutsConfig();

    public void select(By locator, String sText);

    public void upload(By locator, TimeoutType timeout, String sPath);

    public void rightClick(By locator, int sNumKeyDown, TimeoutType timeout);

    public WebElement rightClick(By rightClicklocator, By optionLocator, TimeoutType timeout);

    void selectKendoUi(String sText, int iTableNumber);

    public boolean isElementClickable(By locator);

    boolean verifySelectOptionExist(By locator, String sText);

    List<String> getAllDropDownList(By locator);

    void selectByClickAndInput(By locator, By loc, String sValue);

    public WebElement rightClickOnly(By rightClicklocator, TimeoutType timeout);

    int getDivsCount(String string, String string2);

    void mouseHover(By locator);

    void doubleclick(By locator);

    WebElement clearText(By locator, TimeoutType timeout);

    void select(By locator, String sText, TimeoutType timeoutType);

    boolean verifySelectOptionExist(By locator, String sText,
                                    TimeoutType timeoutType);

    List<String> getAllDropDownList(By locator, TimeoutType timeoutType);

    WebElement verifyElementPresent(By locator);

    void verifyElementContainsText(By locator, String text);

    WebElement inputText(By locator, String text, TimeoutType timeout);

    void rightClick(By locator, int sNumKeyDown);

    WebElement click(By locator);

    WebElement rightClickOnly(By rightClicklocator);

    <T> T waitAndCheckOnExpectedCondition(ExpectedCondition<T> expectedCondition, String message, TimeoutType timeout);

    void upload(By locator, String sPath);

    void verifyElementNotPresent(By locator);

    public String getTextWithJSFunc(By id);

    boolean isElementSelected(By locator, String sAttributeName);

    void dragAndDrop(By fromLocator, By toLocator);

    void selectKendoUi(String sText, String sAttributeAriaOwns);

    String getAlertText();
}
