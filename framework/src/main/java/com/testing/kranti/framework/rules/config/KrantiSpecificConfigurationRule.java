package com.testing.kranti.framework.rules.config;

import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class KrantiSpecificConfigurationRule extends AbstractMethodRule {
    private List<String> m_lstDirectives = new ArrayList<String>();

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        extractSpecificConfiguration(oTest.getClass().getAnnotation(KrantiSpecificConfiguration.class), mTest.getAnnotation(KrantiSpecificConfiguration.class));
        if (!RegressionEnvironment.isRegression()) {
            TestEnvironment.setUseExistingConfiguration(null != mTest.getAnnotation(KrantiUseCurrentConfiguration.class) || oTest.getClass().isAnnotationPresent(KrantiUseCurrentConfiguration.class));
        }
    }

    private void extractSpecificConfiguration(KrantiSpecificConfiguration... arrSpecificConfigurations) {
        for (KrantiSpecificConfiguration eSpecificConfiguration : arrSpecificConfigurations) {
            if (null != eSpecificConfiguration) {
                m_lstDirectives.addAll(Arrays.asList(eSpecificConfiguration.value()));
            }
        }
    }

    public boolean adjustConfiguration(Map<String, String> replacementMap) {
        boolean restartRequired = false;
        for (String sDirective : m_lstDirectives) {
            adjustConfigurationForDirective(sDirective, replacementMap);
        }
        return restartRequired;
    }

    private void adjustConfigurationForDirective(String sValue, Map<String, String> replacementMap) {
        String updatedValue = sValue;
        for (Entry<String, String> e : replacementMap.entrySet()) {
            updatedValue = updatedValue.replace(e.getKey(), e.getValue());
        }
        //KrantiConfigurationUpdater.changeConfiguration(updatedValue);
    }
}
