package com.testing.kranti.framework.operation.commmand.impl;

import java.io.File;
import java.io.IOException;

interface CommandProcessFactory {
    CommandProcess createAndExec(String[] cmd, File cwd, String[] env, String user) throws IOException;
}
