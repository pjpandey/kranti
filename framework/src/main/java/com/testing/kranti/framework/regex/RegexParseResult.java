package com.testing.kranti.framework.regex;

import java.util.List;

public class RegexParseResult {
    private List<String> m_lstParsedItems;
    private boolean m_bMatch;

    public RegexParseResult(List<String> lstItems, boolean bMatch) {
        m_lstParsedItems = lstItems;
        m_bMatch = bMatch;

    }

    public boolean isMatch() {
        return m_bMatch;
    }

    public List<String> getParsedItems() {
        return m_lstParsedItems;
    }

}
