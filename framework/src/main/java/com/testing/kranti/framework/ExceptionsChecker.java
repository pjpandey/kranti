package com.testing.kranti.framework;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.configuration.kranti.KrantiConfiguration;
import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.reporter.TestDataReporter;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.utils.File.KrantiFileUtils;
import com.testing.kranti.framework.utils.File.FileUtil;
import org.apache.commons.lang3.StringUtils;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.NotSerializableException;
import java.util.*;
import java.util.stream.Collectors;

public class ExceptionsChecker {
    private static Class[] EXCEPTIONS =
            {
                    NullPointerException.class,
                    ArrayIndexOutOfBoundsException.class,
                    ConcurrentModificationException.class,
                    NoSuchMethodError.class,
                    ClassNotFoundException.class,
                    NoSuchElementException.class,
                    ClassCastException.class,
                    ArithmeticException.class,
                    OutOfMemoryError.class,
                    StackOverflowError.class,
                    IllegalMonitorStateException.class,
                    IllegalStateException.class,
                    AssertionError.class,
                    IllegalArgumentException.class,
                    NotSerializableException.class,
                    StringIndexOutOfBoundsException.class,
                    UnsupportedOperationException.class,
                    LoginException.class,
            };

    private static String[] MESSAGES =
            {
                    " FATAL "
            };

    private Set<Class> exceptions = Sets.newHashSet();
    private Set<String> messages = Sets.newHashSet();
    private String ignoredMessages = "";
    private List<String> lstExcludedFilesPatten = new ArrayList<>();
    private boolean fullExceptionMessage = false;

    public ExceptionsChecker() {
        initSet(exceptions, EXCEPTIONS);
        initSet(messages, MESSAGES);
    }

    private <T> void initSet(Set<T> st, T[] arr) {
        for (T t : arr) {
            st.add(t);
        }
    }

    public void checkExceptions(String sDir) {
        Set<String> lstAllMessages = getAllMessagesToBeChecked();
        String[] arrCommand = buildGrepCommand(sDir, getPatternForGrep(lstAllMessages));
        for (String sHost : TestEnvironment.getHosts()) {
            CommandResult rResult = TestCommandExecution.runCommand(arrCommand, sHost);
            logExceptionsFound(sDir, lstAllMessages, sHost, rResult);
        }
    }

    protected void logExceptionsFound(String sDir, Set<String> lstAllMessages, String sHost, CommandResult rResult) {
        if (rResult.getExitStatus() == 0) {
            TestDataReporter.addData("exceptions", 1.0);
            final List<String> stdOut = rResult.getStdOut();
            String sExceptionsString = "Exceptions found: " + constructExceptionsString(stdOut);
            if (TestEnvironment.isPerformance() && RegressionEnvironment.isRegression()) {
                TestReporter.TRACE(sExceptionsString);
            } else {
                TestReporter.FAIL(sExceptionsString);
            }
            try {
                File fExceptionsFile = new File(sDir + "/exceptions." + sHost);
                fExceptionsFile.createNewFile();
                FileUtil.setContents(fExceptionsFile, String.join("\n", stdOut));
                TestReporter.TraceLink("see exceptions in file", fExceptionsFile.getAbsolutePath());
            } catch (Exception e) {
            }
            String fileName = getExceptionFile(stdOut);
            String sGrepCommand2 = "egrep -20rw '" + getPatternForGrep(lstAllMessages) + "' " + fileName;
            String[] arrCommand2 = {"sh", "-c", sGrepCommand2};
            CommandResult rResult2 = TestCommandExecution.runCommand(arrCommand2, sHost);
            TestReporter.TRACE("\nDetails from log file " + fileName + ":"
                    + "\n\n\n====================================================================\n\n\n"
                    + String.join("\n", rResult2.getStdOut())
                    + "\n\n\n====================================================================\n\n\n");
        }
        TestDataReporter.addData("exceptions", 0.0);
    }

    protected String getPatternForGrep(Set<String> lstAllMessages) {
        return Joiner.on("|").join(lstAllMessages);
    }

    protected String[] buildGrepCommand(String sDir, String sPattern) {
        String sExcludedFilesPattern = buildExcludedFilesPatten();
        String sGrepCommand = "egrep -rw " + sExcludedFilesPattern + " --include '*.log' --include '*.out' '" + sPattern + "' " + sDir + "/* | grep -v /test.log | grep -v /commands/command_file | grep -v grep | grep -v codeine_perl_lib/exceptions.pl | grep -v /exceptions.";
        if (!StringUtils.isEmpty(ignoredMessages)) {
            sGrepCommand += "| grep -v '" + ignoredMessages + "'";
        }
        String[] arrCommand = {"sh", "-c", sGrepCommand};
        return arrCommand;
    }

    protected Set<String> getAllMessagesToBeChecked() {
        List<String> lstAllMessages = exceptions.stream().map((Function<Class, String>) subject -> subject.getSimpleName()).collect(Collectors.toList());
        lstAllMessages.addAll(messages);
        return Sets.newHashSet(lstAllMessages);
    }

    public void checkSuspiciousPatterns(String sDir, KrantiConfiguration krantiConf) {

    }

    private String getExceptionFile(List<String> stdOut) {
        String result = "";
        boolean first = true;
        for (String e : stdOut) {
            if (!first) {
                result += "\n";
            }
            first = false;
            final String[] split = e.split(":");
            String exception = split[1];
            String file = split[0];
            result = exception + " in " + file;
            return file;
        }
        return null;
    }

    private String constructExceptionsString(final List<String> stdOut) {
        String result = "";
        boolean first = true;
        for (String e : stdOut) {
            if (!first) {
                result += "\n";
            }
            first = false;
            int limitForSplit = fullExceptionMessage ? 2 : 0;
            final String[] split = e.split(":", limitForSplit);
            if (split.length > 1) {
                String exception = split[1];
                String file = split[0];
                result = exception + " in " + file;
            }
        }
        return result;
    }

    public void removeException(Class exception) {
        exceptions.remove(exception);
    }

    public void addException(Class exception) {
        exceptions.add(exception);
    }

    public void setIgnoredMessages(String ignoredMessages) {
        this.ignoredMessages = ignoredMessages;
    }

    public boolean checkSpecificExpection(String sExceptionMessage, String sLogFilePath) {
        if (KrantiFileUtils.grepCount(sExceptionMessage, sLogFilePath) > 0) {
            return true;
        }
        return false;
    }

    private String buildExcludedFilesPatten() {
        StringBuilder sExcludedFilesPattern = new StringBuilder();
        for (String sSingleExcludedFilesPattern : lstExcludedFilesPatten) {
            sExcludedFilesPattern.append(" --exclude '").append(sSingleExcludedFilesPattern).append("'");
        }
        return sExcludedFilesPattern.toString();
    }

    public void setExcludedFilesPatterns(List<String> excludedFilesPatterns) {
        lstExcludedFilesPatten = excludedFilesPatterns;
    }

    void setFullExceptionMessage(boolean fullExceptionMessage) {
        this.fullExceptionMessage = fullExceptionMessage;
    }
}
