package com.testing.kranti.framework.utils.process;

import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

abstract class AbstractProcessUtils implements IProcessUtils {
    transient static Logger log = Logger.getLogger(AbstractProcessUtils.class);

    protected final List<String> runCommand(String sHost, String[] cmd) throws Exception {
        CommandResult rResult = TestCommandExecution.runCommand(cmd, sHost);
        if (rResult.getExitStatus() != 0) {

            throw new Exception("negative exit status");
        }
        return rResult.getStdOut();
    }

    protected List<String> runCommandNoException(String sHost, String[] cmd) {
        try {
            return runCommand(normalizeHost(sHost), cmd);
        } catch (Exception e) {
            log.warn("runCommandnoException() - got exception while running command " + String.join(" ", cmd) + " on " + sHost);
            return Collections.emptyList();
        }
    }

    protected String normalizeHost(String sHost) {
        return sHost;
    }


}
