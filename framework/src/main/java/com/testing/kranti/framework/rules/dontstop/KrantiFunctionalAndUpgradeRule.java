package com.testing.kranti.framework.rules.dontstop;

import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiFunctionalAndUpgradeRule extends AbstractMethodRule {

    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        if (RegressionEnvironment.isUpgradeRegression()) {
            boolean bFunctionalAndUpgrade = (null != mTest.getAnnotation(KrantiFunctionalAndUpgradeTest.class)) || (null != oTest.getClass().getAnnotation(KrantiFunctionalAndUpgradeTest.class)) || (null != oTest.getClass().getPackage().getAnnotation(KrantiFunctionalAndUpgradeTest.class));
            TestEnvironment.setComponentsUpgradeRequired(bFunctionalAndUpgrade);
        }
    }
}
