package com.testing.kranti.framework.gui.browserItem;

import com.testing.kranti.framework.reporter.TestReporter;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteWebDriverUtils {
    public static WebDriver startRemoteWebDriver(String seleniumHubUrl, Capabilities desiredCapabilities) {
        try {
            RemoteWebDriver driver = new RemoteWebDriver(new URL(seleniumHubUrl), desiredCapabilities);
            return driver;
        } catch (MalformedURLException e) {
            TestReporter.TRACE("Failed to start remoteWebDriver, caught MalformedException \n" + e);
        } catch (Exception e) {
            TestReporter.TRACE("Failed to start remote web driver \n" + e);
        }

        return null;
    }

}
