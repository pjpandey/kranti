package com.testing.kranti.framework.env;

public enum RegressionEnvironment {
    ConfFile("REGRESSION_CONF_FILE"),
    BaseDir("REGRESSION_BASE_DIR"),
    IsRegression("IS_REGRESSION"),
    Application("REGRESSION_APPLICATION"),
    IsSmartCoverage("REGRESSION_SMART_COVERAGE"),
    UpgradeRegression("REGRESSION_WITH_UPGRADE"),
    DowngradeRegression("REGRESSION_WITH_DOWNGRADE"),
    ;

    private String key = null;

    RegressionEnvironment(String sKey) {
        key = sKey;
    }

    public String getValue() {
        if (null != System.getProperty(key)) {
            return System.getProperty(key);
        }

        if (null != System.getenv(key)) {
            return System.getenv(key);
        }
        return null;
    }

    public void setValue(String sValue) {
        System.setProperty(key, sValue);
    }

    public static boolean isRegression() {
        return IsRegression.getValue() != null;
    }

    public static boolean isUpgradeRegression() {
        return UpgradeRegression.getValue() != null;
    }

    public static boolean isDowngradeRegression() {
        return DowngradeRegression.getValue() != null;
    }

    public static boolean isSmartCoverage() {
        return IsSmartCoverage.getValue() != null;
    }


}
