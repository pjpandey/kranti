package com.testing.kranti.framework.rules.persistency;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface KrantiForceCleanPersistencyLeftoversOnTeardown
{

}