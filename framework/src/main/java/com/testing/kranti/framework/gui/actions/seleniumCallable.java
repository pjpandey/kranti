package com.testing.kranti.framework.gui.actions;

import java.util.concurrent.Callable;

public interface seleniumCallable<T> extends Callable<T> {
    T getObject();
}