package com.testing.kranti.framework.annot;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
@Inherited
@TestRequirement
public @interface PerformanceTest {

}

