package com.testing.kranti.framework.gui.actions;


import com.testing.kranti.framework.gui.browserItem.FirefoxBrowser;

public class FirefoxSeleniumActions extends SeleniumActions<FirefoxBrowser> {
    public FirefoxSeleniumActions(FirefoxBrowser browser) {
        super(browser);
    }
}