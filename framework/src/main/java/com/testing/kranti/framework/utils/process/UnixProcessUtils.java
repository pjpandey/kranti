package com.testing.kranti.framework.utils.process;

import com.google.common.collect.Lists;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.reporter.TestReporter;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UnixProcessUtils extends AbstractProcessUtils implements IProcessUtils {
    public static String PS_CMD = "/bin/ps";
    public static String KILL_CMD = "/bin/kill";
    public static String GREP_CMD = "/usr/bin/grep";
    public static String TASKSET_CMD = "/usr/bin/taskset";


    @Override
    public List<String> getChildProcessIDs(String sHost, Integer sPPID) {
        TestReporter.TRACE("looking for matches: " + sPPID);
        String[] arrCmd = {PS_CMD, "-ejH"};
        List<String> colProcesses = runCommandNoException(sHost, arrCmd);
        final String sMatch = "" + sPPID;
        colProcesses = colProcesses.stream().filter(sProcess -> ((sProcess.toLowerCase().contains(sMatch)) &&
                (!sProcess.toLowerCase().contains(PS_CMD)))).collect(Collectors.toList());

        colProcesses = colProcesses.stream().map(sProcess -> {
            String[] arrParts = sProcess.split(" ");
            return arrParts[0];
        }).collect(Collectors.toList());

        if (!colProcesses.isEmpty()) {
            colProcesses.remove(sMatch);
        }
        return colProcesses;
    }

    @Override
    public List<String> getChildProcessIDUsingPPiD(String sHost, Integer sPPID) {
        TestReporter.TRACE("looking for matches: " + sPPID);
        String[] arrCmd = {PS_CMD, "-eo", "pid,ppid"};
        List<String> colProcesses = runCommandNoException(sHost, arrCmd);
        final String sMatch = "" + sPPID;
        colProcesses = colProcesses.stream().filter(sProcess -> ((sProcess.toLowerCase().contains(sMatch)) &&
                (!sProcess.toLowerCase().contains(PS_CMD)))).collect(Collectors.toList());

        colProcesses = colProcesses.stream().map(sProcess -> {
            String[] arrParts = sProcess.split(" ");
            return arrParts[0];
        }).collect(Collectors.toList());

        if (!colProcesses.isEmpty()) {
            colProcesses.remove(sMatch);
        }
        return colProcesses;
    }

    @Override
    public Integer getppid(String sHost, Integer pid) {
        String sPid = Integer.toString(pid);
        String[] arrCmd = {PS_CMD, "-f", sPid};
        List<String> colProcesses = runCommandNoException(sHost, arrCmd);
        for (String sLine : colProcesses) {
            String[] arrFields = sLine.split("\\s+");
            if (arrFields.length >= 3 && arrFields[1].equals(sPid)) {
                return Integer.parseInt(arrFields[2]);
            }
        }
        return null;
    }


    @Override
    public String getpgid(String sHost, Integer pid) {
        String sPid = Integer.toString(pid);
        String[] arrCmd = {PS_CMD, "-eo", "pid,pgid"};
        List<String> colProcesses = runCommandNoException(sHost, arrCmd);
        final String sMatch = "" + sPid;
        colProcesses = colProcesses.stream().filter(sProcess -> ((sProcess.toLowerCase().contains(sMatch)) &&
                (!sProcess.toLowerCase().contains(PS_CMD)))).collect(Collectors.toList());

        for (int index = 0; index < colProcesses.size(); index++) {
            String pgid = colProcesses.get(index);
            String[] m_pgid = pgid.trim().split(" ");
            if (m_pgid[0].equals(sPid)) {
                return m_pgid[1];
            }
            index++;
        }
        return null;
    }


    public boolean processExistence(String sHost, int iPid) {
        return processRunning(sHost, iPid);
    }

    @Override
    public boolean processRunning(String sHost, int iPid) {
        if (iPid <= 0) {
            return false;
        }
        String[] cmd = {PS_CMD, "-p", Integer.toString(iPid)};
        return !CollectionUtils.isEmpty(runCommandNoException(sHost, cmd));
    }

    @Override
    public String getProcessStatus(String sHost, int iPid) {
        return getProcessField(sHost, iPid, "state");
    }

    @Override
    public String getProcessField(String sHost, int iPid, String sField) {
        if (iPid > 0) {
            String[] cmd = {PS_CMD, "-p", Integer.toString(iPid), "-o", sField};
            List<String> lstResult = runCommandNoException(sHost, cmd);
            if (lstResult.size() > 1) {
                return lstResult.get(1);
            }
        }
        throw new RuntimeException("No status for process " + iPid + " on " + sHost);
    }

    @Override
    public void killProcess(String sHost, int... pids) {
        for (int iPid : pids) {
            sendSignal(sHost, iPid, Integer.toString(-9));
        }
    }

    @Override
    public void sendSignal(String sHost, int iPid, String iSignal) {
        if (iPid <= 0) {
            return;
        }
        String[] cmd = {KILL_CMD, iSignal, Integer.toString(iPid)};
        runCommandNoException(sHost, cmd);
    }

    @Override
    public void suspendProcess(String sHost, int... pids) {
        for (int iPid : pids) {
            sendSignal(sHost, iPid, Integer.toString(-19));
        }
    }

    @Override
    public void resumeProcess(String sHost, int... pids) {
        for (int iPid : pids) {
            sendSignal(sHost, iPid, Integer.toString(-20));
        }
    }

    @Override
    public List<String> getProcessTable(String sHost) {
        String[] arrCmd = {PS_CMD, "-efww"};
        return runCommandNoException(sHost, arrCmd);
    }

    public OpResult getCpuListOfProcessOutput(String sHost, String sProcessId) {
        return null;
    }

    @Override
    public int getCpuListOfProcess(String sHost, String sProcessId) {
        return -1;
    }

    @Override
    public List<String> getSleepCommand(String sHost, String numOfSec) {
        List<String> lst = Lists.newArrayList();
        lst.add("/bin/sleep");
        lst.add(numOfSec);
        return lst;
    }

    @Override
    public Long getCpuUtilizationOfProcess(String sHost, String sMatches) {
        return null;
    }

    @Override
    public ArrayList<Integer> getThreadIDs(String sHost, String sPPID) {
        return null;
    }
}
