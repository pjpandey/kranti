package com.testing.kranti.framework.reporter;

public interface IOutputFileStrategy
{
	String getCommandFile(int commandNum);
}
