package com.testing.kranti.framework.utils.time;

public class TimeUnitsScale {
    private static final long TIME_SCALE_MINUTES_FACTOR = 60 * 1000L;
    private static final long TIME_SCALE_DAYS_FACTOR = 24 * 60 * 60 * 1000L;

    public static final long DAY_TO_MINUTE_COMPRESSION_FACTOR = 24 * 60;

    private static long CUSTOMIZED_TIME_SCALE_FACTOR = Long.MIN_VALUE;

    public static boolean SIMULATION = false;

    public static long TIME_SCALE_FACTOR = TIME_SCALE_DAYS_FACTOR;

    public static boolean isSimulation() {
        return SIMULATION;
    }

    public static void reset() {
        if (SIMULATION) {
            if (0 < CUSTOMIZED_TIME_SCALE_FACTOR) {
                TIME_SCALE_FACTOR = CUSTOMIZED_TIME_SCALE_FACTOR;
            } else {
                TIME_SCALE_FACTOR = TIME_SCALE_MINUTES_FACTOR;
            }
        } else {
            TIME_SCALE_FACTOR = TIME_SCALE_DAYS_FACTOR;
        }
    }
}
