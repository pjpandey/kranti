package com.testing.kranti.framework.utils.File;

import com.google.common.collect.Lists;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.check.Check;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.utils.LocalHost;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class KrantiFileUtils {
    public static String COMMAND = "-command";
    private static WindowsFileUtils windowsHost = new WindowsFileUtils();
    private static UnixFileUtils unixHost = new UnixFileUtils();
    private static int nFile = 1;


    public static void createDirectory(String sFullpath, String sHost) {
        getSpecificUtils(sHost).createDirectory(sFullpath, sHost);
    }

    public static void deleteDirectory(String sFullpath, String sHost) {
        getSpecificUtils(sHost).deleteDirectory(sFullpath, sHost);
    }

    public static Boolean fileExists(String sPath) {
        return fileExists(LocalHost.getPhysicalShort(), sPath);
    }

    public static Boolean fileExists(String sHost, String sPath) {
        return getSpecificUtils(sHost).fileExists(sHost, sPath);
    }

    public static List<String> getStringMatches(String path, String pattern, String sHost) {
        List<String> result = getSpecificUtils(sHost).getStringMatches(path, pattern, sHost);
        return getMatches(result, pattern);
    }

    public static String getJobRecoveryFilePath(String fullID) {
        IFileUtils fu = TestEnvironment.isWindowsTest() ? windowsHost : unixHost;
        return fu.getJobRecoveryFilePath(fullID);
    }

    public static void deleteFile(String sFullpath, String sHost) {
        getSpecificUtils(sHost).deleteFile(sFullpath, sHost);
    }

    public static void remove(String sFullpath, String sHost) {
        TestCommandExecution.runCommand("rm -rf " + sFullpath, sHost);
    }

    public static List<String> listFileInWindows(String sDir, String sHost) {
        OpResult runCommand = TestCommandExecution.runCommand(new String[]{WindowsFileUtils.WINDOWS_POWERSHELL, COMMAND, "dir \"" + sDir + "\""}, sHost);
        if (runCommand.getExitStatus() == 0) {
            ArrayList<String> sDirs = new ArrayList<String>();
            for (String sDi : runCommand.getStdOut()) {
                if (sDi.startsWith("d----") || sDi.startsWith("da---")) {
                    String[] s = sDi.split("\\s+");
                    sDirs.add(s[s.length - 1]);
                }
            }
            return sDirs;
        }
        return new ArrayList<>();
    }

    public static boolean pathExists(String path) {
        return new File(path).exists();
    }

    public static boolean remotePathExists(String sHost, String path) {
        CommandResult res = TestCommandExecution.runCommand(new String[]{"/usr/bin/ssh", "-x", sHost, "/usr/bin/test -e '" + path + "'"});
        return 0 == res.getExitStatus();
    }

    public static boolean isLocalPath(String path) {
        return !path.startsWith("/nfs");
    }

    public static long getSizeinKB(String pathname) {
        long sizeInBytes = getSize(pathname);
        return sizeInBytes / 1024;
    }

    public static long getSizeinMB(String pathname) {
        long sizeInBytes = getSize(pathname);
        return sizeInBytes / (1024 * 1024);
    }

    public static long getSize(String pathname) {
        File file = new File(pathname);
        return getSize(file);
    }

    public static long getSize(File file) {
        long size = 0;
        if (!file.exists()) {
            return size;
        }

        if (file.isFile()) {
            size += file.length();
        } else {
            for (File iterator : file.listFiles()) {
                try {
                    if (!iterator.getCanonicalPath().equals(iterator.getAbsolutePath())) {
                        continue;
                    }
                } catch (IOException ex) {
                    continue;
                }

                if (iterator.isFile()) {
                    size += iterator.length();
                } else {
                    size += getSize(iterator);
                }
            }
        }
        return size;

    }

    public static Integer findDiskUsage(String sPath) {
        return findDiskUsage(LocalHost.getPhysicalShort(), sPath);
    }

    public static Integer findDiskUsage(String sHost, String sPath) {
        TestReporter.TRACE("Finding disk usage of: " + sPath);
        String[] arrCmd = {"du", "-s", sPath};
        CommandResult result = TestCommandExecution.runCommand(arrCmd, sHost);
        if (result.getExitStatus() != 0) {
            return null;
        }
        int size = parseDiskSize(result);
        return size / (1024);

    }

    public static int parseDiskSize(CommandResult result) {
        int size = Integer.parseInt(result.getStdOut().get(0).split("\\s+")[0]);
        return size;
    }

    public static int findTotalLines(String sPath) {
        return findTotalLines(LocalHost.getPhysicalShort(), sPath);
    }

    public static int findTotalLines(String sHost, String sPath) {
        TestReporter.TRACE("Finding Number of lines in: " + sPath);
        String[] arrCmd = {"wc", "-l", sPath};
        CommandResult result = TestCommandExecution.runCommand(arrCmd, sHost);
        if (result.getExitStatus() != 0) {
            return 0;
        }
        return Integer.parseInt(result.getStdOut().get(0).split("\\s+")[0]);
    }

    public static void createDirectory(String sFullpath) {
        createDirectory(sFullpath, null);
    }

    public static void createSubDirecotries(String sMainDirectory, String[] sSubdirs) {
        for (String subdir : sSubdirs) {
            Check.assertTrue(TestCommandExecution.runCommand("mkdir -p " + sMainDirectory + "/" + subdir).getExitStatus() == 0, "Creating directory " + sMainDirectory + "/" + subdir);
        }

    }

    public static void deleteDirectory(String sFullpath) {
        deleteDirectory(sFullpath, null);
    }

    public static OpResult deleteDirectoryWithoutCheck(String sFullpath, String sHost) {
        return TestCommandExecution.runCommand("rm -rf " + sFullpath, sHost);
    }

    public static OpResult deleteDirectoryContents(String sFullpath, String sHost) {
        return TestCommandExecution.runCommand("rm -rf " + sFullpath + "/*", sHost);
    }

    public static void createFile(String sFileFullPath) {
        createFile(sFileFullPath, null);
    }

    public static void createFile(String sFileFullPath, String host) {
        Check.assertTrue(TestCommandExecution.runCommand("touch " + sFileFullPath, host).getExitStatus() == 0, "Creating File " + sFileFullPath);
    }

    /**
     *
     */
    public static void createNFilesOfSize(Integer filesCount, Integer sizeInMB, String parentFolder) {

        int no = sizeInMB * 1024;
        createFiles(filesCount, 1024, no, parentFolder);
    }

    public static void createFiles(Integer filesCount, Integer blockSize, Integer noOfBlocks, String parentFolder) {

        if (!new File(parentFolder).exists()) {
            Check.assertTrue(TestCommandExecution.runCommand("mkdir -p " + parentFolder).getExitStatus() == 0, "Creating the directory under dataset ");
        }
        int i;
        for (i = nFile; i < filesCount + nFile; i++) {
            Check.assertTrue(
                    TestCommandExecution.runCommand(
                            "dd of=" + parentFolder + "/file." + Integer.toString(i) + " if=/dev/zero bs=" + blockSize + " count="
                                    + Integer.toString(noOfBlocks)).getExitStatus() == 0, "Creating " + i + "file of  bs=" + blockSize + "count=" + noOfBlocks);
        }
        nFile = i;
    }

    public static void createFileOfSize(String sParentFolder, int sizeInMB, String sFileName) {
        if (!new File(sParentFolder).exists()) {
            Check.assertTrue(TestCommandExecution.runCommand("mkdir -p " + sParentFolder).getExitStatus() == 0, "Creating the directory under dataset ");
        }
        if (!new File(sParentFolder + "/" + sFileName).exists()) {
            int no = 1024 * sizeInMB;
            Check.assertTrue(TestCommandExecution.runCommand("dd of=" + sParentFolder + "/" + sFileName + " if=/dev/zero bs=1024 count=" + no).getExitStatus() == 0, "Creating file " + sParentFolder + "/" + sFileName + " of size " + sizeInMB);
        }
    }

    public static List<String> getStringMatches(String path, String pattern) {
        return getStringMatches(new File(path), pattern);
    }

    public static List<String> getStringMatches(File file, String pattern) {
        String filecontents = FileUtil.getContents(file);
        List<String> Lines = Arrays.asList(filecontents.split("\n"));
        return getMatches(Lines, pattern);
    }

    private static List<String> getMatches(List<String> lData, String pattern) {
        List<String> matches = new ArrayList<String>();

        for (String line : lData) {
            if (line.contains(pattern)) {
                matches.add(line);
            }
        }
        return matches;

    }

    public static String getFilePermissionInOctal(String sPath) {
        TestReporter.TRACE("Finding File permission for path : " + sPath);
        String[] arrCmd = {"/usr/bin/stat", "--format", "%a", sPath};
        return runCommand(arrCmd, null);

    }

    public static String getFilePermissionInOctal(String sPath, String sHost) {
        TestReporter.TRACE("Finding File permission for path : " + sPath);
        String[] arrCmd = {"/usr/bin/stat", "--format", "%a", sPath};
        return runCommand(arrCmd, sHost);

    }

    private static String runCommand(String[] arrCmd, String sHost) {
        CommandResult result = null;
        if (sHost == null) {
            result = TestCommandExecution.runCommand(arrCmd);
        } else {
            result = TestCommandExecution.runCommand(arrCmd, sHost);
        }
        if (result.getExitStatus() != 0) {
            return null;
        }
        return result.getStdOut().get(0);
    }

    public static OpResult runCommandOnRemoteHost(String[] arrCmd, String sHost) {
        CommandResult result = null;
        if (sHost == null) {
            result = TestCommandExecution.runCommand(arrCmd);
        } else {
            result = TestCommandExecution.runCommand(arrCmd, sHost);
        }
        if (result.getExitStatus() != 0) {
            return null;
        }
        return result;
    }

    public static String getFileOwner(String sPath) {
        TestReporter.TRACE("Finding File Owner for path : " + sPath);
        String[] arrCmd = {"/usr/bin/stat", "--format", "%U", sPath};
        return runCommand(arrCmd, null);

    }

    public static String getFileOwner(String sPath, String host) {
        TestReporter.TRACE("Finding File Owner for path : " + sPath);
        String[] arrCmd = {"/usr/bin/stat", "--format", "%U", sPath};
        return runCommand(arrCmd, host);
    }

    public static String getRemoteFileContent(String sPath, String sHost) {
        TestReporter.TRACE("Checking File Content for path : " + sPath);
        String[] arrCmd = {"/bin/cat", sPath};
        return runCommand(arrCmd, sHost);
    }

    public static OpResult getFileContent(String sPath, String sHost) {
        return getSpecificUtils(sHost).getFileContent(sPath, sHost);
    }

    public static String getFileGroup(String sPath) {
        TestReporter.TRACE("Finding File Group for path : " + sPath);
        String[] arrCmd = {"/usr/bin/stat", "--format", "%G", sPath};
        return runCommand(arrCmd, null);

    }

    public static void verifyFilesExists(final String sHost, final Boolean sStatus, final String... sPath) {
        for (String s : sPath) {
            verifyFileExists(sHost, sStatus, s);
        }
    }

    public static void verifyFileExists(final String sHost, final Boolean sStatus, final String sPath) {
        verifyFileExists(sHost, sStatus, sPath, 60);
    }

    public static void verifyFileExists(final String sHost, final Boolean sStatus, final String sPath, final int iTimeout) {
        Check.assertBusyWait(subject -> {
            Boolean yStatus = KrantiFileUtils.fileExists(sHost, sPath);
            return yStatus == sStatus;
        }, false, iTimeout, 1, sPath + " file does not exists");
    }

    public static int getNumberOfFiles(String path) {
        return getNumberOfFiles(new File(path));
    }

    public static int getNumberOfFiles(File file) {
        if (!file.exists() || file.isFile()) {
            return 0;
        }

        File[] listFiles = file.listFiles();
        if (listFiles.length > 0) {
            TestReporter.TRACE("File at: " + file.getPath() + " are: " + listFiles);
        }
        return listFiles.length;
    }

    public static int getNumberOfFiles(String sPath, String sHost) {
        return findNoOfFilesUsingFindCmd(sPath, sHost);
    }

    public static File[] getMatchedFiles(String sPath, final String sFileNamePattern, String... sHost) {
        File[] matchedFiles = new File(sPath).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.contains(sFileNamePattern);
            }
        });
        return matchedFiles;
    }

    private static int findNoOfFilesUsingFindCmd(String sPath, String sHost) {

        String[] arrCmd = {"/usr/bin/find", sPath, "-type", "f", "-print"};
        CommandResult result = TestCommandExecution.runCommand(arrCmd, sHost);
        if (result.getExitStatus() != 0 || (result.getStdOut().size() == 0)) {
            return 0;
        }
        String[] stringToArray = result.getStdOut().toString().split(" ");
        if (stringToArray.length > 0) {
            TestReporter.TRACE("Found " + stringToArray.length + " files in path: " + sPath + " first file is: " + stringToArray[0]);
        }
        return stringToArray.length;
    }

    public static String resolveLink(String src) {
        return FileUtil.resolveLinks(src);
    }

    public static String getTargetOfLink(String src) throws IOException {
        Path path = FileSystems.getDefault().getPath(src);
        Path Link = Files.readSymbolicLink(path);
        return Link.toString();
    }

    public static CommandResult grep(String sPattern, String sFile) {
        return grep(sPattern, sFile, LocalHost.getPhysicalLong());
    }

    public static CommandResult grep(String sPattern, String sFile, String sHostname) {
        String sGrepCommand = "egrep -rw '" + sPattern + "' " + sFile;
        String[] arrCommand = {"sh", "-c", sGrepCommand};
        CommandResult output = TestCommandExecution.runCommand(arrCommand, sHostname);
        return output;
    }

    public static int grepCount(String sPattern, String sFile) {
        return grepCount(sPattern, sFile, LocalHost.getPhysicalLong());
    }

    public static int grepCount(String sPattern, String sFile, String sHostname) {
        String sGrepCommand = "egrep -wc '" + sPattern + "' " + sFile;
        String[] arrCommand = {"sh", "-c", sGrepCommand};
        CommandResult output = TestCommandExecution.runCommand(arrCommand, sHostname);
        Pattern p = Pattern.compile("(\\d+)");
        Matcher m = p.matcher(output.getStdOut().get(0));
        if (m.find()) {
            return Integer.parseInt(m.group());
        }
        TestReporter.FATAL(output.getStdOut().toString());
        return -1;
    }

    public static void remove(File sFile) {
        FileUtil.delete(sFile);
    }

    public static void copyFile(File sFile, File dFile) {
        TestReporter.TRACE("Copying file from " + sFile.getAbsolutePath() + " to " + dFile.getAbsolutePath());
        FileUtil.copyFile(sFile, dFile);
    }

    public static void replace(String sToReplace, String sReplaceWith, String sFile) {
        replace(sToReplace, sReplaceWith, sFile, true);
    }

    public static void replacewithoutVerify(String sToReplace, String sReplaceWith, String sFile) {
        replace(sToReplace, sReplaceWith, sFile, false);
    }

    public static void replacewithoutVerify(String sToReplace, String sReplaceWith, String sFile, boolean addSpaceToEveryLine) {
        replace(sToReplace, sReplaceWith, sFile, false, addSpaceToEveryLine);
    }

    private static void replace(String sToReplace, String sReplaceWith, String sFile, boolean verifyNoLeftovers) {
        FileContentReplacer contentReplacer = new FileContentReplacer(verifyNoLeftovers);
        contentReplacer.addReplacement(sToReplace, sReplaceWith);
        try {
            contentReplacer.execute(new File(sFile));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void replace(String sToReplace, String sReplaceWith, String sFile, boolean verifyNoLeftovers, boolean addSpaceToEveryLine) {
        FileContentReplacer contentReplacer = new FileContentReplacer(verifyNoLeftovers);
        contentReplacer.addReplacement(sToReplace, sReplaceWith);
        contentReplacer.setAddSpaceToEveryLine(addSpaceToEveryLine);
        try {
            contentReplacer.execute(new File(sFile));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static List<String> findFile(String dirFullPath, String fileName) {
        return findFile(dirFullPath, fileName, null);
    }

    public static List<String> findFile(String dirFullPath, String fileName, String sHost) {
        String[] arrCmd = {"/usr/bin/find", dirFullPath, "-type", "f", "-name", fileName};
        CommandResult result = TestCommandExecution.runCommand(arrCmd, sHost);
        if (result.getExitStatus() != 0) {
            return Lists.newArrayList();
        }
        return result.getStdOut();
    }

    public static List<String> find(String dirFullPath, String sHost) {
        String[] arrCmd = {"/usr/bin/find", dirFullPath};
        CommandResult result = TestCommandExecution.runCommand(arrCmd, sHost);
        if (result.getExitStatus() != 0) {
            return Lists.newArrayList();
        }
        return result.getStdOut();
    }

    public static List<String> find(String dirFullPath) {
        return find(dirFullPath, null);
    }

    public static String getAbsolutePath(String sPath) {
        try {
            return new File(sPath).getCanonicalPath();
        } catch (Exception e) {
            return sPath;
        }
    }

    public static void createSymLink(String target, String filename) {
        Check.assertTrue(TestCommandExecution.runCommand("ln -s " + target + " " + filename, null).getExitStatus() == 0, "Creating symlink " + filename + " -> " + target);
    }

    public static List<String> getDirectoryList(String sHost, String sParent, final Predicate<String> predicate) {
        List<String> lstValidPath = new ArrayList<>();
        if (!(fileExists(sHost, sParent))) {
            return lstValidPath;
        }
        String sCommands[] = {"/usr/bin/tree", "-d", "-L", "1", "-i", "--noreport", sParent + "/"};
        OpResult iDirectories = runCommandOnRemoteHost(sCommands, sHost);
        for (String sPath : iDirectories.getStdOut()) {
            if (predicate.test(sPath) && !(sPath.equalsIgnoreCase(sParent + "/"))) {
                lstValidPath.add(sPath);
            }
        }
        return lstValidPath;
    }

    public static List<String> getMatchedFilesRecursive(String mPath, String mName, String mHost) {
        List<String> $ = Lists.newArrayList();
        List<String> subDirList = getDirectoryList(mHost, mPath, subject -> true);

        File[] files = getMatchedFiles(mPath, mName, mHost);
        if (files == null) {
            return $;
        }
        if (files.length > 0) {
            for (File i : files) {
                $.add(i.getPath());
            }
        }
        if (subDirList.size() != 0) {
            for (String subDir : subDirList) {
                $.addAll(getMatchedFilesRecursive(mPath + "/" + subDir, mName, mHost));
            }
        }
        return $;
    }

    public static List<String> findFilesRecursively(String mPath, String mName, String mHost) {
        ArrayList<String> lstFiles = Lists.newArrayList();
        String[] arrCmd = {"find", mPath, "-type", "f", "-name", mName};
        CommandResult result = TestCommandExecution.runCommand(arrCmd, mHost);
        if (result.getExitStatus() == 0) {
            for (String $ : result.getStdOut()) {
                if (!StringUtils.isEmpty($))
                    lstFiles.add($);
            }
        }
        return lstFiles;
    }

    public static Collection<String> readFileLineByLine(String sFile) {

        Collection<String> col = new ArrayList<String>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(sFile)));

            String sLine = null;
            while ((sLine = in.readLine()) != null) {
                sLine = sLine.trim();
                if (sLine.length() > 0) {
                    col.add(sLine);
                }
            }
        } catch (Exception e) {
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        return col;
    }

    public static void getAllFilesRecursively(String sPath, Collection<String> lstFiles) {
        File parentFile = new File(sPath);
        if (parentFile.exists()) {
            for (File file : parentFile.listFiles()) {
                if (file.isDirectory()) {
                    getAllFilesRecursively(file.getAbsolutePath(), lstFiles);
                }
                lstFiles.add(file.getAbsolutePath());
            }
        } else {
            TestReporter.TRACE(sPath + "Path not exists");
        }
    }

    private static IFileUtils getSpecificUtils(String sHost) {
        return TestEnvironment.isWindowsHost(sHost) ? windowsHost : unixHost;
    }
}