package com.testing.kranti.framework.rules.config;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
public @interface KrantiUseCurrentConfiguration {
}
