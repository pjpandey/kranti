package com.testing.kranti.framework.store;

import com.testing.kranti.framework.TestComponent;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.configuration.kranti.ApplicationConfiguration;
import com.testing.kranti.framework.configuration.kranti.KrantiConfiguration;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class ApplicationStore {
    private Collection<TestComponent> components = new ArrayList<TestComponent>();
    private List<TestComponent> startedComponents = Collections.synchronizedList(new ArrayList<TestComponent>());

    private static ApplicationStore instance = new ApplicationStore();
    private final TestComponentData.Builder builder;

    protected ApplicationStore() {
        builder = new TestComponentData.Builder();
    }

    public static ApplicationStore getInstance() {
        return instance;
    }

    public <T extends TestComponent> T get(Class<T> clazz, int iNum) {
        return get(clazz, iNum, subject -> true);
    }

    public <T extends TestComponent> T get(Class<T> clazz, int iNum, Predicate<TestComponent> predicate) {
        int i = 0;
        for (TestComponent tComp : components) {
            if (clazz.isAssignableFrom(tComp.getClass()) && predicate.test(tComp)) {
                if (++i == iNum) {
                    return (T) tComp;
                }
            }
        }

        TestReporter.FATAL("Can't find object " + clazz.getSimpleName() + "make sure the component is configured in the user configuration file");
        return null;
    }

    protected void add(TestComponent tComponent) {
        components.add(tComponent);
    }

    public void close() {
        for (TestComponent cComponent : startedComponents) {
            if (cComponent.isRunning()) {
                cComponent.stop();
            }
        }
        startedComponents.clear();
    }

    public void init(KrantiConfiguration cConfiguration, String sTestedApplication) {
        close();
        // TODO add again
    }

    protected TestComponentData createComponentData(KrantiConfiguration cConfiguration, ApplicationConfiguration cComponentConfiguration, String compName, int i) {
        TestComponentData dData = getTestComponentDataBuilder().build(cConfiguration.getHosts().get(i), cComponentConfiguration.getComponentInstallationPath(compName, i), cComponentConfiguration.getConfigurationPath(compName, i));
        return dData;
    }

    private TestComponentData.Builder getTestComponentDataBuilder() {
        return builder;
    }

    public List<TestComponent> getStartedComponents() {
        return startedComponents;
    }

    protected void addStarted(TestComponent cComponent) {
        TestReporter.TRACE("adding component " + cComponent);
        startedComponents.add(cComponent);
    }

    public void removeStartedComponent(TestComponent cComponent) {
        startedComponents.remove(cComponent);
    }
}
