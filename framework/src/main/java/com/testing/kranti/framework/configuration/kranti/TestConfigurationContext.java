package com.testing.kranti.framework.configuration.kranti;


import com.testing.kranti.framework.TestCase;

public class TestConfigurationContext {
    private String sApplication;
    private Class<? extends TestCase> cTestClass;
    private String sTestName;

    public TestConfigurationContext(String application, Class<? extends TestCase> testClass, String testName) {
        sApplication = application;
        cTestClass = testClass;
        sTestName = testName;
    }

    public String getApplication() {
        return sApplication;
    }

    protected void setApplication(String application) {
        sApplication = application;
    }

    public Class<? extends TestCase> getTestClass() {
        return cTestClass;
    }

    protected void setTestClass(Class<? extends TestCase> testClass) {
        cTestClass = testClass;
    }

    public String getTestName() {
        return sTestName;
    }

    protected void setTestName(String testName) {
        sTestName = testName;
    }

    @Override
    public String toString() {
        String sClass = cTestClass == null ? "null" : cTestClass.getSimpleName();
        return "TestConfigurationContext [sApplication=" + sApplication + ", cTestClass=" + sClass + ", sTestName=" + sTestName + "]";
    }
}
