package com.testing.kranti.framework;

import java.util.function.Predicate;

class ComponentStatusPredicate implements Predicate<TestComponent> {
    private boolean m_bCheckIfUp;

    public ComponentStatusPredicate(boolean checkIfUp) {
        m_bCheckIfUp = checkIfUp;
    }

    @Override
    public String toString() {
        return "ComponentStatusPredicate." + (m_bCheckIfUp ? "isUp?" : "isDown?");
    }


    @Override
    public boolean test(TestComponent subject) {
        boolean isRunning = subject.isRunning();
        if (m_bCheckIfUp) {
            return isRunning;
        }
        return !isRunning;
    }

    @Override
    public Predicate<TestComponent> and(Predicate<? super TestComponent> other) {
        return null;
    }

    @Override
    public Predicate<TestComponent> negate() {
        return null;
    }

    @Override
    public Predicate<TestComponent> or(Predicate<? super TestComponent> other) {
        return null;
    }
}
