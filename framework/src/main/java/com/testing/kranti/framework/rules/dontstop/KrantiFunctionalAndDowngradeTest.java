package com.testing.kranti.framework.rules.dontstop;

import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE})
@Inherited
public @interface KrantiFunctionalAndDowngradeTest
{	
}

