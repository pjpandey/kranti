package com.testing.kranti.framework;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.testing.kranti.framework.annot.IntegrationTest;
import com.testing.kranti.framework.annot.KrantiRestAPITest;
import com.testing.kranti.framework.annot.PerformanceTest;
import com.testing.kranti.framework.check.Check;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.configuration.kranti.*;
import com.testing.kranti.framework.configuration.kranti.writer.KrantiConfigurationWriter;
import com.testing.kranti.framework.env.RegressionEnvironment;
import com.testing.kranti.framework.gui.abstrct.WebBrowserType;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.parameterized.IParameterizedTest;
import com.testing.kranti.framework.parameterized.TestParametersManager;
import com.testing.kranti.framework.reporter.KrantiFatalOnFailRule;
import com.testing.kranti.framework.reporter.TestDataReporter;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.rules.config.KrantiFilesRule;
import com.testing.kranti.framework.rules.config.KrantiSpecificConfigurationRule;
import com.testing.kranti.framework.rules.dontstop.KrantiDontCleanLogOnPass;
import com.testing.kranti.framework.rules.dontstop.KrantiFunctionalAndDowngradeRule;
import com.testing.kranti.framework.rules.dontstop.KrantiFunctionalAndUpgradeRule;
import com.testing.kranti.framework.rules.dontstop.KrantiManageComponentOnTestFinishRule;
import com.testing.kranti.framework.rules.env.KrantiMandatoryEnvRule;
import com.testing.kranti.framework.rules.exceptions.KrantiAddExceptionRule;
import com.testing.kranti.framework.rules.exceptions.KrantiIgnoredLeakRule;
import com.testing.kranti.framework.rules.exceptions.KrantiIgnoredMessagesRule;
import com.testing.kranti.framework.rules.exceptions.krantiIgnoredExceptionRule;
import com.testing.kranti.framework.rules.forcerestart.KrantiForceRestartComponentsOnTestStartRule;
import com.testing.kranti.framework.rules.persistency.KrantiForceCleanPersistencyLeftoversOnTeardownRule;
import com.testing.kranti.framework.rules.shouldthrow.KrantiShouldThrowRule;
import com.testing.kranti.framework.store.ApplicationStore;
import com.testing.kranti.framework.utils.File.FileContentReplacer;
import com.testing.kranti.framework.utils.File.FileUtil;
import com.testing.kranti.framework.utils.File.KrantiFileUtils;
import com.testing.kranti.framework.utils.LocalHost;
import com.testing.kranti.framework.utils.time.TimeUnitsTranslator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestName;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@RunWith(KrantiTestRunner.class)
public abstract class AbstractTestCase implements TestCase {
    public static int MAX_RUN_TIME_MILLI = (int) TimeUnitsTranslator.fromMinutes(50);
    private final ObjectMapper objectMapper = new ObjectMapper();


    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();
    @Rule
    public TestName testName = new TestName();
    @Rule
    public KrantiShouldThrowRule shouldThrowRule = new KrantiShouldThrowRule();
    @Rule
    public KrantiManageComponentOnTestFinishRule manageComponentsOnTestFinish = new KrantiManageComponentOnTestFinishRule();
    @Rule
    public KrantiSpecificConfigurationRule specificConfigurationRule = new KrantiSpecificConfigurationRule();
    @Rule
    public KrantiForceRestartComponentsOnTestStartRule forceRestartComponents = new KrantiForceRestartComponentsOnTestStartRule();
    @Rule
    public KrantiForceCleanPersistencyLeftoversOnTeardownRule orceCleanPersistency = new KrantiForceCleanPersistencyLeftoversOnTeardownRule();
    @Rule
    public KrantiFilesRule preCopyRule = new KrantiFilesRule();
    @Rule
    public KrantiFunctionalAndUpgradeRule upgradeRule = new KrantiFunctionalAndUpgradeRule();
    @Rule
    public KrantiFunctionalAndDowngradeRule downgradeRule = new KrantiFunctionalAndDowngradeRule();
    @Rule
    public KrantiFatalOnFailRule fatalOnFail = new KrantiFatalOnFailRule();
    @Rule
    public Timeout testGlobalTimeout = new Timeout(MAX_RUN_TIME_MILLI);
    @Rule
    public krantiIgnoredExceptionRule ignoredExceptionRule = new krantiIgnoredExceptionRule();
    @Rule
    public KrantiAddExceptionRule additionalExceptionRule = new KrantiAddExceptionRule();
    @Rule
    public KrantiIgnoredMessagesRule ignoredMessagesRule = new KrantiIgnoredMessagesRule();
    @Rule
    public KrantiIgnoredLeakRule ignoreLeaksRule = new KrantiIgnoredLeakRule();
    @Rule
    public KrantiMandatoryEnvRule krantiMandatoryEnvRule = new KrantiMandatoryEnvRule();

    private String baseDir = null;
    private String windowsBaseDir = null;
    private KrantiConfiguration krantiConf = null;
    private boolean running = true;
    private boolean restartRequired = false;
    private TestConfigurationSearchStrategy confSearchStrategy;
    private ExceptionsChecker exceptionChecker;
    private String applicationToBeConfigured = null;
    protected final String KRANTI_CONFIGURATION_DIR = "/.kranti/";
    private final String currentApplication;

    static {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
    }

    protected AbstractTestCase(String aApplication) {
        currentApplication = aApplication;
    }

    @After
    public final void resetCommandExecution() {
        TestCommandExecution.reset();
    }

    @Before
    public final void init() {
        try {
            decideOnBaseDir();
            initKrantiConfiguration();
            initTestEnvironment();
            initExceptionsChecker();
            initReporter();
            initConfSearchStrategy();
            initApplicationsStore();
            TestReporter.TRACE("Copying configuration");
            setConfiguration();
            initTestCaseSpecific();
            TestReporter.TRACE("Initializing components");
            initComponents();
            initMemoryLeakCheckList();
            prepareTestEnvironment();
            prepareSandBoxDir();
            if (!isRestartNotRequired()) {
                TestReporter.TRACE("Cleaning & restarting");
                TestReporter.TRACE("Stopping components");
                stopComponents();
                TestReporter.TRACE("Cleaning with force");
                forceClean();
            }
            prepareComponentsForStart();
            if (isRestartNotRequired()) {
                initRunningComponents();
                TestReporter.TRACE("Cleaning from previous tests");
                clean();
                TestReporter.TRACE("Components up, hupping");
                hupComponents();
            } else {
                TestReporter.TRACE("Starting components");
                startComponents();
                _sleep(3L);
                clean();
            }
            prepareComponentsForExecution();
        } catch (Throwable t) {
            TestReporter.FATAL(t);
        }
        snapshotSpecific();
        TestReporter.TRACE("============================= Normal Startup sequence finished");
        TestEnvironment.setReadyTime();
    }


    public KrantiConfiguration getKrantiConf() {
        return krantiConf;
    }

    public String getCurrentApplication() {
        return currentApplication;
    }

    protected void snapshotSpecific() {
        //getSnapshotManager().refresh();
    }

    protected void prepareTestEnvironment() {
        Collection<String> colHosts = new HashSet<String>();
        for (TestComponent cComponent : getTestComponents()) {
            if (cComponent != null) {
                colHosts.add(cComponent.getHost());
            }
        }
        TestEnvironment.prepare(colHosts);
        preCopyRule.preCopy();
        if (specificConfigurationRule.adjustConfiguration(getConfigurationReplacementMap(getConfigurationPath(), null))) {
            setRestartRequired();
        }
        addMandatoryEnvToTestCommandExecution();
    }

    private void addMandatoryEnvToTestCommandExecution() {
        Map<String, String> replacementMap = getConfigurationReplacementMap(getConfigurationPath(), null);
        for (String sEnv : krantiMandatoryEnvRule.getMandatoryEnv()) {
            for (Entry<String, String> e : replacementMap.entrySet()) {
                sEnv = sEnv.replace(e.getKey(), e.getValue());
            }
            int iIndex = sEnv.indexOf('=');
            if (iIndex <= 0) {
                TestReporter.FATAL("bad env change directive: " + sEnv);
            }
            String sKey = sEnv.substring(0, iIndex);
            String sValue = sEnv.substring(iIndex + 1);
            TestCommandExecution.SetDefaultEnv(sKey, sValue);
        }

    }

    private void setRestartRequired() {
        restartRequired = true;
    }

    private void initRunningComponents() {
        onAllComponents(comp -> {
                    comp.initRunningComponent();
                    return null;
                }
        );
    }

    protected void prepareSandBoxDir() {
        onAllComponents(comp -> {
            if (TestEnvironment.isWindowsHost(comp.getHost())) {
                comp.setSandBoxDir(getWindowsSandBoxDir());
            } else {
                comp.setSandBoxDir(getSandBoxDir());
                comp.prepareSandBoxDir();
            }
            return null;
        });
    }

    protected void prepareComponentsForExecution() {
        onAllComponents(comp -> {
                    comp.prepareForExecution();
                    return null;
                }
        );
    }

    protected void prepareComponentsForStart() {
        TestReporter.TRACE("Prepare for start");
        onAllComponents(comp -> {
                    comp.prepareForStart();
                    return null;
                }
        );
    }

    protected void initConfSearchStrategy() {
        confSearchStrategy = new TestConfigurationSearchStrategy(getProjectHead(), getApplication(), getTestingMode());
        confSearchStrategy.setLocalWorkspaceMachine(getKrantiConfiguration().getLocalWorkspaceMachine());
    }

    protected String getTestingMode() {
        return "testing";
    }

    abstract protected void initComponents();

    @Override
    public final void tearDown() {
        try {
            tearDownComponents();
        } finally {
            finishReporter();
        }
    }

    protected void tearDownComponents() {
        try {
            setRunning(false);
            TestReporter.TRACE("tearDownComponents");
            TestReporter.TRACE("============================= Test Completed TearDown Started=====================");
            tearDownComponentsSpecific();
            closeApplicationsStore();
            if (manageComponentsOnTestFinish.shouldClean()) {
                TestReporter.TRACE("Managing Components On Test Finish");
                resumeSuspenedComponents();
                startNotStoppedComponents();
                cleanPersistencyLeftoversInComponents();
                checkTestSpecificLeaks();
                clean();
            }

            checkExceptions();
            checkMemoryLeaks();
        } finally {
            reportTestExecutionInfo();
            if (TestReporter.testFailed() && krantiConf.getHaltOnError()) {
                System.exit(5);
            }
            if (manageComponentsOnTestFinish.shouldStop()) {
                stopComponents();
                forceClean();
            }
        }
    }

    public void checkTestSpecificLeaks() {
        return;
    }

    protected void reportTestExecutionInfo() {

    }

    private void checkMemoryLeaks() {
    }

    protected void tearDownComponentsSpecific() {
    }

    private void startNotStoppedComponents() {
        for (TestComponent cComponent : getTestComponents()) {
            if (!cComponent.isRunning() && (cComponent.getProcessID() == -1)) {
                cComponent.start();
            }
        }
    }

    private void resumeSuspenedComponents() {
        for (TestComponent cComponent : getTestComponents()) {
            if (cComponent.isSuspended()) {
                cComponent.resumeComponent();
            }
        }
    }


    protected boolean isRestartNotRequired() {
        /*if (isWindowsTest()) {
            return false;
        }*/
        if (isRestartRequiredByTestCase()) {
            TestReporter.TRACE("returning false because isRestartRequiredByTestCase()");
            return false;
        }
        final Boolean[] bCompRunning = {true};
        onAllComponents(comp -> {
            if (!Objects.requireNonNull(comp).isRunning()) {
                TestReporter.TRACE("will restart because " + comp + "is not running");
                bCompRunning[0] = false;
            }
            return null;
        });
        if (!bCompRunning[0]) {
            TestReporter.TRACE("returning false because !bCompRunning.isTrue()");
            return false;
        }
        // should be checked only if components up
        if (isRestartRequiredByTestedCompoenents()) {
            TestReporter.TRACE("returning false isRestartRequiredByTestedCompoenents() ");
            return false;
        }
        TestReporter.TRACE("returning true ");
        return true;
    }

    private boolean isRestartRequiredByTestCase() {
        return forceRestartComponents.isForceRestart() || restartRequired;
    }

    protected boolean isRestartRequiredByTestedCompoenents() {
        return false;
    }

    private void initReporter() {
        TestReporter.init(this);
        TestReporter.start();
        TestDataReporter.init();

        if (this instanceof IParameterizedTest) {
            TestParametersManager.report();
        }
    }

    private void finishReporter() {
        TestReporter.finish();
    }

    @Override
    public String getTestName() {
        return testName.getMethodName();
    }

    @Override
    public String getTestGroup() {
        return getClass().getName();
    }

    private void decideOnBaseDir() {
        baseDir = TestBaseDirectory.generate(this);
        windowsBaseDir = TestBaseDirectory.generateWindows(this);
        FileUtil.delete(baseDir);
        FileUtil.createLink(TestEnvironment.BASE_DIR_ROOT + "current", baseDir);
    }

    @Override
    public String getSandBoxDir() {
        return baseDir;
    }

    public String getWindowsSandBoxDir() {
        return windowsBaseDir;
    }

    protected String getProjectHead() {
        return getKrantiConfiguration().getProjectHead();
    }


    protected String getSeleniumHubUrl() {
        return getKrantiConfiguration().getSeleniumHubUrl();
    }

    protected WebBrowserType getWebBrowserType() {
        String browserType = getKrantiConfiguration().getWebBrowserType().toUpperCase();
        if (browserType.equalsIgnoreCase("FIREFOX"))
            return WebBrowserType.FIREFOX;
        else if (browserType.equalsIgnoreCase("CHROME"))
            return WebBrowserType.CHROME;
        else if (browserType.equalsIgnoreCase("IE"))
            return WebBrowserType.IE;
        else
            return null;
    }

    protected String getKrantiConfigurationBaseFile() {
        String fromRegression = RegressionEnvironment.ConfFile.getValue();
        if (null != fromRegression) {
            TestReporter.TRACE("getKrantiConfigurationBaseFile " + fromRegression);
            return fromRegression;
        }
        return System.getenv("HOME") + KRANTI_CONFIGURATION_DIR + getApplication();
    }

    private void initKrantiConfiguration() {
        try {
            krantiConf = objectMapper.readValue(new File(getKrantiConfigurationBaseFile() + ".json"), KrantiConfiguration.class);
        } catch (Exception e) {
            TestReporter.FATAL(e);
        }
    }

    protected KrantiConfiguration getKrantiConfiguration() {
        return krantiConf;
    }

    protected abstract String getApplication();

    public List<String> getHosts() {
        return getKrantiConfiguration().getHosts();

    }

    protected List<String> getWindowsHosts() {
        return getKrantiConfiguration().getWindowsHosts();
    }

    protected void hupComponents() {
        onAllComponents(comp -> {
                    comp.hup();
                    return null;
                }
        );
    }

    protected void startComponents() {
        onAllComponents(comp -> {
            long lStart = System.currentTimeMillis();
            comp.start();
            long lFinish = System.currentTimeMillis();
            recordStartupTime(comp, lFinish - lStart);
            return null;
        });
    }

    protected void recordStartupTime(TestComponent comp, long l) {
        TestReporter.TRACE("Startup Time for " + comp.getComponentType() + " in host " + comp.getHost() + " : " + l);
    }

    protected boolean shouldActOnComponentsInParallel() {
        return true;
    }

    protected void stopComponents() {
        onAllComponents(comp -> {
                    comp.isRunning();
                    comp.stop();
                    return null;
                }
        );
    }

    protected void cleanComponents(final boolean bForce) {
        onAllComponents(comp -> {
                    comp.clean(bForce);
                    return null;
                },
                comp -> comp.getCleanOrder()
        );
    }

    private void cleanPersistencyLeftoversInComponents() {
        onAllComponents(comp -> {
                    comp.setShouldCleanPersistencyLeftoversOnTeardown(orceCleanPersistency.isForceCleanPersistencyLeftovers());
                    return null;
                }, null
        );
    }

    protected <T extends TestComponent> T getComponent(Class<T> c, int i) {
        return getApplicationStore().get(c, i);
    }

    protected <T extends TestComponent> T getComponent(Class<T> c, int i, Predicate<TestComponent> predicate) {
        return getApplicationStore().get(c, i);
    }

    private final void setConfiguration() {
        //TODO ? may be  use existing config
        TestReporter.TRACE("copyConfiguration() - started");
        try {
            for (String sApplication : getApplicationsForConfigurationCopy()) {
                TestConfigurationContext testContext = new TestConfigurationContext(sApplication, getClass(), getTestName());
                setConfiguration(testContext);
            }
        } catch (Throwable e) {
            TestReporter.FATAL(e);
        }
        try {
            setWindowsConfiguration();
        } catch (IOException ex) {
            TestReporter.FATAL(ex);
        }
    }

    protected Collection<String> getApplicationsForConfigurationCopy() {
        return getAllApplicationsForConfigurationCopy();
    }


    protected Collection<String> getAllApplicationsForConfigurationCopy() {
        return getKrantiConfiguration().getConfiguredApplications();
    }

    protected void setWindowsConfiguration() throws IOException {

    }

    protected void setConfiguration(TestConfigurationContext testContext) throws IOException {

        if (null != getUpgradedConfigurationPath(testContext.getApplication())) {
            cleanExistingConfiguration(getUpgradedConfigurationPath(testContext.getApplication()));
        }

        if (null != getDowngradedConfigurationPath(testContext.getApplication())) {
            cleanExistingConfiguration(getDowngradedConfigurationPath(testContext.getApplication()));
        }
        setApplicationToBeConfigured(testContext.getApplication());
        String installDir = getKrantiConfiguration().getApplicationConfiguration(testContext.getApplication()).getDefaultInstallationPath();
        setConfiguration(testContext, getConfigurationPath(testContext.getApplication()), installDir);

        if (null != RegressionEnvironment.ConfFile.getValue()) {
            for (String sHost : getHosts()) {
                setConfiguration(testContext, getConfigurationPath(testContext.getApplication()) + "." + sHost, installDir);
                if (null != getKrantiConfiguration().getApplicationConfiguration(testContext.getApplication()).getDefaultUpgradedInstallationPath()) {
                    setConfiguration(testContext, getUpgradedConfigurationPath(testContext.getApplication()) + "." + sHost, installDir);
                }
                if (null != getKrantiConfiguration().getApplicationConfiguration(testContext.getApplication()).getDefaultDowngradedInstallationPath()) {
                    setConfiguration(testContext, getDowngradedConfigurationPath(testContext.getApplication()) + "." + sHost, installDir);
                }
            }
        } else {
            for (ComponentInstallation comp : getKrantiConfiguration().getApplicationConfiguration(testContext.getApplication()).getComponents()) {
                for (String path : comp.getInstallationPath()) {
                    Preconditions.checkNotNull(path);
                    setConfiguration(testContext, path + "/conf", path);
                }

                for (String path : comp.getUpgradedInstallationPath()) {
                    Preconditions.checkNotNull(path);
                    cleanExistingConfiguration(path + "/conf");
                }
                for (String path : comp.getDowngradedInstallationPath()) {
                    Preconditions.checkNotNull(path);
                    cleanExistingConfiguration(path + "/conf");
                }
            }
        }
    }

    public String getApplicationToBeConfigured() {
        return applicationToBeConfigured;
    }

    private void setApplicationToBeConfigured(String sAppName) {
        applicationToBeConfigured = sAppName;
    }

    private void setConfiguration(TestConfigurationContext testContext, String sConfigurationPath, String sInstallationPath) throws IOException {
        cleanExistingConfiguration(sConfigurationPath);
        copyConfiguration(testContext, sConfigurationPath);
        renameInsideConfiguration(testContext, sConfigurationPath, sInstallationPath);
        restructureConfigurationStructureSpecific(sConfigurationPath, sInstallationPath);
    }

    protected void cleanExistingConfiguration(String sConfigurationPath) {
        File[] lstVmFiles = new File(sConfigurationPath).listFiles();
        if (null == lstVmFiles) {
            return;
        }
        for (File fFile : lstVmFiles) {
            KrantiFileUtils.remove(fFile);
        }
    }

    protected void restructureConfigurationStructureSpecific(String sConfigurationPath, String sInstallationPath) {
    }

    private void renameInsideConfiguration(TestConfigurationContext testContext, String sConfigurationPath, String sInstallPath) throws IOException {
        TestCommandExecution.runCommand("chmod -R 775 " + sConfigurationPath);

        Map<String, String> mpConfigurationReplacementMap = getConfigurationReplacementMap(sConfigurationPath, sInstallPath);
        TestReporter.TRACE("Will replace in configuration files: " + mpConfigurationReplacementMap);
        renameInsideConfiguration(sConfigurationPath, mpConfigurationReplacementMap);
        TestReporter.TRACE("renameInsideConfiguration() - finished");
    }

    private Map<String, String> getConfigurationReplacementMap(String sConfigurationPath, String sInstallPath) {
        Map<String, String> mpConfigurationReplacementMap = new HashMap<>();
        fillConfigurationReplacementMap(mpConfigurationReplacementMap);
        mpConfigurationReplacementMap.put("/tmp/__USER__/", getSandBoxDir() + "/");
        mpConfigurationReplacementMap.put("__PROJECT_ROOT__", getProjectHead());
        mpConfigurationReplacementMap.put("__TEST_ROOT__", TestEnvironment.getTestFolderPath(this));
        mpConfigurationReplacementMap.put("__TEST_CONF__", sConfigurationPath);
        mpConfigurationReplacementMap.put("__SANDBOX__", getSandBoxDir());
        //putComponentConfigurationInReplacementMap(mpConfigurationReplacementMap);
        mpConfigurationReplacementMap.put("__DOMAIN__", LocalHost.getDomainFromLong(LocalHost.getPhysicalLong()));
        return mpConfigurationReplacementMap;
    }

    /*private void putComponentConfigurationInReplacementMap(Map<String, String> mpConfigurationReplacementMap) {
        List<ApplicationConfiguration> componentConfigurations = krantiConf.getComponentConfigurations();
        for (ApplicationConfiguration cConf : componentConfigurations) {
            if (null != cConf.getName() && null != cConf.getDefaultConfigurationPath())
                mpConfigurationReplacementMap.put(formKeyFromApplicationName(cConf.getName()), cConf.getDefaultConfigurationPath());
        }
    }
*/
    private String formKeyFromApplicationName(String sApplication) {
        return "__" + sApplication.toUpperCase() + "__CONF__";
    }


    protected void renameInsideConfiguration(String sConfigurationPath, Map<String, String> mpConfigurationReplacementMap) throws IOException {
        FileContentReplacer contentReplacer = new FileContentReplacer(true);
        for (Entry<String, String> e : mpConfigurationReplacementMap.entrySet()) {
            contentReplacer.addReplacement(e.getKey(), e.getValue());
        }
        contentReplacer.execute(new File(sConfigurationPath));
    }

    protected void copyConfiguration(TestConfigurationContext testContext, String sDestination) {
        TestReporter.TRACE("copying configuration to " + sDestination);
        KrantiConfigurationWriter confWriter = new KrantiConfigurationWriter();
        confWriter.setDestination(sDestination);
        confWriter.setSources(getConfSearchStrategy().getTestConfigurationArea(testContext));
        confWriter.copy();
    }

    protected final void copyConfiguration(List<String> listSource, String sDestination) {
        KrantiConfigurationWriter confWriter = new KrantiConfigurationWriter();
        confWriter.setDestination(sDestination);
        confWriter.setSources(listSource);
        confWriter.copy();
    }

    protected String getConfigurationPath(String sApplication) {
        if (sApplication.matches("(?i).*web")) {
            return getKrantiConfiguration().getApplicationConfiguration(sApplication).getWebConfigurationPath();
        }
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getDefaultConfigurationPath();
    }

    protected String getUpgradedConfigurationPath() {
        return getUpgradedConfigurationPath(getApplication());
    }

    protected String getConfigurationPath(String sApplication, String sComponentType, int index) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getConfigurationPath(sComponentType, index);
    }

    protected String getUpgradedConfigurationPath(String sApplication) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getDefaultUpgradedConfigurationPath();
    }


    protected String getDowngradedConfigurationPath(String sApplication) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getDefaultDowngradedConfigurationPath();
    }

    protected String getUpgradedConfigurationPath(String sApplication, String sComponentType, int index) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getUpgradedConfigurationPath(sComponentType, index);
    }

    protected String getDowngradedConfigurationPath(String sApplication, String sComponentType, int index) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getDowngradedConfigurationPath(sComponentType, index);
    }

    protected String getInstallationPath(String sApplication, String sComponentType, int index) {
        ApplicationConfiguration applicationConfiguration = getKrantiConfiguration().getApplicationConfiguration(sApplication);
        if (applicationConfiguration == null) {
            TestReporter.FATAL("There is no application " + sApplication + " configured in .kranti file");
        }
        return applicationConfiguration.getComponentInstallationPath(sComponentType, index);
    }

    protected String getUpgradedInstallationPath(String sApplication, String sComponentType, int index) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getComponentUpgradedInstallationPath(sComponentType, index);
    }

    protected String getDowngradedInstallationPath(String sApplication, String sComponentType, int index) {
        return getKrantiConfiguration().getApplicationConfiguration(sApplication).getComponentDowngradedInstallationPath(sComponentType, index);
    }

    protected String getConfigurationPath() {
        return getConfigurationPath(getApplication());
    }


    protected abstract void fillConfigurationReplacementMap(Map<String, String> mMap);

    protected Map<String, String> getComponentsMapping(String prefix, List<String> hosts) {
        int i = 0;
        Map<String, String> mp = new HashMap<String, String>();
        for (String h : hosts) {
            mp.put(prefix + i++, h);
        }
        return mp;
    }

    protected void initApplicationsStore() {
        getApplicationStore().init(getKrantiConfiguration(), getApplication());
    }

    private void closeApplicationsStore() {
        onAllComponents(subject -> {
            getApplicationStore().removeStartedComponent(subject);
            return null;
        });
        getApplicationStore().close();
    }

    /*private SnapshotManager getSnapshotManager() {
        return SnapshotManager.getInstance();
    }*/

    protected ApplicationStore getApplicationStore() {
        return ApplicationStore.getInstance();
    }

    private final void clean() {
        cleanComponents(false);
    }


    private final void forceClean() {
        cleanComponents(true);
    }


    protected void onAllComponents(final Function<TestComponent, Void> fFunction) {
        onAllComponents(fFunction, null);
    }


    protected void onAllComponents(final Function<TestComponent, Void> fFunction, final Function<TestComponent, Integer> orderFunc) {
        List<? extends TestComponent> lstValidComponents = getTestComponents().stream().filter(Objects::nonNull).collect(Collectors.toList());

        if (shouldActOnComponentsInParallel()) {
            TreeMap<Integer, List<TestComponent>> treeMap = sortTestComponentsAccordingToOrder(lstValidComponents, orderFunc);
            for (Integer order : treeMap.navigableKeySet()) {
                runParallelFlowOnComponentList(treeMap.get(order), fFunction);
            }
        } else {
            //even if not running in parallel , this provides a sort order for tearing down components
            if (null != orderFunc) {
                TreeMap<Integer, List<TestComponent>> treeMap = sortTestComponentsAccordingToOrder(lstValidComponents, orderFunc);
                for (Integer order : treeMap.navigableKeySet()) {
                    treeMap.get(order).stream().map(fFunction);
                }
            } else {
                sort(lstValidComponents).stream().map(fFunction);
            }
        }
    }

    private void runParallelFlowOnComponentList(List<? extends TestComponent> lstComponents, final Function<TestComponent, Void> fFunction) {
        List<TestFlow> testFlows = lstComponents.stream().map(subject -> new TestFlow() {
            @Override
            public void runSequence() {
                fFunction.apply(subject);
            }
        }).collect(Collectors.toList());
        runParallelFlows(testFlows.toArray(new TestFlow[0]));
    }

    private TreeMap<Integer, List<TestComponent>> sortTestComponentsAccordingToOrder(List<? extends TestComponent> lstComponents, final Function<TestComponent, Integer> orderFunc) {
        TreeMap<Integer, List<TestComponent>> treeMap = new TreeMap<Integer, List<TestComponent>>();
        for (TestComponent t : lstComponents) {
            int order = (null == orderFunc) ? 0 : orderFunc.apply(t);
            List<TestComponent> lst = treeMap.get(order);
            if (CollectionUtils.isEmpty(lst)) {
                lst = new ArrayList<>();
                treeMap.put(order, lst);
            }
            lst.add(t);
        }
        return treeMap;
    }


    protected List<? extends TestComponent> sort(List<? extends TestComponent> testComponents) {
        return testComponents;
    }


    @Override
    public boolean isRunning() {
        return running;
    }

    private void setRunning(boolean running) {
        this.running = running;
    }

    protected abstract void initTestCaseSpecific() throws Exception;

    protected TestConfigurationSearchStrategy getConfSearchStrategy() {
        return confSearchStrategy;
    }

    protected void runParallelFlows(int noOfFlows, TestFlow flow) {
        TestFlow[] flows = new TestFlow[noOfFlows];
        for (int i = 0; i < noOfFlows; i++) {
            flows[i] = flow;
        }
        runParallelFlows(flows);
    }

    protected void runParallelFlows(TestFlow... flows) {
        if (null == flows) {
            return;
        }
        long lMaxFlowTime = TimeUnitsTranslator.fromMinutes(10);

        runParallelFlows(lMaxFlowTime, flows);
    }

    protected void runParallelFlows(long lMaxFlowTime, TestFlow... flows) {
        if (null == flows || flows.length == 0) {
            return;
        }
        Thread[] tFlows = new Thread[flows.length];
        for (int i = 0; i < flows.length; i++) {
            tFlows[i] = new Thread(flows[i], "flow" + i);
            tFlows[i].start();
        }
        long lStartTime = System.currentTimeMillis();
        for (int i = 0; i < flows.length; i++) {
            try {
                long lTimeout = (lMaxFlowTime + lStartTime) - System.currentTimeMillis();
                if (lTimeout <= 0) {
                    lTimeout = 1;
                }
                tFlows[i].join(lTimeout);
            } catch (InterruptedException e) {
                TestReporter.FATAL("Couldn't wait till the end of flow " + i);
            }
        }
        for (TestFlow flow : flows) {
            if (null != flow.getFatal()) {
                TestReporter.FATAL(flow.getFatal());
            }
        }
    }

    protected void _sleep(long lSeconds) {
        TestEnvironment.sleep(lSeconds);
    }

    private void initTestEnvironment() {
        TestEnvironment.init(this, getSandBoxDir(), tmpFolder, getWindowsSandBoxDir());
        TestEnvironment.setPerformance(containsPerformanceAnnotation());
        TestEnvironment.setIntegration(containsIntegrationAnnotation());
        TestEnvironment.setWebserverTest(containsRestApiAnnotation());
        TestEnvironment.setShoudKeepLog(containsKrantiDontCleanLogOnPassAnnotation());
        TestEnvironment.setHaltOnError(krantiConf.getHaltOnError());
        TestEnvironment.setKrantiConf(krantiConf);
        TestEnvironment.setCurrentApplication(getCurrentApplication());
    }

    private boolean containsPerformanceAnnotation() {
        if (this.getClass().getPackage().isAnnotationPresent(PerformanceTest.class)
                || this.getClass().isAnnotationPresent(PerformanceTest.class)) {
            return true;
        }
        return false;
    }

    private boolean containsIntegrationAnnotation() {
        if (this.getClass().getPackage().isAnnotationPresent(IntegrationTest.class)
                || this.getClass().isAnnotationPresent(IntegrationTest.class)) {
            return true;
        }
        return false;
    }


    private boolean containsRestApiAnnotation() {
        if (this.getClass().getPackage().isAnnotationPresent(KrantiRestAPITest.class)
                || this.getClass().isAnnotationPresent(KrantiRestAPITest.class)) {
            return true;
        }
        return false;
    }

    private boolean containsKrantiDontCleanLogOnPassAnnotation() {
        if (this.getClass().getPackage().isAnnotationPresent(KrantiDontCleanLogOnPass.class)
                || this.getClass().isAnnotationPresent(KrantiDontCleanLogOnPass.class)
                || this.getClass().isAnnotationPresent(KrantiDontCleanLogOnPass.class)) {
            return true;
        }
        return false;
    }

    public TemporaryFolder getTemporaryFolder() {
        return TestEnvironment.getTemporaryFolder();
    }

    private void checkExceptions() {
        getExceptionChecker().checkExceptions(getSandBoxDir());
        if (isCheckForSuspiciousPatternsInLog()) {
            getExceptionChecker().checkSuspiciousPatterns(getSandBoxDir(), krantiConf);
        }
    }

    protected boolean isCheckForSuspiciousPatternsInLog() {
        return false;
    }

    protected ExceptionsChecker getExceptionChecker() {
        return exceptionChecker;
    }

    protected void measureTime(TestFlow fFlow, long lMaxTime) {
        long lExecutionTime = runFlowAndMeasureTime(fFlow);
        TestReporter.TRACE("Flow took " + lExecutionTime + " milli");
        Check.assertBetween(0, lMaxTime + 10, lExecutionTime);
    }

    protected void measureTime(OpResult rResult, long lMaxTime) {
        TestReporter.TRACE("Command took " + rResult.getExecutionTime() + " milli");
        Check.assertBetween(0, lMaxTime + 10, rResult.getExecutionTime() / 1000L);
    }

    protected void runFlowAtLeast(long lMinTimeSeconds, TestFlow fFlow) {
        long lExecutionTime = runFlowAndMeasureTime(fFlow);
        _sleep(lMinTimeSeconds - lExecutionTime);
    }

    protected void runFlowAtMost(long lMinTimeSeconds, TestFlow fFlow) {
        long lExecutionTime = runFlowAndMeasureTime(fFlow);
        if (lExecutionTime > lMinTimeSeconds) {
            TestReporter.FAIL("Flow took " + lExecutionTime + " seconds, more than expected " + lMinTimeSeconds);
        }
    }

    protected long runFlowAndMeasureTime(TestFlow fFlow) {
        return runFlowAndMeasureTimeinMillis(fFlow) / 1000L;
    }

    protected long runFlowAndMeasureTimeinMillis(TestFlow fFlow) {
        long lStart = System.currentTimeMillis();
        fFlow.runSequence();
        return (System.currentTimeMillis() - lStart);
    }

    protected long runFlowAndMeasureTimeinMilli(TestFlow fFlow) {
        long lStart = System.currentTimeMillis();
        fFlow.runSequence();
        long lExecutionTime = (System.currentTimeMillis() - lStart);
        return lExecutionTime;
    }

    protected void assignMaskingComponentsSpecific() {
    }

    protected void initExceptionsChecker() {
        exceptionChecker = new ExceptionsChecker();
        setAllIgnoredMessages(exceptionChecker);
        exceptionChecker.setFullExceptionMessage(isFullExceptionMessage());
    }

    protected boolean isFullExceptionMessage() {
        return false;
    }

    protected void setAllIgnoredMessages(ExceptionsChecker exceptionChecker) {
        for (Class ignoredException : ignoredExceptionRule.getIgnoredException()) {
            exceptionChecker.removeException(ignoredException);
        }
        for (Class additionalException : additionalExceptionRule.getKrantiAdditionalException()) {
            exceptionChecker.addException(additionalException);
        }
        exceptionChecker.setIgnoredMessages(ignoredMessagesRule.getIngnoredMessages());
    }

    protected void checkSnapshot() {
        //getSnapshotManager().check();
    }

    protected String getTestDirectory() {
        //todo
        if (TestEnvironment.isGradleProject("")) {

        }
        return getProjectHead() + "/" + getApplication() + "." + getTestingMode() + "/java/tests/" + getClass().getPackage().getName().replace(".", "/");
    }

    protected void initMemoryLeakCheckList() {
    }

    public Integer getSystemTotalMemoryInKB() {
        String[] cmd = {"cat", "/proc/meminfo"};
        OpResult rResult = TestCommandExecution.runCommand(cmd);
        if (rResult.getExitStatus() == 0) {
            for (String entry : rResult.getStdOut()) {
                String nameSwitch = "MemTotal:";
                if (entry.startsWith(nameSwitch)) {
                    return Integer.parseInt(entry.substring(entry.indexOf(nameSwitch) + nameSwitch.length()).replace("kB", "").trim());
                }
            }
        }
        return 0;
    }

    public Integer getSystemTotalCPUs() {
        String[] cmd = {"lscpu"};
        OpResult rResult = TestCommandExecution.runCommand(cmd);
        if (rResult.getExitStatus() == 0) {
            for (String entry : rResult.getStdOut()) {
                String nameSwitch = "CPU(s):";
                if (entry.startsWith(nameSwitch)) {
                    return Integer.parseInt(entry.substring(entry.indexOf(nameSwitch) + nameSwitch.length()).trim());
                }
            }
        }
        return 0;
    }


}
