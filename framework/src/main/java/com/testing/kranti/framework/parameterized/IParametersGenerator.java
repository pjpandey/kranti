package com.testing.kranti.framework.parameterized;

import java.util.List;
import java.util.Map;

public interface IParametersGenerator {
    List<Map<String, String>> getParametersList();
}
