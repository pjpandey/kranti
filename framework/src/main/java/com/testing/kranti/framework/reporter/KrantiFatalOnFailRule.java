package com.testing.kranti.framework.reporter;

import com.testing.kranti.framework.rules.AbstractMethodRule;
import org.junit.runners.model.FrameworkMethod;

public class KrantiFatalOnFailRule extends AbstractMethodRule {
    @Override
    protected void extractTestProperties(FrameworkMethod mTest, Object oTest) {
        if (oTest.getClass().isAnnotationPresent(KrantiFatalOnFail.class) || mTest.getMethod().isAnnotationPresent(KrantiFatalOnFail.class)) {
            TestReporter.setFatalOnFail();
        }
    }

}

	