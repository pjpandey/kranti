package com.testing.kranti.framework.utils.time;

import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUnitsTranslator {
    transient private static Logger log = Logger.getLogger(TimeUnitsTranslator.class);

    private final static int DAYS_IN_WEEK = 7;
    private final static int DAYS_IN_MONTH = 30;
    private final static int DAYS_IN_YEAR = 365;

    public final static long MILLI_IN_SEC = 1000;
    private final static long SEC_IN_MIN = 60;
    private final static long MIN_IN_HOUR = 60;
    private final static long HOUR_IN_DAY = 24;


    private final static long MILLI_IN_MIN = MILLI_IN_SEC * SEC_IN_MIN;
    private final static long MILLI_IN_HOUR = MILLI_IN_MIN * MIN_IN_HOUR;
    private final static long MILLI_IN_DAY = MILLI_IN_HOUR * HOUR_IN_DAY;
    private final static long MILLI_IN_WEEK = MILLI_IN_DAY * DAYS_IN_WEEK;
    private final static long MILLI_IN_YEAR = MILLI_IN_DAY * DAYS_IN_YEAR;

    private final static long SEC_IN_HOUR = SEC_IN_MIN * MIN_IN_HOUR;
    private final static long SEC_IN_DAY = SEC_IN_HOUR * HOUR_IN_DAY;
    private final static long SEC_IN_WEEK = SEC_IN_DAY * DAYS_IN_WEEK;

    private final static long NANOS_IN_MILLI = 1000000;

    public static String UNLIMITED = "nolimit";

    private static final long NO_EXPIRATION = Long.MAX_VALUE;

    private static Calendar s_calendar = Calendar.getInstance();

    public static long getTimeIntervalInDaysSafe(String size) throws TimeTranslationException {
        String sSize = size;
        if (sSize == null) {
            return -1;
        }

        long factor = 1;
        long retVal;
        if (sSize.equalsIgnoreCase(UNLIMITED)) {
            return NO_EXPIRATION;
        } else if (sSize.toUpperCase().endsWith("D")) {
            sSize = sSize.substring(0, sSize.length() - 1);
            factor = 1;
        } else if (sSize.toUpperCase().endsWith("W")) {
            sSize = sSize.substring(0, sSize.length() - 1);
            factor = DAYS_IN_WEEK;
        } else if (sSize.toUpperCase().endsWith("M")) {
            sSize = sSize.substring(0, sSize.length() - 1);
            factor = DAYS_IN_MONTH;
        } else if (sSize.toUpperCase().endsWith("Y")) {
            sSize = sSize.substring(0, sSize.length() - 1);
            factor = DAYS_IN_YEAR;
        }

        try {

            retVal = factor * Long.decode(sSize).longValue();
            return retVal;
        } catch (Exception e) {
            throw new TimeTranslationException("invalid format of duration: '" + sSize + "'");
        }
    }

    public static long getTimeIntervalInDays(String sSize) {
        long timeIntervalInDaysSafe;
        try {
            timeIntervalInDaysSafe = getTimeIntervalInDaysSafe(sSize);
        } catch (Exception e) {
            return -1;
        }
        return timeIntervalInDaysSafe;
    }

    public static long getDateFromString(String sDate) {
        if (sDate.length() != 8) {
            return -1;
        }
        String sMonth = sDate.substring(0, 2);
        String sDay = sDate.substring(2, 4);
        String sYear = sDate.substring(4);

        if (log.isDebugEnabled()) {
            log.debug("getDurationInDaysFromDate() - called with " + sMonth + ", " + sDay + ", " + sYear);
        }

        Calendar clExpiration = Calendar.getInstance();
        try {
            if (Integer.valueOf(sMonth) > 12 || Integer.valueOf(sDay) > 31) {
                return -1;
            }
            clExpiration.set(Calendar.DAY_OF_MONTH, Integer.parseInt(sDay));
            clExpiration.set(Calendar.MONTH, Integer.parseInt(sMonth) - 1);
            clExpiration.set(Calendar.YEAR, Integer.parseInt(sYear));
            clExpiration.set(Calendar.HOUR_OF_DAY, 23);
        } catch (Exception e) {
            return -1;
        }
        return clExpiration.getTimeInMillis();
    }

    public static long getTimeIntervalInMilli(String sParameterName, String sPeriodicy) throws TimeTranslationException {
        return getTimeIntervalInMilli(sParameterName, sPeriodicy, MILLI_IN_DAY);
    }

    public static long getTimeIntervalInMilli(String sPeriodicy) throws TimeTranslationException {
        return getTimeIntervalInMilli("Periodicity", sPeriodicy);
    }

    public static long getTimeIntervalinMilliSimulationaware(String sPeriodic) throws TimeTranslationException {
        long retVal = getTimeIntervalInMilli("Periodicity", sPeriodic, TimeUnitsScale.TIME_SCALE_FACTOR);


        return retVal;
    }

    public static long getTimeIntervalInMilliNoException(String sParameterName, String sPeriodicy, long defaultFactor) {
        try {
            return getTimeIntervalInMilli(sParameterName, sPeriodicy, defaultFactor);
        } catch (TimeTranslationException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static long getTimeIntervalInMilli(String sParameterName, String sPeriodicy, long defaultFactor) throws TimeTranslationException {
        return getTimeIntervalInMilli(sParameterName, sPeriodicy, defaultFactor, false);
    }

    public static long getTimeIntervalInMilli(String sParameterName, String periodicy, long defaultFactor, boolean bAllowZero) throws TimeTranslationException {
        return getTimeIntervalInMilli(sParameterName, periodicy, defaultFactor, bAllowZero, false);
    }

    public static long getTimeIntervalInMilli(String sParameterName, String periodicy, long defaultFactor, boolean bAllowZero, boolean bAllowFloat) throws TimeTranslationException {
        String peridicyOrig = periodicy;
        String sPeriodicy = periodicy;
        if ((null == sPeriodicy) || (0 == sPeriodicy.length())) {
            return 0;
        }
        long factor = 1;
        long retVal;
        try {
            if (sPeriodicy.toUpperCase().endsWith("S")) {
                sPeriodicy = sPeriodicy.substring(0, sPeriodicy.length() - 1);
                factor = MILLI_IN_SEC;
            } else if (sPeriodicy.toUpperCase().endsWith("M")) {
                sPeriodicy = sPeriodicy.substring(0, sPeriodicy.length() - 1);
                factor = MILLI_IN_MIN;
            } else if (sPeriodicy.toUpperCase().endsWith("H")) {
                sPeriodicy = sPeriodicy.substring(0, sPeriodicy.length() - 1);
                factor = MILLI_IN_HOUR;
            } else if (sPeriodicy.toUpperCase().endsWith("D")) {
                sPeriodicy = sPeriodicy.substring(0, sPeriodicy.length() - 1);
                factor = MILLI_IN_DAY;
            } else if (sPeriodicy.toUpperCase().endsWith("W")) {
                sPeriodicy = sPeriodicy.substring(0, sPeriodicy.length() - 1);
                factor = MILLI_IN_WEEK;
            } else if (sPeriodicy.toUpperCase().endsWith("Y")) {
                sPeriodicy = sPeriodicy.substring(0, sPeriodicy.length() - 1);
                factor = MILLI_IN_YEAR;
            } else {
                factor = defaultFactor;
            }
            if (bAllowFloat) {
                retVal = (long) (factor * Double.parseDouble(sPeriodicy));
            } else {
                retVal = factor * Long.decode(sPeriodicy).longValue();
            }
            if (!bAllowZero && retVal <= 0) {
                throw new TimeTranslationException(sParameterName + " is " + sPeriodicy + " but should be a positive number ");
            } else if (bAllowZero && retVal < 0) {
                throw new TimeTranslationException(sParameterName + " is " + sPeriodicy + " but should be a non-negative number ");
            }
            return retVal;
        } catch (NumberFormatException e) {
            throw new TimeTranslationException(peridicyOrig + " is not a valid " + sParameterName);
        }
    }


    public static String getTimeIntervalAsString(long intervalinMilli) {
        return getTimeIntervalAsStirng(intervalinMilli, null);
    }


    public static String getTimeIntervalAsStirng(long intervalInMilli, String defaultUnit) {
        String unit = null;
        String output;

        if (intervalInMilli == 0) {
            return String.valueOf(intervalInMilli);
        }
        if (null == defaultUnit) {
            unit = (intervalInMilli >= MILLI_IN_SEC) ? "s" : "ms";
            unit = (intervalInMilli >= MILLI_IN_MIN) ? "m" : unit;
            unit = (intervalInMilli >= MILLI_IN_HOUR) ? "h" : unit;
            unit = (intervalInMilli >= MILLI_IN_DAY) ? "d" : unit;
            unit = (intervalInMilli >= MILLI_IN_WEEK) ? "w" : unit;

        } else {
            unit = defaultUnit;
        }
        switch (unit.toLowerCase()) {
            case "ms":
                output = String.valueOf(intervalInMilli);
                break;
            case "s":
                output = String.valueOf(intervalInMilli / MILLI_IN_SEC);
                break;
            case "m":
                output = String.valueOf(intervalInMilli / MILLI_IN_MIN);
                break;
            case "h":
                output = String.valueOf(intervalInMilli / MILLI_IN_HOUR);
                break;
            case "d":
                output = String.valueOf(intervalInMilli / MILLI_IN_DAY);
                break;
            case "w":
                output = String.valueOf(intervalInMilli / MILLI_IN_WEEK);
                break;
            default:
                throw new RuntimeException("Unit " + defaultUnit + "  are not supported");

        }
        return (output + unit).toLowerCase();
    }

    public static long toGMT(long time) {
        return time - s_calendar.getTimeZone().getOffset(time);
    }

    public static long fromGMT(long time) {
        return time + s_calendar.getTimeZone().getOffset(time);
    }

    public static Date toGMT(Date time) {
        if (null == time) {
            return null;
        }

        return new Date(toGMT(time.getTime()));
    }

    public static Date fromGMT(Date time) {
        if (null == time) {
            return null;
        }

        return new Date(fromGMT(time.getTime()));
    }

    public static long secondsToJiffies(long seconds, long jiffiesInSec) {
        return (seconds * jiffiesInSec); //OSDataFactory.getProvider().getHZ());
    }

    public static long millisToSeconds(long millis) {
        return millis / MILLI_IN_SEC;
    }

    public static long millisToToMin(long millis) {
        return millis / MILLI_IN_MIN;
    }

    public static long millisToDaysCeil(long lMillis, long lDayInMillis) {
        return (long) Math.ceil(lMillis / lDayInMillis);
    }

    public static long millisToSecondsRounded(long millis) {
        return (Math.round((double) millis / (double) MILLI_IN_SEC));
    }

    /**
     * @param minutes
     * @return time in milli
     */
    public static long fromMinutes(long minutes) {
        return minutes * MILLI_IN_MIN;
    }

    public static long fromSeconds(long secs) {
        return secs * MILLI_IN_SEC;
    }

    public static long fromHours(int hours) {
        return hours * MILLI_IN_HOUR;
    }

    public static long fromDays(int days) {
        return days * MILLI_IN_DAY;
    }

    public static long fromJiffies(long jiffies, long jiffiesInSec) {
        return (long) (jiffies * ((double) MILLI_IN_SEC / (double) jiffiesInSec));
    }

    public static long fromNanos(long nanos) {
        return nanos / NANOS_IN_MILLI;
    }

    public static Date toOffset(Date localDate, TimeZone timeZone) {
        return new Date(toOffset(localDate.getTime(), timeZone));
    }

    public static long toOffset(long localDate, TimeZone timeZone) {
        long gmtTime = toGMT(localDate);
        long clientOffsetFromGMT = timeZone.getOffset(gmtTime);
        return gmtTime + clientOffsetFromGMT;
    }

    public static long fromOffset(long clientTime, TimeZone timeZone) {
        long gmtTime = clientTime - timeZone.getOffset(clientTime);
        return fromGMT(gmtTime);
    }

    public static Date fromOffset(Date clientDate, TimeZone timeZone) {
        return new Date(fromOffset(clientDate.getTime(), timeZone));
    }

    public static Date getDate(long lDate) {
        if ((lDate > 0) && (lDate < Long.MAX_VALUE)) {
            return new Date(lDate);
        }
        return null;
    }

    public static long minutesToHours(long hours) {
        return hours * MIN_IN_HOUR;
    }
}
