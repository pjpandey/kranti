package com.testing.kranti.framework;

import com.testing.kranti.framework.check.Check;
import com.testing.kranti.framework.operation.OpResult;
import com.testing.kranti.framework.operation.Operation;
import com.testing.kranti.framework.operation.commmand.AbstractCommandOperation;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.utils.process.ProcessUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.NotImplementedException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public abstract class AbstractTestComponent implements TestComponent {
    public static int TIMES_TO_CHECK = 30;
    private static int NUMBER_OF_TIMES_TRY_TO_START = 5;
    private TestComponentData dData = null;
    private int iProcessID = -1;
    private boolean bSuspended = false;
    private boolean bUpgraded = false;
    private final List<ISnapshot> lstSnapshots = new ArrayList<ISnapshot>();
    protected boolean bShouldCleanPersistencyLeftoversOnTeardown = false;
    private boolean bDowngraded;
    private String sandboxDir;
    private boolean wasStarted = false;
    private boolean shouldVerifyNextStop = true;
    private String processUser = "";

    protected AbstractTestComponent(TestComponentData dData) {
        super();
        this.dData = dData;
        registerSnapshots();
    }

    protected TestComponentData getData() {
        return dData;
    }

    protected void registerSnapshots() {
    }

    protected void addSnapshot(ISnapshot snapshot) {
        lstSnapshots.add(snapshot);
    }

    @Override
    public final List<ISnapshot> getSnapShots() {
        return lstSnapshots;
    }

    @Override
    public void setSandBoxDir(String sandBoxDir) {
        sandboxDir = sandBoxDir;
    }

    @Override
    public String getSandBoxDir() {
        return sandboxDir;
    }

    @Override
    public final String getInstallationDir() {
        return getData().getInstallationDir();
    }

    public final String getCommandBinaries() {
        return TestEnvironment.getKrantiConf().getApplicationConfiguration(this.getTestedApplication()).getCommandBinaries();
    }

    @Override
    public final String getUpgradedInstallationDir() {
        return getData().getUpgradedInstallationDir();
    }

    @Override
    public final String getDowngradedInstallationDir() {
        return getData().getDowngradedInstallationDir();
    }

    @Override
    public final String getDowngradedConfigurationDir() {
        return getData().getDowngradedConfigurationDir();
    }

    @Override
    public final String getHost() {
        return getData().getHost();
    }

    public final void setHost(String host) {
        getData().setHost(host);
    }

    @Override
    public String getConfigurationDir() {
        return getData().getConfigurationDir();
    }

    @Override
    public void setConfigurationDir(String sDir) {
        getData().setConfigurationDir(sDir);
    }


    @Override
    public void clean(boolean bForce) {
        if (!bForce) {
            clean();
        } else {
            cleanForce();
        }
    }

    @Override
    public int getCleanOrder() {
        return 10;
    }

    private final void cleanForce() {
        cleanForceSpecific();
        ProcessUtils.killProcess(getHost(), getProcessID());
        deletePersistency();
    }

    protected void cleanForceSpecific() {

    }

    public void deletePersistency() {
        if (TestEnvironment.isWindowsHost(getHost())) {
            return;
        }
        String sPersistencyDir = getPersistencyDir();
        if (null != sPersistencyDir) {
            TestCommandExecution.runCommand("rm -rf " + sPersistencyDir, getHost());
        }
    }

    protected abstract String getPersistencyType();

    @Override
    public String getPersistencyDir() {
        if (null == getData().getPersistencyRoot()) {
            return null;
        }
        if (getPersistencyType() == null) {
            return new String();
        }
        return getData().getPersistencyRoot() + "/" + getPersistencyType() + "_" + getHost();
    }

    protected abstract void clean();

    @Override
    public OpResult performFailedOperation(Operation oOperation) {
        OpResult result = performOperation(oOperation);
        if (result.getExitStatus() == 0) {
            TestReporter.FAIL("Succeeded to perform " + ((AbstractCommandOperation) oOperation).getName() + "operation when should have failed. Command result: " + result);
        }
        return result;
    }

    public OpResult performSuccessOperation(AbstractCommandOperation oOperation) {
        OpResult result = performOperation(oOperation);
        if (result.getExitStatus() != 0) {
            TestReporter.FAIL("Failed to perform " + oOperation.getName() + "(" + oOperation.getInstallationDir() + ") operation when should have succeeded. Command result: " + result);
        }
        return result;
    }

    @Override
    public final OpResult performOperation(Operation oOperation) {
        oOperation.execute();
        return oOperation.getResult();
    }


    protected void performCheckedOperation(AbstractCommandOperation operation, Predicate predicate) {
        OpResult result = performOperation(operation);
        if (result.getExitStatus() != 0) {
            TestReporter.FATAL("Unable to perform " + operation.getName() + ". Command result: " + result);
        }
        Check.assertBusyWait(predicate, (Void) null, 30, 1);
    }

    protected void performCheckedOperation(AbstractCommandOperation operation, Predicate<String> predicate, Object oExpected) {
        String sExpected = (null != oExpected) ? oExpected.toString() : "";

        OpResult result = performOperation(operation);
        if (result.getExitStatus() != 0) {
            TestReporter.FATAL("Unable to perform " + operation.getName() + ". Command result: " + result);
        }
        Check.assertBusyWait(predicate, sExpected, 30, 1);
    }

    protected boolean wasStarted() {
        return wasStarted;
    }

    @Override
    public void start() {
        wasStarted = true;
        TestReporter.TRACE("Starting component " + this);
        executeStartOperation();
        TestReporter.TRACE("Component started " + this);
    }

    protected void executeStartOperation() {
        AbstractCommandOperation op = getStartOperation();
        int tried = 0;
        int pid = 0;
        do {
            if (tried > 0) {
                TestEnvironment.sleep(1L);
            }
            performOperation(op);
            processStartOperationResult(op.getResult());
            pid = getCurrentlyRunningPID(true);
            setProcessID(pid);
            tried++;
        } while (pid <= 0 && tried < NUMBER_OF_TIMES_TRY_TO_START);
        if (pid <= 0) {
            TestReporter.FATAL("Couldn't start component " + this + "\n" + op.getResult());
        }
        if (!isDisableStatusCheckOnStart()) {
            Check.assertBusyWait(new ComponentStatusPredicate(true), this, TIMES_TO_CHECK, getIntervalSecondsForStatusCheckOnStartup());
        }
    }

    protected int getIntervalSecondsForStatusCheckOnStartup() {
        return 1;
    }

    @Override
    public OpResult startWithoutExceptionReturnsResult() {
        wasStarted = true;
        TestReporter.TRACE("Starting component " + this);
        OpResult icr = executeStartOperationWithoutExceptionReturnsResult();
        TestReporter.TRACE("Component started " + this);
        return icr;
    }

    protected OpResult executeStartOperationWithoutExceptionReturnsResult() {
        AbstractCommandOperation op = getStartOperation();
        int tried = 0;
        int pid = 0;
        do {
            if (tried > 0) {
                TestEnvironment.sleep(1L);
            }
            performOperation(op);
            processStartOperationResult(op.getResult());
            pid = getCurrentlyRunningPID(true);
            setProcessID(pid);
            tried++;
        } while (pid <= 0 && tried < NUMBER_OF_TIMES_TRY_TO_START);
        return op.getResult();
    }

    protected boolean isDisableStatusCheckOnStart() {
        return false;
    }

    protected void processStartOperationResult(OpResult result) {

    }

    @Override
    public void setShouldVerifyNextStop(boolean shouldVerify) {
        shouldVerifyNextStop = shouldVerify;
    }

    @Override
    public void kill() {
        ProcessUtils.killProcess(getHost(), getProcessID());
        verifyProcessStopped();
    }

    @Override
    public void stop() {
        performStopOperation();
        if (shouldVerifyNextStop) {
            verifyProcessStopped();
        }
        shouldVerifyNextStop = true;
    }

    public void verifyProcessStopped() {
        Check.assertBusyWait(new ComponentStatusPredicate(false), this, 30, 3);

        int iPID = getCurrentlyRunningPID(true);
        TestReporter.TRACE("iPID = " + iPID);
        if (iPID > 0) {
            Check.assertBusyWait(
                    (Predicate<TestComponent>) subject -> !ProcessUtils.processRunning(getHost(), getProcessID()),
                    this, 30, 1);
        }

        setProcessID(-1);
        TestReporter.TRACE("Component stopped " + this);
    }

    protected void performStopOperation() {
        TestReporter.TRACE("Stopping component " + this);

        AbstractCommandOperation op = getStopOperation();
        performOperation(op);
    }

    @Override
    public final void restart(TestFlow... arrFlowsBeforeStart) {
        stop();
        if (null != arrFlowsBeforeStart) {
            for (TestFlow fFlow : arrFlowsBeforeStart) {
                fFlow.run();
            }
        }
        if (!isUpgraded() && needsUpgrade()) {
            upgradeWithNoRestart();
        } else if (!isDowngraded() && needsDowngrade()) {
            downgradeWithNoRestart();
        }
        start();
    }

    private boolean needsUpgrade() {
        return TestEnvironment.isComponentsUpgradeRequired();
    }

    private boolean needsDowngrade() {
        return TestEnvironment.isComponentsDowngradeRequired();
    }

    public void upgradeWithNoRestart() {
        if (getData().getUpgradedInstallationDir() == null) {
            TestReporter.FATAL("Upgraded installation path not found, check your .kranti file");
        }
        if (isNewInstallationDirTheSame(getData().getUpgradedInstallationDir())) {
            TestReporter.FATAL("Downgraded installtion path and current installation path are the same");
        }
        //TODO ?? need to upgrade config  ??
        //upgradeConfig();
        getData().setInstallationDir(getData().getUpgradedInstallationDir());
        getData().setConfigurationDir(getData().getUpgradedConfigurationDir());
        setUpgraded(true);
    }

    @Override
    public void upgrade(TestFlow... arrFlowsBeforeStart) {
        stop();
        if (null != arrFlowsBeforeStart) {
            for (TestFlow fFlow : arrFlowsBeforeStart) {
                fFlow.run();
            }
        }
        upgradeWithNoRestart();
        start();
    }

    public void downgradeWithNoRestart(String componentType) {
        if (getData().getDowngradedInstallationDir() == null) {
            TestReporter.FATAL("Downgraded installtion path not found, check your .kranti file");
        }
        if (isNewInstallationDirTheSame(getData().getDowngradedInstallationDir())) {
            TestReporter.FATAL("Downgraded installtion path and current installation path are the same");
        }
        //TODO ?? need to downgrade config  ??
        //downgradeConfig();
        getData().setInstallationDir(getData().getDowngradedInstallationDir());
        getData().setConfigurationDir(getData().getDowngradedConfigurationDir());
    }

    protected boolean isNewInstallationDirTheSame(String newInstallationDir) {
        return getData().getInstallationDir().equalsIgnoreCase(newInstallationDir);
    }

    public void downgradeWithNoRestart() {
        downgradeWithNoRestart(getComponentType());
    }

    @Override
    public void downgrade(TestFlow... arrFlowsBeforeStart) {
        stop();
        if (null != arrFlowsBeforeStart) {
            for (TestFlow fFlow : arrFlowsBeforeStart) {
                fFlow.run();
            }
        }
        downgradeWithNoRestart(getComponentType());
        start();
    }

    @Override
    public OpResult hup() {
        AbstractCommandOperation startOp = getHupOperation();
        if (null == startOp) {
            return null;
        }
        OpResult res = performOperation(startOp);
        TestEnvironment.sleep(getSleepAfterHup());
        return res;
    }

    protected long getSleepAfterHup() {
        return 0;
    }

    protected boolean resolveStatusOperation(AbstractCommandOperation statusOp, OpResult result) {
        return result.getExitStatus() == 0;
    }

    @Override
    public boolean isRunning() {
        AbstractCommandOperation healthOp = getHealthOperation();
        OpResult performOperation = performOperation(healthOp);
        if (!resolveStatusOperation(healthOp, performOperation)) {
            return false;
        }

        int iPID = getCurrentlyRunningPID(true);
        if (iPID <= 0) {
            return false;
        }
        setProcessID(iPID);
        String user = null;
        try {
            user = ProcessUtils.getProcessField(getHost(), iPID, "user");
        } catch (Exception e) {
        }
        setProcessUser(user);
        return true;
    }

    protected abstract AbstractCommandOperation getHealthOperation();

    protected void setProcessUser(String user) {
        this.processUser = user;
    }

    protected String getProcessUser() {
        return processUser;
    }


    @Override
    public boolean shouldCleanPersistencyLeftoversOnTeardown() {
        return bShouldCleanPersistencyLeftoversOnTeardown;
    }

    @Override
    public void setShouldCleanPersistencyLeftoversOnTeardown(boolean shouldCleanPersistencyLeftoversOnTeardown) {
        this.bShouldCleanPersistencyLeftoversOnTeardown = shouldCleanPersistencyLeftoversOnTeardown;
    }

    protected int getCurrentlyRunningPID(boolean bOnlyThisInstallation) {
        List<Integer> colPIDs = new ArrayList<Integer>();
        String sInstallationDir = null;
        String sInstallationDirNonCanonical = null;
        if (bOnlyThisInstallation) {
            sInstallationDir = getInstallationDir();
            sInstallationDirNonCanonical = getInstallationDir();
            try {
                sInstallationDir = new File(sInstallationDir).getCanonicalPath();
                sInstallationDirNonCanonical = new File(sInstallationDirNonCanonical).getAbsolutePath();
            } catch (Exception e) {
            }
        }

        for (String sMatch : getComponentRepresentationInPS()) {
            colPIDs = getPids(sInstallationDir, sMatch);
            if (CollectionUtils.isNotEmpty(colPIDs)) {
                break;
            }
            colPIDs = getPids(sInstallationDirNonCanonical, sMatch);
            if (CollectionUtils.isNotEmpty(colPIDs)) {
                break;
            }
        }
        if (CollectionUtils.isEmpty(colPIDs)) {
            return 0;
        }
        return colPIDs.get(0);
    }

    private List<Integer> getPids(String sInstallationDir, String sMatch) {
        return ProcessUtils.getProcessID(getHost(), getComponentExecutableName(), sMatch, sInstallationDir);
    }

    public String getComponentExecutableName() {
        return "java";
    }


    protected String[] getComponentRepresentationInPS() {
        String[] arr = {""};
        if (getPersistencyType() == null) {
            return arr;
        }
        return arr;
    }

    protected abstract AbstractCommandOperation getStartOperation();

    protected abstract AbstractCommandOperation getStopOperation();

    protected abstract AbstractCommandOperation getHupOperation();

    @Override
    public int getProcessID() {
        return iProcessID;
    }

    protected void setProcessID(int processID) {
        iProcessID = processID;
    }

    @Override
    public String getLogDir() {
        return getData().getLogDir();
    }


    protected int getPort() {
        return -1;
    }

    @Override
    public void initRunningComponent() {

    }

    @Override
    public void prepareForExecution() {
    }

    @Override
    public void prepareForStart() {
    }

    @Override
    public String getComponentType() {
        return getClass().getSimpleName();
    }

    private void setSuspended(boolean suspended) {
        bSuspended = suspended;
    }

    @Override
    public boolean isSuspended() {
        return bSuspended;
    }

    @Override
    public void suspendComponent() {
        // This function is used to suspend the java process of the TestComponent
        CommandResult cSus = TestCommandExecution.runCommand("/bin/kill -STOP " + getProcessID(), getHost());
        if (cSus.getExitStatus() == 0) {
            setSuspended(true);
        }
    }

    @Override
    public void resumeComponent() {
        // This function is used to resume the java process of the TestComponent
        CommandResult cSus = TestCommandExecution.runCommand("/bin/kill -CONT " + getProcessID(), getHost());
        if (cSus.getExitStatus() == 0) {
            setSuspended(false);
        }
    }

    public CommandResult performInteractiveOperation(AbstractCommandOperation oOperation) {
        return oOperation.executeInteractive();

    }

    protected void beforeCommandExecution() {
    }

    protected void afterCommandExecution() {
    }

    protected CommandResult runCommand(String... commandString) {
        if (commandString.length == 1) {
            String sCmd = commandString[0];
            commandString = sCmd.split(" ");
        }
        try {
            beforeCommandExecution();
            return TestCommandExecution.runCommand(commandString);
        } finally {
            afterCommandExecution();
        }
    }

    @Override
    public String getLogConfigurationFile() {
        throw new NotImplementedException("");
    }

    private void setUpgraded(boolean upgraded) {
        bUpgraded = upgraded;
    }

    private boolean isUpgraded() {
        return bUpgraded;
    }

    private void setDowngraded(boolean downgraded) {
        bDowngraded = downgraded;
    }

    private boolean isDowngraded() {
        return bDowngraded;
    }


    protected void notifySnapshot(Object event) {
        //SnapshotManager.getInstance().onEvent(event);
    }

    @Override
    public void registerMemoryLeakCheckers() {

    }

    @Override
    public void captureSnapshotOnError(String sTargetDir) {
        //ToDo - To capture statuses use CaptureSnapshotOnErrorHelper

    }

    @Override
    public Collection<String> getIngoredReferencesForLeakCheck() {
        return Collections.emptyList();
    }

    protected abstract String getTestedApplication();

    @Override
    public void prepareSandBoxDir() {
    }

    @Override
    public boolean disableLeakCheck() {
        return false;
    }
}
