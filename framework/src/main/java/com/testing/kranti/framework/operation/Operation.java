package com.testing.kranti.framework.operation;

public interface Operation {
    void execute();

    OpResult getResult();

    boolean shouldRunInBackground();

    String getName();
}
