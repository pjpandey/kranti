package com.testing.kranti.framework.configuration.kranti.writer;

import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.utils.File.FileUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import java.util.List;

public class KrantiConfigurationWriter {

    private static Logger log = Logger.getLogger(KrantiConfigurationWriter.class);
    private String destination = null;
    private List<String> lstSources;

    public boolean copy() {
        if (getDestination() == null || CollectionUtils.isEmpty(getSources())) {
            return false;
        }
        for (String sourceDir : getSources()) {
            TestReporter.TRACE("Copying configuration from " + sourceDir + " to " + getDestination());
            copyDir(sourceDir, getDestination());
        }
        return true;
    }

    public void copyDir(String source, String destination) {
        copyDir(source, destination, "-rlptcgov --exclude .git");
    }

    public CommandResult copyDir(String source, String destination, String sArgs) {
        CommandResult cr = TestCommandExecution.runCommand("/usr/bin/rsync " + sArgs + " " + source + " " + destination);
        if (cr.getExitStatus() >= 128) {
            TestReporter.FATAL("rsync command caught signal:" + cr.getExitStatus() + "stderr:" + cr.getStdErr() + "stdout:" + cr.getStdOut());
        }
        return cr;
    }


    public void setDestination(String destination) {
        this.destination = FileUtil.resolveLinks(destination);
    }

    public void setDestinationWithoutCheck(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }


    public void setSources(List<String> sources) {
        lstSources = sources;
    }


    public List<String> getSources() {
        return lstSources;
    }

}
