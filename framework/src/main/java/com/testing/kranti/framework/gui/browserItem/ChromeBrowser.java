package com.testing.kranti.framework.gui.browserItem;

import com.google.common.base.Optional;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.gui.abstrct.Browser;
import com.testing.kranti.framework.gui.abstrct.WebBrowserType;
import com.testing.kranti.framework.gui.actions.ChromeSeleniumActions;
import com.testing.kranti.framework.gui.timeout.CustomTimeouts;
import com.testing.kranti.framework.operation.commmand.AbstractCommandOperation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;

public class ChromeBrowser extends Browser {


    public ChromeBrowser(TestComponentData data, String baseTestUrl, CustomTimeouts timeouts, Optional<String> driverPath, Optional<String> browserBinaryPath, Optional<String> browserVersion, String seleniumHubURL) {
        super(data, baseTestUrl, timeouts, driverPath, browserBinaryPath, browserVersion, seleniumHubURL);
    }


    @Override
    public WebBrowserType getBrowserType() {
        return WebBrowserType.CHROME;
    }

    @Override
    public DesiredCapabilities getDesiredCapabilities() {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        desiredCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

        // If the browser binary path is present and not empty, then set this as the Chrome Binary file
        Optional<String> browserBinaryPath = getBrowserBinaryPath();
        if (browserBinaryPath.isPresent() && !browserBinaryPath.get().isEmpty()) {
            desiredCapabilities.setCapability("chrome.binary", browserBinaryPath.get());
        }

        // If the version is present and not empty, then set it as the desired version
        Optional<String> version = getBrowserVersion();
        if (version.isPresent() && !version.get().isEmpty()) {
            desiredCapabilities.setCapability(CapabilityType.VERSION, version.get());
        }

        return desiredCapabilities;
    }

    @Override
    public ChromeSeleniumActions getActions() {
        return new ChromeSeleniumActions(this);
    }

    @Override
    protected WebDriver startWebDriver() {
        Optional<String> driverPath = getWebDriverPath();
        if (!driverPath.isPresent()) {
            return RemoteWebDriverUtils.startRemoteWebDriver(getSeleniumHubUrl(), getDesiredCapabilities());
        } else {
            ChromeDriverService.Builder builder = new ChromeDriverService.Builder().usingAnyFreePort();
            File chromedriverFile = new File(driverPath.get());
            builder.usingDriverExecutable(chromedriverFile);
            ChromeDriverService service = builder.build();
            try {
                service.start();
            } catch (IOException e) {
            }
            return new ChromeDriver(service, getDesiredCapabilities());
        }
    }


    @Override
    protected AbstractCommandOperation getHealthOperation() {
        return new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "chromeHealthOp";
            }
        };
    }

    @Override
    protected String getTestedApplication() {
        return "chrome";
    }
}
