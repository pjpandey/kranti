package com.testing.kranti.framework.configuration.kranti;

import com.testing.kranti.framework.TestCase;
import com.testing.kranti.framework.TestEnvironment;
import com.testing.kranti.framework.reporter.TestReporter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TestConfigurationSearchStrategy {
    private static final String DEFAULT_CONFIGURATION_DIR = "default";
    private static final String LINUX_CONFIGURATION_SUFFIX = ".configuration";
    private static final String WINDOWS_CONFIGURATION_SUFFIX = ".winconf";

    private String sProjectHead;
    private String sApplication;
    private String m_sRootSuffix = "";
    private final String mode;
    private String m_localWorkspaceMachine;

    public TestConfigurationSearchStrategy(String projectHead, String application, String mode) {
        sProjectHead = projectHead;
        sApplication = application;
        this.mode = mode;
    }

    public List<String> getTestConfigurationArea(TestConfigurationContext context) {
        return getTestConfigurationArea(context, m_sRootSuffix + LINUX_CONFIGURATION_SUFFIX);
    }

    public List<String> getTestWindowsConfigurationArea(TestConfigurationContext context) {
        return getTestConfigurationArea(context, m_sRootSuffix + WINDOWS_CONFIGURATION_SUFFIX);
    }

    private List<String> getTestConfigurationArea(TestConfigurationContext context, String confSuffix) {
        TestReporter.TRACE("getTestConfigurationArea - in for suffix " + confSuffix + " and context " + context);
        Class<? extends TestCase> c = context.getTestClass();
        List<String> candidates = new ArrayList<String>();
        addCandidate(candidates, c.getPackage().getName().replaceAll("\\.", "/") + "/" + c.getSimpleName() + "." + context.getTestName() + confSuffix);
        for (String candidate : generateClassHierarchyCandidates(context, confSuffix)) {
            addCandidate(candidates, candidate);
        }
        addCandidate(candidates, DEFAULT_CONFIGURATION_DIR + confSuffix);
        List<String> fullPathCandidates = generateFullPathValidatedCandidates(candidates, context, confSuffix);

        if (fullPathCandidates == null || fullPathCandidates.isEmpty()) {
            TestReporter.FATAL("Can't find configuration dir for test. candidates: " + candidates);
            return null;
        }

        Collections.reverse(fullPathCandidates);
        if (m_localWorkspaceMachine != null) {
            fullPathCandidates = fullPathCandidates.stream().map(subject -> m_localWorkspaceMachine + ":" + subject).collect(Collectors.toList());
        }
        TestReporter.TRACE("getTestConfigurationArea - returning " + fullPathCandidates);
        return fullPathCandidates;
    }

    private List<String> generateFullPathValidatedCandidates(List<String> candidates, TestConfigurationContext context, String confSuffix) {
        TestReporter.TRACE("generateFullPathValidatedCandidates - in for " + context + " " + candidates);
        String prefix;
        prefix = getProjectHead() + "/" + getApplication() + "." + getDirectorySuffix() + "/"/* + "/java/tests/"*/;
        TestReporter.TRACE("String prefix for the Conf Strategy ::" + prefix + "########");
        if (TestEnvironment.isGradleProject(prefix)) {
            List<String> gradleCandidates = new LinkedList<>();
            for (String sCandidate : candidates) {
                gradleCandidates.add(sCandidate);
                gradleCandidates.add("src/main/java/" + sCandidate);
            }
            candidates.clear();
            candidates.addAll(gradleCandidates);
        }
        List<String> candidatesFullPath = new ArrayList<>();
        for (String sCandidate : candidates) {

            String e = prefix + sCandidate + "/" + context.getApplication() + "/";
            candidatesFullPath.add(e);
            if (StringUtils.equals(context.getApplication(), TestEnvironment.getCurrentApplication())) {
                candidatesFullPath.addAll(getSpecificCandidatesForDefaultApplication(prefix + sCandidate));
            }
        }
        candidatesFullPath.add(getTestDefaultConfigurationArea(context.getApplication(), confSuffix) + "/");
        candidatesFullPath.add(getTestDefaultConfigurationAreaInCommonTesting(context.getApplication(), confSuffix) + "/");
        candidatesFullPath.add(getTestOldDefaultConfigurationArea(context.getApplication(), confSuffix) + "/");
        Predicate<String> isDirectory = subject -> {
            if (new File(subject).exists()) {
                return true;
            } else {
                return false;
            }
        };

        List<String> filtered = candidatesFullPath.stream().filter(isDirectory).collect(Collectors.toList());
        TestReporter.TRACE("generateFullPathValidatedCandidates - returning " + filtered);
        return filtered;
    }

    private String getDirectorySuffix() {
        return getTestingMode();
    }

    private String getTestingMode() {
        return mode;
    }

    private List<String> getSpecificCandidatesForDefaultApplication(final String sPath) {
        String[] arrFiles = new File(sPath).list();
        if (arrFiles == null) {
            return Collections.emptyList();
        }
        //List<String> filtered = Arrays.stream(arrFiles).filter(sFile -> (null == ETestedApplication.lookup(sFile))).collect(Collectors.toList());
        return Arrays.stream(arrFiles).map(sFile -> sPath + "/" + sFile).collect(Collectors.toList());
    }

    private String getTestDefaultConfigurationArea(String sApplication, String confSuffix) {
        String dir = getProjectHead() + "/initial" + confSuffix + "/" + sApplication;
        TestReporter.TRACE("adding initial configuration: " + dir);
        return dir;
    }

    private String getTestDefaultConfigurationAreaInCommonTesting(String sApplication, String confSuffix) {
        String dir = getProjectHead() + "/common.testing/" + "/initial" + confSuffix + "/" + sApplication;
        TestReporter.TRACE("adding common.testing initial configuration: " + dir);
        return dir;
    }

    private String getTestOldDefaultConfigurationArea(String sApplication, String confSuffix) {
        String dir = getProjectHead() + "/" + TestEnvironment.getCurrentApplication() + ".testing/initial" + confSuffix + "/" + sApplication;
        TestReporter.TRACE("adding initial old configuration: " + dir);
        return dir;
    }

    private Collection<? extends String> generateClassHierarchyCandidates(TestConfigurationContext context, String confSuffix) {
        TestReporter.TRACE("generateClassHierarchyCandidates - in for " + context);
        List<String> candidates = new ArrayList<String>();
        Class<? extends TestCase> testClass = context.getTestClass();
        for (Class<?> c = testClass; c != Object.class; c = c.getSuperclass()) {
            String candidate = c.getPackage().getName().replaceAll("\\.", "/") + "/" + c.getSimpleName() + confSuffix;
            candidates.add(candidate);
            candidate = c.getPackage().getName().replaceAll("\\.", "/") + "/" + DEFAULT_CONFIGURATION_DIR + confSuffix;
            candidates.add(candidate);
        }
        TestReporter.TRACE("generateClassHierarchyCandidates - returning " + candidates);
        return candidates;
    }

    private String getApplication() {
        return sApplication;
    }

    private String getProjectHead() {
        return sProjectHead;
    }

    private List<String> addCandidate(List<String> candidates, String candidate) {
        while (candidates.remove(candidate)) {/**/}
        candidates.add(candidate);
        return candidates;
    }

    public void setLocalWorkspaceMachine(String localWorkspaceMachine) {
        m_localWorkspaceMachine = localWorkspaceMachine;
    }

    public void setRootSuffix(String rootSuffix) {
        m_sRootSuffix = rootSuffix;
    }
}
