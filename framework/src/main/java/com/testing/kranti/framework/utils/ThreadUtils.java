package com.testing.kranti.framework.utils;

import org.apache.log4j.Logger;

public class ThreadUtils {
    private static Logger log = Logger.getLogger(ThreadUtils.class);

    public static void sleep(long millis) {
        if (millis <= 0)
            return;
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            log.info("sleep() - interrupted");
        }
    }
}
