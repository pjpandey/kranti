package com.testing.kranti.framework.rules.config;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Inherited
public @interface KrantiSpecificConfiguration
{
	String[] value();
}
