package com.testing.kranti.framework.utils.File;

import com.testing.kranti.framework.operation.OpResult;

import java.util.List;

public interface IFileUtils
{
	void createDirectory(String sFullpath, String sHost);

	void deleteDirectory(String sFile, String sHost);

	Boolean fileExists(String sHost, String sPath);

	List<String> getStringMatches(String path, String pattern, String sHost);

	void deleteFile(String sFile, String sHost);

	String getJobRecoveryFilePath(String fullID);

	OpResult getFileContent(String sPath, String sHost);
	
}