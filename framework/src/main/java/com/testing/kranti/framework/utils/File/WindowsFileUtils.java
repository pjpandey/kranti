package com.testing.kranti.framework.utils.File;

import com.google.common.collect.Lists;
import com.testing.kranti.framework.operation.commmand.TestCommandExecution;
import com.testing.kranti.framework.operation.commmand.CommandResult;
import com.testing.kranti.framework.operation.OpResult;

import java.util.ArrayList;
import java.util.List;


public class WindowsFileUtils implements IFileUtils {

    public static String WINDOWS_POWERSHELL = "c:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe";
    public static String WINDOWS_CMD = "C:\\Windows\\System32\\cmd.exe";
    public static String COMMAND = "-command";

    public static OpResult getFileContents(String sFile, String sHost) {
        // Get-Content C:\test.txt
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "Get-Content", sFile};
        OpResult runCommand = TestCommandExecution.runCommand(sCommand, sHost);
        return runCommand;
    }

    @Override
    public OpResult getFileContent(String sPath, String sHost) {
        // Get-Content C:\test.txt
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "Get-Content", sPath};
        OpResult runCommand = TestCommandExecution.runCommand(sCommand, sHost);
        return runCommand;
    }

    public static boolean copy(String winHost, String src, String dst, boolean recursive) {
//		if (!checkFileExists(WINDOWS_PSCP, winHost))
//		{
//			TestReporter.TRACE("pscp command " + WINDOWS_PSCP + " wasn't found on windows host " + winHost);
//			return false;
//		}
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "Copy-Item", src, dst, recursive ? "-recurse" : ""};
//		String sCommand[] = {WINDOWS_PSCP, "-scp", "-batch", recursive ? "-r" : "", "-l", "nbdist", "-pw", "oshai6^7" , src, LocalHost.getPhysicalLong() + ":" + dst };
        OpResult runCommand = TestCommandExecution.runCommand(sCommand, winHost);
        return runCommand.getExitStatus() == 0;

    }

    @Override
    public List<String> getStringMatches(String file, String pattern, String sHost) {
        //Select-String C:\test.txt -pattern "abc 1 2 3 4 5"
        ArrayList<String> cmd = Lists.newArrayList(WINDOWS_CMD, "/c", WINDOWS_POWERSHELL, COMMAND);
        String line = "(Select-String <FILENAME> -pattern '<PATTERN>')";
        line = line.replace("<FILENAME>", file);
        line = line.replace("<PATTERN>", pattern);
        cmd.add(line);
        String[] sCommand = cmd.toArray(new String[cmd.size()]);
        CommandResult result = TestCommandExecution.runCommand(sCommand, sHost);
        return result.getStdOut();
    }

    private static void runNewItemCmdlet(String sPath, String sType, String sHost) {
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "New-Item " + sPath + " -type " + sType};
        TestCommandExecution.runCommand(sCommand, sHost);
    }

    public static void createFile(String sFile, String sHost) {
        runNewItemCmdlet(sFile, "file", sHost);
    }

    @Override
    public void createDirectory(String sFullPath, String sHost) {
        runNewItemCmdlet(sFullPath, "directory", sHost);
    }

    public static void createLink(String sLinkPath, String sDestination, String sHost) {
        String sCommand[] = {WINDOWS_CMD, "/c", "mklink", "/D", sLinkPath, sDestination};
        TestCommandExecution.runCommand(sCommand, sHost);
    }

    public static void removeFile(String sFile, String sHost, boolean force) {
        removeFile(sFile, sHost, force, true);
    }

    public static void removeFile(String sFile, String sHost, boolean force, boolean recurse) {
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "Remove-Item", sFile, recurse ? "-recurse" : "", force ? "-force" : ""};
        TestCommandExecution.runCommand(sCommand, sHost);
    }

    public static void setFileContents(String sFile, String contents, String sHost) {
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "New-Item", sFile, "-type", "file", "--force", "--value", contents};
        TestCommandExecution.runCommand(sCommand, sHost);
    }

    public static void clearFileContents(String sFile, String sHost) {
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "Clear-Content", sFile};
        TestCommandExecution.runCommand(sCommand, sHost);
    }

    public static void addFileContents(String sFile, String contents, String sHost) {
        ArrayList<String> cmd = Lists.newArrayList(WINDOWS_POWERSHELL, COMMAND);
        String line = "(Add-Content -Path <FILENAME> -force -value '<CONTENT>')";
        line = line.replace("<FILENAME>", sFile);
        line = line.replace("<CONTENT>", contents);
        cmd.add(line);
        String[] sCommand = cmd.toArray(new String[cmd.size()]);
        TestCommandExecution.runCommand(sCommand, sHost);
    }

    @Override
    public Boolean fileExists(String sHost, String sFile) {
        String sCommand[] = {WINDOWS_POWERSHELL, COMMAND, "Test-Path", sFile};
        OpResult cResult = TestCommandExecution.runCommand(sCommand, sHost);
        if (cResult.getStdOut().get(0).matches("True")) {
            return true;
        }
        return false;
    }

    @Override
    public void deleteDirectory(String sFile, String sHost) {
        removeFile(sFile, sHost, true);
    }

    @Override
    public void deleteFile(String sFile, String sHost) {
        removeFile(sFile, sHost, true, false);
    }

    @Override
    public String getJobRecoveryFilePath(String fullID) {
        return "C:\\Temp\\NetStar.JOB-" + fullID;
    }
}