package com.dummy.kranti.testing.test;

import com.testing.kranti.framework.reporter.TestReporter;
import com.testing.kranti.framework.rules.dontstop.KrantiDontCleanLogOnPass;
import org.junit.Test;

@KrantiDontCleanLogOnPass
public class DummyLifeTestCase extends DummyTestCase  {

    @Test
    public void  test() {
        TestReporter.TRACE("java.lang.NullPointerException\n" +
                "\tcom.journaldev.spring.controller.HomeController.user(HomeController.java:38)\n" +
                "\tsun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n" +
                "\tsun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n" +
                "\tsun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n" +
                "\tjava.lang.reflect.Method.invoke(Method.java:498)");
        System.out.println("I am testing");
    }
}
