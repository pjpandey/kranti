package com.dummy.kranti.testing.test;

import com.testing.kranti.framework.AbstractNonStartableTestComponent;
import com.testing.kranti.framework.operation.commmand.AbstractCommandOperation;
import com.testing.kranti.framework.TestComponentData;

import java.util.ArrayList;
import java.util.List;

public class DummyTestComponent extends AbstractNonStartableTestComponent {

    protected DummyTestComponent(TestComponentData dData) {
        super(dData);
    }

    @Override
    protected String getPersistencyType() {
        return null;
    }

    @Override
    protected void clean() {

    }

    @Override
    protected AbstractCommandOperation getHealthOperation() {
        return new AbstractCommandOperation() {

            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "healthCheck";
            }
        };
    }

    @Override
    protected AbstractCommandOperation getStartOperation() {
        return new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "start";
            }
        };
    }

    @Override
    protected AbstractCommandOperation getStopOperation() {
        return new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "Stopping";
            }
        };
    }

    @Override
    protected AbstractCommandOperation getHupOperation() {
        return new AbstractCommandOperation() {
            @Override
            public boolean shouldRunInBackground() {
                return false;
            }

            @Override
            public String getName() {
                return "Hupping";
            }
        };
    }

    @Override
    protected String getTestedApplication() {
        return "DUMMY";
    }
}
