package com.dummy.kranti.testing.test;

import com.testing.kranti.framework.store.AbstractComponentFactory;

public class DummyComponentFactory extends AbstractComponentFactory<DummyTestComponent> {
    public DummyComponentFactory() {
        super("DUMMY");
    }
    @Override
    public DummyTestComponent createComponent() {
        return new DummyTestComponent(getComponentData());
    }

    @Override
    public String getName() {
        return "DUMMY";
    }
}
