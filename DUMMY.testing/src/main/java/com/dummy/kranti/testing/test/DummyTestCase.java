package com.dummy.kranti.testing.test;

import com.testing.kranti.framework.AbstractTestCase;
import com.testing.kranti.framework.TestComponent;
import com.testing.kranti.framework.TestComponentData;
import com.testing.kranti.framework.reporter.TestReporter;

import java.util.*;

public abstract class DummyTestCase extends AbstractTestCase {

    private final TestComponentData.Builder builder;
    private DummyTestComponent server;

    protected DummyTestCase() {
        super("DUMMY");
        this.builder = new TestComponentData.Builder();


    }

    @Override
    protected void initComponents() {
        String dummyInstalPath = getInstallationPath(getApplication(), "DUMMY", 0);
        String upgradedInstallationPath = getUpgradedInstallationPath(getApplication(), "DUMMY", 0);
        String downgradedInstallationPath = getDowngradedInstallationPath(getApplication(), "DUMMY", 0);

        String configurationPath = getConfigurationPath();
        String upgradedConfigurationPath = getUpgradedConfigurationPath();
        String downgradedConfigurationPath = getDowngradedConfigurationPath(getApplication());

        server = new DummyTestComponent(builder.build(getHosts().get(0),
                dummyInstalPath,
                configurationPath,
                upgradedInstallationPath,
                upgradedConfigurationPath,
                downgradedInstallationPath,
                downgradedConfigurationPath));
    }

    @Override
    protected String getApplication() {
        return "DUMMY";
    }

    @Override
    protected void fillConfigurationReplacementMap(Map<String, String> mMap) {
        TestReporter.TRACE("fill map");
    }

    @Override
    protected void initTestCaseSpecific() throws Exception {
        TestReporter.TRACE("init test case specific");

    }

    @Override
    public List<? extends TestComponent> getTestComponents() {
        TestReporter.TRACE("init test components");
        List<TestComponent> lstComponents = new ArrayList<>();
        lstComponents.add(server);
        lstComponents.addAll(getTestComponentsSpecific());
        return lstComponents;
    }

    protected Collection<? extends TestComponent> getTestComponentsSpecific() {
        return Collections.emptyList();
    }
}
